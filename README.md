# JupyLabBook
A Jupyter Notebook used as an interactive lab book on the beamline SIRIUS (SOLEIL Synchrotron).

# Disclaimer

    Copyright (C) 2024  HEMMERLE Arnaud & FONTAINE Philippe

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

The beamline staff does not provide assistance in setting up JupyLabBook on your own computer.

# User manual
See the file JupyLabBook_User_Manual.pdf in the folder docs.
For documentation on the libraries, see ```index_doc_lib.html```.

# What is JupyLabBook?
JupyLabBook is an interactive lab book to be used on SIRIUS. It is meant to be as close as possible as the paper-like lab book that you would use on the beamline (except for the ruler and the tape).
The user can easily plot raw data, do data reduction, and write sample description without typing a single line of code. A PDF with a linear presentation of the notebook can be generated at any time of the experiment.
Expert users can define their own functions to perform more involved data analysis or customed presentation of the data, if needed.

# What is not JupyLabBook?
JupyLabBook was not designed to be a notebook for **analysis**, but for **data reduction** only.
Think of it as the traditional lab book, that should remain untouched after the end of the experiment. Notebooks for specific analysis (GIXS, GIXD, XRF ...) can be provided as well on demand.
JupyLabBook is not made of paper: it can crash, it can be corrupted, it can be deleted by mistake ... In the present state of development, we advise users to keep a written paper-like lab book in parallel with any interactive notebook. Especially for all the info concerning the samples used, that cannot be retrieved afterwards.
