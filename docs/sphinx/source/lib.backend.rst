lib.backend package
===================

Submodules
----------

lib.backend.PyNexus module
--------------------------

.. automodule:: lib.backend.PyNexus
   :members:
   :undoc-members:
   :show-inheritance:

lib.backend.area\_detector module
---------------------------------

.. automodule:: lib.backend.area_detector
   :members:
   :undoc-members:
   :show-inheritance:

lib.backend.data\_1d module
---------------------------

.. automodule:: lib.backend.data_1d
   :members:
   :undoc-members:
   :show-inheritance:

lib.backend.data\_1d\_twinx module
----------------------------------

.. automodule:: lib.backend.data_1d_twinx
   :members:
   :undoc-members:
   :show-inheritance:

lib.backend.gixd module
-----------------------

.. automodule:: lib.backend.gixd
   :members:
   :undoc-members:
   :show-inheritance:

lib.backend.gixs module
-----------------------

.. automodule:: lib.backend.gixs
   :members:
   :undoc-members:
   :show-inheritance:

lib.backend.isotherm module
---------------------------

.. automodule:: lib.backend.isotherm
   :members:
   :undoc-members:
   :show-inheritance:

lib.backend.xrf module
----------------------

.. automodule:: lib.backend.xrf
   :members:
   :undoc-members:
   :show-inheritance:

lib.backend.xrr module
----------------------

.. automodule:: lib.backend.xrr
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: lib.backend
   :members:
   :undoc-members:
   :show-inheritance:
