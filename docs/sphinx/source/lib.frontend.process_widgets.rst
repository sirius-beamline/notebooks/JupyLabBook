lib.frontend.process\_widgets package
=====================================

Submodules
----------

lib.frontend.process\_widgets.warea\_detector module
----------------------------------------------------

.. automodule:: lib.frontend.process_widgets.warea_detector
   :members:
   :undoc-members:
   :show-inheritance:

lib.frontend.process\_widgets.wdata\_1d module
----------------------------------------------

.. automodule:: lib.frontend.process_widgets.wdata_1d
   :members:
   :undoc-members:
   :show-inheritance:

lib.frontend.process\_widgets.wgixd module
------------------------------------------

.. automodule:: lib.frontend.process_widgets.wgixd
   :members:
   :undoc-members:
   :show-inheritance:

lib.frontend.process\_widgets.wgixs module
------------------------------------------

.. automodule:: lib.frontend.process_widgets.wgixs
   :members:
   :undoc-members:
   :show-inheritance:

lib.frontend.process\_widgets.wisotherm module
----------------------------------------------

.. automodule:: lib.frontend.process_widgets.wisotherm
   :members:
   :undoc-members:
   :show-inheritance:

lib.frontend.process\_widgets.wxrf module
-----------------------------------------

.. automodule:: lib.frontend.process_widgets.wxrf
   :members:
   :undoc-members:
   :show-inheritance:

lib.frontend.process\_widgets.wxrr module
-----------------------------------------

.. automodule:: lib.frontend.process_widgets.wxrr
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: lib.frontend.process_widgets
   :members:
   :undoc-members:
   :show-inheritance:
