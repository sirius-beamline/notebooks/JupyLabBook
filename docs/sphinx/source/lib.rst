lib package
===========

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   lib.backend
   lib.frontend

Submodules
----------

lib.jupylabbook module
----------------------

.. automodule:: lib.jupylabbook
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: lib
   :members:
   :undoc-members:
   :show-inheritance:
