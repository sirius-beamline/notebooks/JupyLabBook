"""
Library for area detector.
"""
import os
import datetime
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib import colors
import matplotlib.patches as patches
from PIL import Image
from lib.backend import PyNexus as PN
from lib.frontend import jlb_io

_RED='\x1b[31;01m'
_BLUE='\x1b[34;01m'
_RESET='\x1b[0m'

def process_area_detector_scan(
        nxs_name, path_to_nxs_dir, 
        roi_2d=[-1, -1, -1, -1], clim=(-1,-1),
        absorbers='', is_area_detector_logz=True, map_area_detector='viridis',
        selected_sensors = None,
        path_to_save_dir='', is_print_stamps=False, is_plot=False,
        is_save_sum=False, is_save_each=False, is_print_info=False):
    """
    Call functions for extracting, plotting, and saving an output of a scan on an area detector.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    roi_2d : list of int
        ROI for the integrated spectra [x0, y0, size_x, size_y].
    clim : tuple
        Lim min. and max. of the color scale.
    absorbers : str, optional
        Text to display which absorber was used.
    is_area_detector_logz : bool, optional
        Log scale on the color scale of the image.
    map_area_detector : str, optional
        Colormap of the image.
    selected_sensors : list of str, optional
        List of aliases of sensors, for their mean value to be added to the plot.
    path_to_save_dir : str, optional
        Path to the directory where the treated files will be saved.
    is_print_stamps : bool, optional
        Print the list of sensors contained in the nexus file.
    is_plot : bool, optional
        Plot the 2D image and the integrated profiles.
    is_save_sum : bool, optional
        Save the sum of the images.
    is_save_each : bool, optional
        Save each individual image and the sum.
    is_print_info : bool, optional
        Verbose mode.

    Returns
    -------
    images_sum : array
        Detector images integrated over the scan.
    integ_x : array
        Profile integrated along the horizontal axis.
    integ_y : array
        Profile integrated along the vertical axis.

    """
    images, images_sum, integ_x, integ_y,\
    alias_detector, time_str, sensors_str, stamps_0d, data_0d = \
    extract_area_detector_scan(
        nxs_name, path_to_nxs_dir, selected_sensors,
        is_print_stamps, is_print_info
        )

    fig = None

    # Avoid crash of the cell if 2D images are not present
    if images is None:
        is_plot = False
        is_save_each = False
        is_save_sum = False

    if is_plot:
        fig = plot_area_detector_scan(
            images_sum, roi_2d, clim,
            nxs_name, absorbers, is_area_detector_logz, map_area_detector,
            time_str, sensors_str
            )

    if is_save_each:
        # Save the sum and each individual image in a dedicated folder
        save_area_detector_scan(
            images, images_sum,
            integ_x, integ_y,
            alias_detector, nxs_name, stamps_0d, data_0d, fig,
            path_to_save_dir, is_print_info
            )

    elif is_save_sum:
        # Save only the sum
        save_area_detector_scan(
            None, images_sum,
            integ_x, integ_y,
            alias_detector, nxs_name, stamps_0d, data_0d, fig,
            path_to_save_dir, is_print_info
            )

    # Do not return `images`, as this might be a very big object
    return images_sum, integ_x, integ_y


def extract_area_detector_scan(nxs_name, path_to_nxs_dir, selected_sensors,
                               is_print_stamps, is_print_info):
    """
    Extract the nexus file and return useful quantities.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    selected_sensors : list of str
        List of aliases of sensors, for their mean value to be added to the plot.
    is_print_stamps : bool, optional
        Print the list of sensors contained in the nexus file.
    is_print_info : bool, optional
        Verbose mode.

    Returns
    -------
    images : array
        Array of individual detector images.
    images_sum : array
        Detector images integrated over the scan.
    integ_x : array
        Profile integrated along the horizontal axis.
    integ_y : array
        Profile integrated along the vertical axis.
    alias_detector : str
        The alias of the 2D detector, e.g. 'pilatus' or 'ufxc'.
    time_str : str
        Starting/ending dates of the scan.
    sensors_str : str
        Label with mean values of selected sensors.
    stamps_0d : array
        Aliases of each 0D sensor in the scan.
    data_0d : array
        Values of each 0D sensor in the scan.

    Raises
    ------
    FileNotFoundError
        If a file or a folder is missing.
    SystemExit('Could not open Nexus file.'))
        If the Nexus file cannot be accessed.

    """
    nxs_path = path_to_nxs_dir+nxs_name

    if not os.path.isfile(nxs_path):
        print(_RED+'Scan %s is not present in the file directory.'%(nxs_name)+_RESET)
        print('\t\t file directory : %s'%path_to_nxs_dir)
        raise FileNotFoundError('Missing folder or file.')

    column_z = None
    alias_detector = ''
    column_epoch = None

    if is_print_info:
        print(_BLUE+" - Open Nexus Data File :"+ _RESET)
        print('\t%s'%nxs_path)

    try:
        nexus = PN.PyNexusFile(nxs_path, fast=True)
    except:
        print(_RED+'\t Could not open Nexus file.'+_RESET)
        raise Exception('Could not open Nexus file.')

    nbpts = int(nexus.get_nbpts())
    if is_print_info:
        print("\t. Number of data points: %s"%nbpts)

    # Extract stamps
    stamps = nexus.extractStamps()
    if is_print_stamps :
        print("\t. Available Counters:")
    for i, stamp in enumerate(stamps):
        if stamp[1] is not None:
            if is_print_stamps :
                print("\t\t%s  -------> %s"%(i,stamp[1]))
            if stamp[1].lower() in ['pilatus', 'ufxc', 'ufx', 'pilatus_image', 'ufxc_image']:
                column_z = i
                alias_detector = stamp[1].lower()
        else:
            if is_print_stamps :
                print("\t\t%s  -------> %s"%(i,stamp[0]))

    # Extract 0D data
    stamps_0d, data_0d = nexus.extractData('0D')

    # Find columns
    for i, stamp_0d in enumerate(stamps_0d):
        if stamp_0d[0].lower()=='sensorstimestamps':
            column_epoch = i

    # Check that 2D images are present
    is_images_present = False
    if column_z is not None:
        is_images_present = True
        if is_print_info:
            print('\t. 2D detector data found, (column %d, alias %s)'%(column_z, stamps[column_z][1]))
    else:
        print(_RED+'\t. No 2D detector data found'+_RESET)
        nexus.close()
        #raise Exception('Area detector not found.')

    # Find positions of non Nan values based on data[0]
    args = ~np.isnan(data_0d[0])

    # Extract the value of the sensors in the list selected_sensors
    sensors_str = ''
    if selected_sensors is not None:
        for sensor in selected_sensors:
            for i, stamp_0d in enumerate(stamps_0d):
                if stamp_0d[1] is not None:
                    if stamp_0d[1].lower()==sensor:
                        sensor_value = np.mean(data_0d[i][args])
                        sensors_str += f'{sensor} = {sensor_value}\n'

    # Get first and last position of the non NaN values
    if is_print_info:
        print('\t. Valid data between points %d and %d'
                      %(np.argmax(args), len(args)-np.argmax(args[::-1])-1))
        print(sensors_str)

    # Extract time
    time_str = ''
    if column_epoch is not None:
        epoch_time =  data_0d[column_epoch][args]
        time_str = 'Starting time: %s\nEnding time: %s'%(
                   datetime.datetime.fromtimestamp(epoch_time[0]),
                   datetime.datetime.fromtimestamp(epoch_time[-1]))

    if is_images_present:

        # Load images
        _, images = nexus.extractData('2D')
        nexus.close()
        images = np.asarray(images)[0]

        # Get positions of the dead pixels and set them to zero
        # Start from the known path of a module
        if alias_detector in ['pilatus', 'pilatus_image']:
            file_dead_pixels_pilatus = os.path.dirname(jlb_io.__file__)\
                                       +'/../params/dead_pixels/dead_pixels_pilatus.dat'
            dead_pixels = jlb_io.extract_dead_pixels(file_dead_pixels_pilatus)
        elif alias_detector in ['ufxc', 'ufx', 'ufxc_image']:
            file_dead_pixels_ufxc = os.path.dirname(jlb_io.__file__)\
                                       +'/../params/dead_pixels/dead_pixels_ufxc.dat'
            dead_pixels = jlb_io.extract_dead_pixels(file_dead_pixels_ufxc)

        for ii,jj in dead_pixels:
            images[:,ii,jj] = 0.0

        # Sum the images over the scan
        images_sum = images.sum(axis=0)

        # Replace dead zones with an intensity of -2.
        images_sum = np.where(images_sum<0., -2., images_sum)

        # Profiles
        # Put all the negative pixels to zero before integration
        integ_x = np.where(images_sum>0, images_sum, 0.).sum(axis=1)
        integ_y = np.where(images_sum>0, images_sum, 0.).sum(axis=0)


    else:
        images = None
        images_sum = None
        integ_x = None
        integ_y = None
        alias_detector = None

    return images, images_sum, integ_x, integ_y, \
           alias_detector, time_str, sensors_str, stamps_0d, data_0d

def plot_area_detector_scan(
    images_sum, roi_2d, clim, 
    nxs_name, absorbers, is_area_detector_logz, map_area_detector,
    time_str, sensors_str):
    """
    Plot area detector data.

    Parameters
    ----------
    images_sum : array
        Detector images integrated over the scan.
    roi_2d : list of int
        ROI for the integrated spectra [x0, y0, size_x, size_y].
    clim : tuple
        Lim min. and max. of the color scale.
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    absorbers : str, optional
        Text to display which absorber was used.
    is_area_detector_logz : bool, optional
        Log scale on the color scale of the image.
    map_area_detector : str, optional
        Colormap of the image.
    time_str : str
        Starting/ending dates of the scan.
    sensors_str : str
        Label with mean values of selected sensors.

    Returns
    -------
    fig : matplotlib figure
        The figure to be saved in pdf.

    """
    # Print absorbers
    if absorbers != '':
        print("Absorbers: %s"%absorbers)

    if time_str != '':
        print(time_str)

    if sensors_str != '':
        print(sensors_str)

    # If there is a negative value in the ROI,
    # take the whole detector
    if any(t < 0 for t in roi_2d):
        roi_2d = [0, 0, np.shape(images_sum)[1]-1, np.shape(images_sum)[0]-1]

    # Compute the profiles over the ROI
    images_roi = images_sum[roi_2d[1]:roi_2d[1]+roi_2d[3],
                            roi_2d[0]:roi_2d[0]+roi_2d[2]]

    integ_x_roi = np.where(images_roi>0, images_roi, 0.).sum(axis=1)
    integ_y_roi = np.where(images_roi>0, images_roi, 0.).sum(axis=0)

    x_list_roi = np.arange(0,np.shape(images_sum)[1])[roi_2d[0]:roi_2d[0]+roi_2d[2]]
    y_list_roi = np.arange(0,np.shape(images_sum)[0])[roi_2d[1]:roi_2d[1]+roi_2d[3]]

    # Grid for the 2D image
    pixels_x = np.arange(0,np.shape(images_sum)[1],1)
    pixels_y = np.arange(0,np.shape(images_sum)[0],1)
    x_grid, y_grid = np.meshgrid(pixels_x, pixels_y)

    # Create fig
    fig = plt.figure(figsize=(15,15))
    fig.subplots_adjust(top=0.95)
    fig.suptitle(nxs_name.split('\\')[-1], fontsize='x-large')

    # Divide the grid in 2x2
    outer = gridspec.GridSpec(2, 2, wspace=0.2)

    # Divide the left row in 2x1
    inner = gridspec.GridSpecFromSubplotSpec(2, 1,
                    subplot_spec=outer[0], hspace=0.5)

    #############################################
    # Plot a profile along y (integrated over x)
    ax1 = fig.add_subplot(inner[0])

    if is_area_detector_logz:
        ax1.set_yscale('log')
    ax1.set(xlabel = 'vertical pixel (y)', ylabel = 'integration along x (ROI)')

    # Compute best limits
    ax1.set_ylim(np.min(integ_x_roi[integ_x_roi>0])*0.8,np.max(integ_x_roi)*1.2)
    
    ax1.plot(y_list_roi, integ_x_roi)


    ###################################################
    # Plot a profile along x (integrated over y)
    ax2 = fig.add_subplot(inner[1])

    if is_area_detector_logz:
        ax2.set_yscale('log')
    ax2.set(xlabel = 'horizontal pixel (x)', ylabel = 'integration along y (ROI)')

    # Compute best limits
    ax2.set_ylim(np.min(integ_y_roi[integ_y_roi>0])*0.8,np.max(integ_y_roi)*1.2)
    
    ax2.plot(x_list_roi, integ_y_roi)


    #Divide the right row in 1x1
    inner = gridspec.GridSpecFromSubplotSpec(1, 1,
                    subplot_spec=outer[1], wspace=0.1, hspace=0.1)

    #####################################################
    # Show the full image integrated over the scan
    ax2 = fig.add_subplot(inner[0])

    if is_area_detector_logz:
        if (clim[0] != -1 and clim[1] != -1):
            ax2.pcolormesh(x_grid, y_grid, images_sum, cmap = map_area_detector, shading = 'auto',
                           norm = colors.LogNorm(vmin=clim[0], vmax=clim[1]), rasterized=True)
        else:
            ax2.pcolormesh(x_grid, y_grid, images_sum, cmap = map_area_detector, shading = 'auto',
                           norm = colors.LogNorm(), rasterized=True)                           
    else:
        if (clim[0] != -1 and clim[1] != -1):
            ax2.pcolormesh(x_grid, y_grid, images_sum, cmap = map_area_detector, shading = 'auto',
                       rasterized=True, vmin=clim[0], vmax=clim[1])            
        else:
            ax2.pcolormesh(x_grid, y_grid, images_sum, cmap = map_area_detector, shading = 'auto',
                           rasterized=True)
            
    ax2.set_xlabel('horizontal pixel (x)', fontsize='large')
    ax2.set_ylabel('vertical pixel (y)', fontsize='large')

    # Add the ROI
    rect = patches.Rectangle( (x_list_roi[0], y_list_roi[1]) ,
                               x_list_roi[-1]-x_list_roi[0],
                               y_list_roi[-1]-y_list_roi[0],
                              linewidth=1, edgecolor='r', facecolor='none')
    ax2.add_patch(rect)
    
    plt.show()

    return fig

def save_area_detector_scan(
    images, images_sum, integ_x, integ_y,
    alias_detector, nxs_name, stamps_0d, data_0d, fig,
    path_to_save_dir, is_print_info):
    """
    Save area detector data.

    XXX_DETECTOR_sum.mat: the matrix corresponding to the image displayed, in ascii.
    XXX_DETECTOR_sum.tiff: the matrix corresponding to the image displayed, in tiff.
    XXX_integrated_x.dat: the horizontal integration of the whole detector as a function of y.
    XXX_integrated_y.dat: the vertical integration of the whole detector as a function of x.
    XXX_images/XXX_DETECTOR_N.tiff : each image of the scan, in tiff.
    XXX.dat : the value of each sensor at each point of the scan.
    XXX.pdf : the figure in pdf.

    Parameters
    ----------
    images : array
        Array of individual detector images.
    images_sum : array
        Detector images integrated over the scan.
    integ_x : array
        Profile integrated along the horizontal axis.
    integ_y : array
        Profile integrated along the vertical axis.
    alias_detector : str
        The alias of the 2D detector, e.g. 'pilatus' or 'ufxc'.
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    stamps_0d : array
        Aliases of each 0D sensor in the scan.
    data_0d : array
        Values of each 0D sensor in the scan.
    fig : None or matplotlib figure
        The figure to be saved in pdf. Pass None if not wanted.
    path_to_save_dir : str
        Path to the directory where the treated files will be saved.
    is_print_info : bool
        Verbose mode.

    """
    # Create Save Name
    save_name = path_to_save_dir + nxs_name[:nxs_name.rfind('.nxs')]

    # Save the profiles and the raw image
    np.savetxt(save_name+'_integrated_y.dat', integ_y,
               delimiter = '\t', comments='', header ='YIntegrated')
    np.savetxt(save_name+'_integrated_x.dat', integ_x,
               delimiter = '\t', comments='', header ='XIntegrated')
    np.savetxt(save_name+'_'+alias_detector+'_sum.mat', images_sum)

    im = Image.fromarray(images_sum)
    im.save(save_name+'_'+alias_detector+'_sum.tiff')

    # Figure in pdf
    if fig is not None:
        plt.figure(fig)
        plt.savefig(save_name+'.pdf', bbox_inches='tight')
        plt.close()

    # Save each image individually in a folder
    if images is not None:
        path_to_dir = path_to_save_dir+nxs_name[:nxs_name.rfind('.nxs')]+'_images'
        if not os.path.exists(path_to_dir):
            os.mkdir(path_to_dir)

        for index, image in enumerate(images):
            im = Image.fromarray(image)
            im.save(path_to_dir+'/'+nxs_name[:nxs_name.rfind('.nxs')]+'_'+alias_detector+'_'+str(index)+'.tiff')

    # Save 0D data
    header_0d = [stamp[1] if stamp[1] is not None else stamp[0] for stamp in stamps_0d]
    header_0d = '\t'.join(header_0d)

    np.savetxt(save_name+'.dat', np.transpose(data_0d), header = header_0d,
               comments = '', fmt='%s', delimiter = '\t')

    if is_print_info:
        print('\t. Original matrix saved in:')
        print('\t'+save_name+'_'+alias_detector+'_sum.mat')
        print('\t. Tiff saved in:')
        print('\t'+save_name+'_'+alias_detector+'_sum.tiff')
        print('\t. Profiles saved in:')
        print('\t%s_integrated_x.dat'%save_name)
        print('\t%s_integrated_y.dat'%save_name)
        print('\t. Dat file saved in:')
        print('\t%s.dat'%save_name)
        if images is not None:
            print('\t. Individual images saved in:')
            print('\t'+path_to_dir+'/')
        if fig is not None:
            print('\t. Figure saved in:')
            print('\t%s.pdf'%save_name)
