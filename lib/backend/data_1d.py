"""
Library for data 1D.
"""
import os
import datetime
import numpy as np
import matplotlib.pyplot as plt
import lmfit as lm
from scipy.special import erf
from lib.backend import PyNexus as PN

_RED='\x1b[31;01m'
_BLUE='\x1b[34;01m'
_RESET='\x1b[0m'


######################################################
######################################################
######################################################
############# PROCESS DATA 1D ########################
######################################################
######################################################
######################################################

def process_data_1d(
        nxs_name, path_to_nxs_dir,
        x_label, y_label, is_logx=False, is_logy=False,
        absorbers='', path_to_save_dir='', is_print_stamps=False, is_plot=False,
        is_save=False, is_print_info=False):
    """
    Call functions for extracting, plotting, and saving 1d data.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    x_label : str
        Exact name of the x sensor, as it appears in the stamps.
    y_label : str
        Exact name of the y sensor, as it appears in the stamps.
    is_logx : bool, optional
        Log scale on the x axis.
    is_logy : bool, optional
        Log scale on the y axis.
    absorbers : str, optional
        Text to display which absorber was used.
    path_to_save_dir : str
        Path to the directory where the treated files will be saved.
    is_print_stamps : bool, optional
        Print the list of sensors contained in the nexus file.
    is_plot : bool, optional
        Plot the data.
    is_save : bool, optional
        Save the results.
    is_print_info : bool, optional
        Verbose mode.

    Returns
    -------
    x : array
        List of x values.
    y : array
        List of y values.

    """
    x, y, time_str, stamps_0d, data_0d = \
    extract_data_1d(
        nxs_name, path_to_nxs_dir,
        x_label, y_label,
        is_print_stamps, is_print_info
        )

    fig = None

    if is_plot:
        fig = plot_data_1d(
            x, y, x_label, y_label, time_str,
            nxs_name, absorbers, is_logx, is_logy
            )

    if is_save:
        save_data_1d(
            nxs_name, stamps_0d, data_0d, fig,
            path_to_save_dir, is_print_info
            )

    return x, y

def extract_data_1d(
    nxs_name, path_to_nxs_dir,
    x_label, y_label,
    is_print_stamps, is_print_info):
    """
    Extract the nexus file and return useful quantities.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    x_label : str
        Exact name of the x sensor, as it appears in the stamps.
    y_label : str
        Exact name of the y sensor, as it appears in the stamps.
    is_print_stamps : bool, optional
        Print the list of sensors contained in the nexus file.
    is_print_info : bool, optional
        Verbose mode.

    Returns
    -------
    x : array
        List of x values.
    y : array
        List of y values.
    time_str : str
        Starting/ending dates of the scan.
    stamps_0d : array
        Aliases of each 0D sensor in the scan.
    data_0d : array
        Values of each 0D sensor in the scan.

    Raises
    ------
    FileNotFoundError
        If a file or a folder is missing.
    Exception('Could not open Nexus file.'))
        If the Nexus file cannot be accessed.
    Exception('Sensor not found.')
        If the sensor is not found in the sensor list.

    """
    nxs_path = path_to_nxs_dir+nxs_name

    if not os.path.isfile(nxs_path):
        print(_RED+'Scan %s is not present in the file directory.'%(nxs_name)+_RESET)
        print('\t\t file directory : %s'%path_to_nxs_dir)
        raise FileNotFoundError('Missing folder or file.')

    column_epoch = None

    if is_print_info:
        print(_BLUE+" - Open Nexus Data File :"+ _RESET)
        print('\t%s'%nxs_path)

    try:
        nexus = PN.PyNexusFile(nxs_path, fast=True)
    except:
        print(_RED+'\t Could not open Nexus file.'+_RESET)
        raise Exception('Could not open Nexus file.')

    nbpts = int(nexus.get_nbpts())
    if is_print_info:
        print("\t. Number of data points: %s"%nbpts)

    # Extract stamps
    stamps = nexus.extractStamps()
    if is_print_stamps :
        print("\t. Available Counters:")
        for i, stamp in enumerate(stamps):
            if stamp[1] is not None:
                print("\t\t%s  -------> %s"%(i,stamp[1]))
            else:
                print("\t\t%s  -------> %s"%(i,stamp[0]))

    # Extract 0D data
    stamps_0d, data_0d = nexus.extractData('0D')

    nexus.close()

    # Find columns
    for i, stamp_0d in enumerate(stamps_0d):
        if stamp_0d[1] is None:
            if stamp_0d[0].lower()=='sensorstimestamps':
                column_epoch = i

    sensor_list = [stamps_0d[i][0] if stamps_0d[i][1] is None else stamps_0d[i][1] for i in range(len(stamps_0d))]

    if x_label in sensor_list:
        x_arg = sensor_list.index(x_label)
    else:
        print(_RED+'\t Sensor %s is not in the sensor list'%(x_label)+_RESET)
        raise Exception('Sensor not found.')

    if y_label in sensor_list:
        y_arg = sensor_list.index(y_label)
    else:
        print(_RED+'\t Sensor %s is not in the sensor list'%(y_label)+_RESET)
        raise Exception('Sensor not found.')

    # Find positions of non Nan values based on data_0d[x_arg]
    args = ~np.isnan(data_0d[x_arg])

    # Get first and last position of the non NaN values
    if is_print_info:
        print('\t. Valid data between points %d and %d'
                      %(np.argmax(args), len(args)-np.argmax(args[::-1])-1))

    # Extract time
    time_str = ''
    if column_epoch is not None:
        epoch_time =  data_0d[column_epoch][args]
        time_str = 'Starting time: %s\nEnding time: %s'%(
                   datetime.datetime.fromtimestamp(epoch_time[0]),
                   datetime.datetime.fromtimestamp(epoch_time[-1]))

    # Take only the non NaN values
    x = data_0d[x_arg][args]
    y = data_0d[y_arg][args]

    return x, y, time_str, stamps_0d, data_0d

def plot_data_1d(
    x, y, x_label, y_label, time_str,
    nxs_name, absorbers, is_logx, is_logy):
    """
    Plot 1d data.

    Parameters
    ----------
    x : array
        List of x values.
    y : array
        List of y values.
    x_label : str
        Exact name of the x sensor, as it appears in the stamps.
    y_label : str
        Exact name of the y sensor, as it appears in the stamps.
    time_str : str
        Starting/ending dates of the scan.
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    absorbers : str, optional
        Text to display which absorber was used.
    is_logx : bool, optional
        Log scale on the x axis.
    is_logy : bool, optional
        Log scale on the y axis.

    Returns
    -------
    fig : matplotlib figure
        The figure to be saved in pdf.

    """
    # Print absorbers
    if absorbers != '':
        print("Absorbers: %s"%absorbers)

    if time_str != '':
        print(time_str)

    fig = plt.figure(1, figsize=(12,5))
    ax = fig.add_subplot(111)
    ax.plot(x, y, 'o-')
    ax.set_xlabel(x_label, fontsize=16)
    ax.set_ylabel(y_label, fontsize=16)
    ax.tick_params(labelsize=16)
    ax.yaxis.offsetText.set_fontsize(16)

    # Avoid strange offsets on the number range
    ax.get_yaxis().get_major_formatter().set_useOffset(False)
    ax.get_xaxis().get_major_formatter().set_useOffset(False)

    if is_logy:
        ax.set_yscale('log')
    if is_logx:
        ax.set_xscale('log')

    suptitle = nxs_name[nxs_name.rfind('/')+1:]
    fig.suptitle(suptitle, fontsize='x-large')

    plt.show()

    return fig

def save_data_1d(
    nxs_name, stamps_0d, data_0d, fig,
    path_to_save_dir, is_print_info):
    """
    Save 1D data.

    XXX.dat : the value of each sensor at each point of the scan.
    XXX.pdf : the figure in pdf.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    stamps_0d : array
        Aliases of each 0D sensor in the scan.
    data_0d : array
        Values of each 0D sensor in the scan.
    fig : None or matplotlib figure
        The figure to be saved in pdf. Pass None if not wanted.
    path_to_save_dir : str
        Path to the directory where the treated files will be saved.
    is_print_info : bool
        Verbose mode.

    """
    # Create Save Name
    save_name = path_to_save_dir + nxs_name[:nxs_name.rfind('.nxs')]

    # Save 0D data
    header_0d = [stamp[1] if stamp[1] is not None else stamp[0] for stamp in stamps_0d]
    header_0d = '\t'.join(header_0d)

    np.savetxt(save_name+'.dat', np.transpose(data_0d), header = header_0d,
               comments = '', fmt='%s', delimiter = '\t')

    if is_print_info:
        print('\t. Scalar data saved in:')
        print('\t%s.dat'%save_name)

    # Figure in pdf
    if fig is not None:
        plt.figure(fig)
        plt.savefig(save_name+'.pdf', bbox_inches='tight')
        plt.close()

        if is_print_info:
            print('\t. Figure saved in:')
            print('\t%s.pdf'%save_name)


######################################################
######################################################
######################################################
################# FIT DATA 1D ########################
######################################################
######################################################
######################################################


def fit_with_gaussian(x, y, params_init, is_print_info):
    """
    Fit with a gaussian using LMFIT.

    Parameters
    ----------
    x : array
        List of x values.
    y : array
        List of y values.
    params_init : dict or None
        Dictionnary of parameters for initial guesses and limits.
        All the parameters should be given. For example:
        params_init = {'cst':{'init':0, 'min':-1, 'max':1},
        'linear_coeff':{'init':0, 'min':-1, 'max':1},
        'mu':{'init':0, 'min':-1, 'max':1},
        'sigma':{'init':0.2, 'min':0., 'max':1},
        'amplitude':{'init':10., 'min':-10, 'max':20}}
    is_print_info : bool
        Verbose mode.

    Returns
    -------
    y_fit : array
        List of y values from the fit.
    lm_result : object MinimizerResult
        Results of lm minimization. Includes data such as status and error messages, fit statistics,
        and the updated (i.e., best-fit) parameters themselves in the params attribute.

    """
    # Define the parameters and first guess
    fit_params = lm.Parameters()

    if params_init is None:
        # Prepare initial guess and rescale data
        nbpts = x.shape[0]
        y_resc = y-y.min()
        mu0 = sum(x*y_resc)/sum(y_resc)
        sigma0 = np.sqrt(sum(y_resc*(x-mu0)**2)/sum(y_resc))
        amplitude0 = y.max()-y.min()
        linear_coeff0 = (y[nbpts-1]-y[0])/(x[nbpts-1]-x[0])
        cst0 = y.min()

        # Free constraints
        mu_min = -np.inf
        mu_max = np.inf
        sigma_min = -np.inf
        sigma_max = np.inf
        amplitude_min = -np.inf
        amplitude_max = np.inf
        linear_coeff_min = -np.inf
        linear_coeff_max = np.inf
        cst_min = -np.inf
        cst_max = np.inf

        # Adding constraints
        """
        mu_min = x.min()
        mu_max = x.max()
        sigma_min = 0.
        sigma_max = x.max()-x.min()
        amplitude_min = 0.
        amplitude_max = (y.max()-y.min())*1.1
        linear_coeff_min = -10*B
        linear_coeff_max = 10*B
        cst_min = -np.inf
        cst_max = y.max()*1.0
        """

    else:
        # Take the parameters given by the user as initial guess
        mu0 = params_init['mu']['init']
        sigma0 = params_init['sigma']['init']
        amplitude0 = params_init['amplitude']['init']
        linear_coeff0 = params_init['linear_coeff']['init']
        cst0 = params_init['cst']['init']

        # Parameters with constraints
        mu_min = params_init['mu']['min']
        mu_max = params_init['mu']['max']
        sigma_min = params_init['sigma']['min']
        sigma_max = params_init['sigma']['max']
        amplitude_min = params_init['amplitude']['min']
        amplitude_max = params_init['amplitude']['max']
        linear_coeff_min = params_init['linear_coeff']['min']
        linear_coeff_max = params_init['linear_coeff']['max']
        cst_min = params_init['cst']['min']
        cst_max = params_init['cst']['max']


    fit_params.add_many(('cst', cst0, True, cst_min, cst_max, None),
                   ('linear_coeff', linear_coeff0 , True, linear_coeff_min, linear_coeff_max, None),
               ('amplitude', amplitude0 , True, amplitude_min, amplitude_max, None),
               ('sigma', sigma0, True, sigma_min, sigma_max, None),
               ('mu', mu0, True, mu_min, mu_max, None)
               )

    # Fit initialisation and fit
    fitter = lm.Minimizer(residuals_gaussian_function, fit_params, fcn_args=(x, y))
    lm_result = fitter.minimize()
    lm_result.fun_name = 'gaussian'

    y_fit = gaussian_function(x, lm_result.params['mu'], lm_result.params['sigma'],
                              lm_result.params['cst'], lm_result.params['linear_coeff'],
                              lm_result.params['amplitude'])

    if is_print_info:
        print(lm.fit_report(lm_result))

    return y_fit, lm_result

def process_gaussian_fit(nxs_name, path_to_nxs_dir,
    x_label, y_label, params_init=None,
    absorbers='', path_to_save_dir='', is_print_stamps=False, is_plot=False,
    is_save=False, is_print_info=False):
    """
    Call functions for extracting, fitting with a gaussian, plotting, and saving 1d data.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    x_label : str
        Exact name of the x sensor, as it appears in the stamps.
    y_label : str
        Exact name of the y sensor, as it appears in the stamps.
    params_init : dict or None, optional
        Dictionnary of parameters for initial guesses and limits.
        All the parameters should be given. For example:
        params_init = {'cst':{'init':0, 'min':-1, 'max':1},
        'linear_coeff':{'init':0, 'min':-1, 'max':1},
        'mu':{'init':0, 'min':-1, 'max':1},
        'sigma':{'init':0.2, 'min':0., 'max':1},
        'amplitude':{'init':10., 'min':-10, 'max':20}}
    absorbers : str, optional
        Text to display which absorber was used.
    path_to_save_dir : str
        Path to the directory where the treated files will be saved.
    is_print_stamps : bool, optional
        Print the list of sensors contained in the nexus file.
    is_plot : bool, optional
        Plot the data.
    is_save : bool, optional
        Save the results.
    is_print_info : bool, optional
        Verbose mode.

    Returns
    -------
    x : array
        List of x values.
    y : array
        List of y values.
    y_fit : array
        List of y values from the fit.
    lm_result : object MinimizerResult
        Results of lm minimization. Includes data such as status and error messages, fit statistics,
        and the updated (i.e., best-fit) parameters themselves in the params attribute.

    """
    x, y, time_str, stamps_0d, data_0d = \
    extract_data_1d(
        nxs_name, path_to_nxs_dir,
        x_label, y_label,
        is_print_stamps, is_print_info
        )

    y_fit, lm_result = fit_with_gaussian(x, y, params_init, is_print_info)

    fig = None

    if is_plot:
        fig = plot_gaussian_fit(
            x, y, y_fit, lm_result,
            x_label, y_label, time_str,
            nxs_name, absorbers, is_print_info
            )

    if is_save:
        save_data_1d(
            nxs_name, stamps_0d, data_0d, fig,
            path_to_save_dir, is_print_info
            )

        save_fit_result(
            nxs_name, x, y, y_fit, lm_result,
            x_label, y_label, fig,
            path_to_save_dir, is_print_info
            )

    return x, y, y_fit, lm_result

def plot_gaussian_fit(
    x, y, y_fit, lm_result,
    x_label, y_label, time_str,
    nxs_name, absorbers, is_print_info):
    """
    Plot fit of 1d data.

    Parameters
    ----------
    x : array
        List of x values.
    y : array
        List of y values.
    y_fit : array
        List of y values from the fit.
    lm_result : object MinimizerResult
        Results of lm minimization. Includes data such as status and error messages, fit statistics,
        and the updated (i.e., best-fit) parameters themselves in the params attribute.
    x_label : str
        Exact name of the x sensor, as it appears in the stamps.
    y_label : str
        Exact name of the y sensor, as it appears in the stamps.
    time_str : str
        Starting/ending dates of the scan.
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    absorbers : str
        Text to display which absorber was used.
    is_print_info : bool
        Verbose mode.

    Returns
    -------
    fig : matplotlib figure
        The figure to be saved in pdf.

    """
    # Print absorbers
    if absorbers != '':
        print("Absorbers: %s"%absorbers)

    if time_str != '':
        print(time_str)

    fig = plt.figure(1, figsize=(12,5))
    ax = fig.add_subplot(111)
    ax.plot(x, y, 'o-', label=nxs_name[nxs_name.rfind('_')+1:nxs_name.rfind('.')])
    ax.plot(x, y_fit, 'r-', lw=2)
    ax.legend(fontsize=16)
    ax.set_xlabel(x_label, fontsize=16)
    ax.set_ylabel(y_label, fontsize=16)
    ax.tick_params(labelsize=16)
    ax.yaxis.offsetText.set_fontsize(16)
    if np.sign(x.min())==1:
        fig.text(0.2, 0.65, 'Center %3.4f'%(lm_result.params['mu']), fontsize=14)
        fig.text(0.2, 0.60, 'FWHM %3.4f'%(2.0*np.sqrt(2.0*np.log(2.))*lm_result.params['sigma']), fontsize=14)
    else:
        fig.text(0.7, 0.60, 'Center %3.4f'%(lm_result.params['mu']), fontsize=14)
        fig.text(0.7, 0.55, 'FWHM %3.4f'%(2.0*np.sqrt(2.0*np.log(2.))*lm_result.params['sigma']), fontsize=14)
    ax.set_title('Gaussian fit', fontsize=14)

    if is_print_info:
        # Plot first guess
        mu0 = lm_result.params['mu'].init_value
        sigma0 = lm_result.params['sigma'].init_value
        amplitude0 = lm_result.params['amplitude'].init_value
        linear_coeff0 = lm_result.params['linear_coeff'].init_value
        cst0 = lm_result.params['cst'].init_value
        ax.plot(x,gaussian_function(x, mu0, sigma0,
                                    cst0, linear_coeff0,
                                    amplitude0),
                'k--', lw=1)

    plt.show()

    return fig

def save_fit_result(
    nxs_name, x, y, y_fit, lm_result,
    x_label, y_label, fig,
    path_to_save_dir, is_print_info):
    """
    Save 1D data and fit results.

    XXX.dat : the value of each sensor at each point of the scan.
    XXX.pdf : the figure in pdf.
    XXX_fit_FUNCTION_report.dat : the lmfit report.
    XXX_fit_FUNCTION_result.dat : the fitted data x, y, y_fit.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    x : array
        List of x values.
    y : array
        List of y values.
    y_fit : array
        List of y values from the fit.
    lm_result : object MinimizerResult
        Results of lm minimization. Includes data such as status and error messages, fit statistics,
        and the updated (i.e., best-fit) parameters themselves in the params attribute.
    x_label : str
        Exact name of the x sensor, as it appears in the stamps.
    y_label : str
        Exact name of the y sensor, as it appears in the stamps.
    fig : None or matplotlib figure
        The figure to be saved in pdf. Pass None if not wanted.
    path_to_save_dir : str
        Path to the directory where the treated files will be saved.
    is_print_info : bool
        Verbose mode.

    """
    # Create Save Name
    save_name = path_to_save_dir + nxs_name[:nxs_name.rfind('.nxs')]

    # Save fit report
    np.savetxt(save_name+'_fit_'+lm_result.fun_name+'_report.dat', [lm.fit_report(lm_result)],
               comments = '', fmt='%s')

    # Save fit result
    np.savetxt(save_name+'_fit_'+lm_result.fun_name+'_result.dat', np.transpose([x, y, y_fit]),
               header = x_label+'\t'+y_label+'\t'+'fit',
               comments = '', fmt='%s')

    if is_print_info:
        print('\t. Fit report saved in:')
        print('\t%s.dat'%(save_name+'_fit_'+lm_result.fun_name+'_report.dat'))
        print('\t. Fit result saved in:')
        print('\t%s.dat'%(save_name+'_fit_'+lm_result.fun_name+'_result.dat'))

    # Figure in pdf
    if fig is not None:
        plt.figure(fig)
        plt.savefig(save_name+'.pdf', bbox_inches='tight')
        plt.close()

        if is_print_info:
            print('\t. Figure saved in:')
            print('\t%s.pdf'%save_name)


def fit_with_erf(x, y, params_init, is_print_info):
    """
    Fit with an error function using LMFIT.

    Parameters
    ----------
    x : array
        List of x values.
    y : array
        List of y values.
    params_init : dict or None
        Dictionnary of parameters for initial guesses and limits.
        All the parameters should be given. For example:
        params_init = {'cst':{'init':0, 'min':-1, 'max':1},
        'mu':{'init':0, 'min':-1, 'max':1},
        'sigma':{'init':0.2, 'min':0., 'max':1},
        'amplitude':{'init':10., 'min':-10, 'max':20},
        'sign':{'value':-1}}
    is_print_info : bool
        Verbose mode.

    Returns
    -------
    y_fit : array
        List of y values from the fit.
    lm_result : object MinimizerResult
        Results of lm minimization. Includes data such as status and error messages, fit statistics,
        and the updated (i.e., best-fit) parameters themselves in the params attribute.

    """
    # Define the parameters and first guess
    fit_params = lm.Parameters()

    if params_init is None:
        # Prepare initial guess
        mu0 = x[np.argmin(np.abs(np.array(y)-(np.max(y)+np.min(y))/2.))]
        sigma0 = 0.1
        amplitude0 = y.max()-y.min()
        cst0 = y.min()

        # Find the decrease direction
        if y[np.argmin(x)]>y[np.argmax(x)]:
            sign = -1
        else:
            sign = 1

        # Free constraints
        mu_min = -np.inf
        mu_max = np.inf
        sigma_min = -np.inf
        sigma_max = np.inf
        amplitude_min = -np.inf
        amplitude_max = np.inf
        cst_min = -np.inf
        cst_max = np.inf

        # Adding constraints
        """
        mu_min = x.min()
        mu_max = x.max()
        sigma_min = 0.
        sigma_max = x.max()-x.min()
        amplitude_min = 0.
        amplitude_max = (y.max()-y.min())*1.1
        cst_min = -np.inf
        cst_max = y.max()*1.0
        """

    else:
        # Take the parameters given by the user as initial guess
        mu0 = params_init['mu']['init']
        sigma0 = params_init['sigma']['init']
        amplitude0 = params_init['amplitude']['init']
        cst0 = params_init['cst']['init']
        sign = params_init['sign']['value']

        # Parameters with constraints
        mu_min = params_init['mu']['min']
        mu_max = params_init['mu']['max']
        sigma_min = params_init['sigma']['min']
        sigma_max = params_init['sigma']['max']
        amplitude_min = params_init['amplitude']['min']
        amplitude_max = params_init['amplitude']['max']
        cst_min = params_init['cst']['min']
        cst_max = params_init['cst']['max']

    fit_params.add_many(('cst', cst0, True, cst_min, cst_max, None),
                   ('amplitude', amplitude0, True, amplitude_min, amplitude_max, None),
                   ('sigma', sigma0, True, sigma_min, sigma_max, None),
                   ('mu', mu0, True, mu_min, mu_max, None),
                   ('sign', sign, False, None, None, None),
                   )

    # Fit initialisation and fit
    fitter = lm.Minimizer(residuals_erf_function, fit_params, fcn_args=(x, y))
    lm_result = fitter.minimize()
    lm_result.fun_name = 'erf'

    y_fit = erf_function(x, lm_result.params['mu'], lm_result.params['sigma'],
                         lm_result.params['amplitude'], lm_result.params['cst'], lm_result.params['sign'])

    if is_print_info:
        print(lm.fit_report(lm_result))

    return y_fit, lm_result

def process_erf_fit(nxs_name, path_to_nxs_dir,
    x_label, y_label, params_init=None,
    absorbers='', path_to_save_dir='', is_print_stamps=False, is_plot=False,
    is_save=False, is_print_info=False):
    """
    Call functions for extracting, fitting with an error function, plotting, and saving 1d data.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    x_label : str
        Exact name of the x sensor, as it appears in the stamps.
    y_label : str
        Exact name of the y sensor, as it appears in the stamps.
    params_init : dict or None, optional
        Dictionnary of parameters for initial guesses and limits.
        All the parameters should be given. For example:
        params_init = {'cst':{'init':0, 'min':-1, 'max':1},
        'mu':{'init':0, 'min':-1, 'max':1},
        'sigma':{'init':0.2, 'min':0., 'max':1},
        'amplitude':{'init':10., 'min':-10, 'max':20},
        'sign':{'value':-1}}
    absorbers : str, optional
        Text to display which absorber was used.
    path_to_save_dir : str
        Path to the directory where the treated files will be saved.
    is_print_stamps : bool, optional
        Print the list of sensors contained in the nexus file.
    is_plot : bool, optional
        Plot the data.
    is_save : bool, optional
        Save the results.
    is_print_info : bool, optional
        Verbose mode.

    Returns
    -------
    x : array
        List of x values.
    y : array
        List of y values.
    y_fit : array
        List of y values from the fit.
    lm_result : object MinimizerResult
        Results of lm minimization. Includes data such as status and error messages, fit statistics,
        and the updated (i.e., best-fit) parameters themselves in the params attribute.

    """
    x, y, time_str, stamps_0d, data_0d = \
    extract_data_1d(
        nxs_name, path_to_nxs_dir,
        x_label, y_label,
        is_print_stamps, is_print_info
        )

    y_fit, lm_result = fit_with_erf(x, y, params_init, is_print_info)

    fig = None

    if is_plot:
        fig = plot_erf_fit(
            x, y, y_fit, lm_result,
            x_label, y_label, time_str,
            nxs_name, absorbers, is_print_info
            )

    if is_save:
        save_data_1d(
            nxs_name, stamps_0d, data_0d, fig,
            path_to_save_dir, is_print_info
            )

        save_fit_result(
            nxs_name, x, y, y_fit, lm_result,
            x_label, y_label, fig,
            path_to_save_dir, is_print_info
            )

    return x, y, y_fit, lm_result

def plot_erf_fit(
    x, y, y_fit, lm_result,
    x_label, y_label, time_str,
    nxs_name, absorbers, is_print_info):
    """
    Plot fit of 1d data.

    Parameters
    ----------
    x : array
        List of x values.
    y : array
        List of y values.
    y_fit : array
        List of y values from the fit.
    lm_result : object MinimizerResult
        Results of lm minimization. Includes data such as status and error messages, fit statistics,
        and the updated (i.e., best-fit) parameters themselves in the params attribute.
    x_label : str
        Exact name of the x sensor, as it appears in the stamps.
    y_label : str
        Exact name of the y sensor, as it appears in the stamps.
    time_str : str
        Starting/ending dates of the scan.
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    absorbers : str
        Text to display which absorber was used.
    is_print_info : bool
        Verbose mode.

    Returns
    -------
    fig : matplotlib figure
        The figure to be saved in pdf.

    """
    # Print absorbers
    if absorbers != '':
        print("Absorbers: %s"%absorbers)

    if time_str != '':
        print(time_str)

    fig = plt.figure(1, figsize=(12,5))
    ax = fig.add_subplot(111)
    ax.plot(x, y, 'o-', label=nxs_name[nxs_name.rfind('_')+1:nxs_name.rfind('.')])
    ax.plot(x, y_fit, 'r-', lw=2)

    # Plot the associated gaussian function
    ax2 = ax.twinx()
    ax2.plot(x,gaussian_function(x, lm_result.params['mu'], lm_result.params['sigma'],
                                 0.0, 0.0, lm_result.params['amplitude']),
             'b-', lw=1)

    ax.legend(fontsize=16)
    ax.set_xlabel(x_label, fontsize=16)
    ax.set_ylabel(y_label, fontsize=16)
    ax.tick_params(labelsize=16)
    ax.yaxis.offsetText.set_fontsize(16)
    ax2.yaxis.offsetText.set_fontsize(16)
    ax2.tick_params(labelsize=16)
    if lm_result.params['sign']>0:
        fig.text(0.2, 0.65,'Center %3.4f'%(lm_result.params['mu']), fontsize=14)
        fig.text(0.2, 0.60,'FWHM %3.4f'%(2.0*np.sqrt(2.0*np.log(2.))*lm_result.params['sigma']), fontsize=14)
    else:
        fig.text(0.7, 0.60,'Center %3.4f'%(lm_result.params['mu']), fontsize=14)
        fig.text(0.7, 0.55,'FWHM %3.4f'%(2.0*np.sqrt(2.0*np.log(2.))*lm_result.params['sigma']), fontsize=14)
    ax.set_title('Fit with an error function', fontsize=14)


    if is_print_info:
        # Plot first guess
        mu0 = lm_result.params['mu'].init_value
        sigma0 = lm_result.params['sigma'].init_value
        amplitude0 = lm_result.params['amplitude'].init_value
        cst0 = lm_result.params['cst'].init_value
        sign = lm_result.params['sign'].value
        print( mu0, sigma0,amplitude0, cst0, sign)
        ax.plot(x,erf_function(x, mu0, sigma0,
                               amplitude0, cst0, sign),
                'k--', lw=1)

    plt.show()

    return fig

######################################################
######################################################
######################################################
################# FITTING FUNCTIONS ##################
######################################################
######################################################
######################################################

def gaussian_function(x, mu, sigma, cst, linear_coeff, amplitude):
    """
    Return a gaussian with a linear background.
    """
    gau_arg = (x-mu)/(sigma*np.sqrt(2.0))

    # Careful, having sigma in the amplitude may mislead the fit
    #result=(1/(sigma*np.sqrt(2.0*np.pi)))*np.exp(-result*result)

    gau_fun = np.exp(-gau_arg*gau_arg)

    return cst + linear_coeff*x + amplitude*gau_fun

def residuals_gaussian_function(params, x, y):
    """
    Return the residuals of the gaussian function.
    """
    residuals = y - gaussian_function(x, params['mu'], params['sigma'],
        params['cst'], params['linear_coeff'], params['amplitude'])

    return residuals

def erf_function(x, mu, sigma, amplitude, cst, sign):
    """
    Return an error function with a constant background.
    """
    erf_arg = sign*(x-mu)/(sigma*np.sqrt(2.))
    # erf is defined as 1-erf in scipy
    erf_fun = (1.+erf(erf_arg))

    return cst + 0.5*amplitude*erf_fun

def residuals_erf_function(params, x, y):
    """
    Return the residuals of the error function.
    """
    residuals = y - erf_function(x, params['mu'], params['sigma'],
        params['amplitude'], params['cst'], params['sign'])

    return residuals


######################################################
######################################################
######################################################
################ CALIBRATION ENERGY ##################
######################################################
######################################################
######################################################


def process_energy_calib(nxs_name, path_to_nxs_dir,
    xas_energy_min, xas_energy_max, xas_energy_shift,
    xas_signal_label, xas_norm_label, xas_standard_name, energy_current, bragg_current,
    path_to_xas_dir, path_to_save_dir='', is_save=False):
    """
    Call functions for extracting, plotting, and saving energy calibration
    with XAS standards.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    xas_energy_min : float
        Energy minimum for the plot in keV.
    xas_energy_max : float
        Energy maximum for the plot in keV.
    xas_energy_shift : float
        Energy shift between the measurement and the standard in keV.
    xas_signal_label : str
        Exact name of the sensor used for signal, as it appears in the stamps.
    xas_norm_label : str
        Exact name of the sensor used for normalization, as it appears in the stamps.
    xas_standard_name : str
        Chosen xas standard file, e.g. 'XAS_ref_Ni.txt'.
    energy_current : float
        Current value of the DCM energy in keV.
    bragg_current : float
        Current value of the bragg angle in deg.
    path_to_xas_dir : str
        Path to the folder containing the standards.
    path_to_save_dir : str
        Path to the directory where the treated files will be saved.
    is_save : bool, optional
        Save the results.

    """
    _, _, _, stamps_0d, data_0d = \
    extract_data_1d(
        nxs_name, path_to_nxs_dir,
        'energydcm', xas_signal_label,
        is_print_stamps=False, is_print_info=False
        )

    bragg_new, xas_energy_standard, xas_transmission_standard, \
    xas_energy_meas, xas_transmission_meas, str_to_display = \
    compute_bragg_new(
        stamps_0d, data_0d, xas_energy_shift,
        xas_signal_label, xas_norm_label, xas_standard_name,
        energy_current, bragg_current, path_to_xas_dir
        )

    fig = plot_energy_calib(
        xas_standard_name, xas_energy_min, xas_energy_max, xas_energy_shift,
        xas_energy_meas, xas_transmission_meas,
        xas_energy_standard, xas_transmission_standard, str_to_display
        )

    if is_save:
        # We only save the figure and the .dat file
        save_data_1d(
            nxs_name, stamps_0d, data_0d, fig,
            path_to_save_dir, is_print_info=False
            )


def compute_bragg_new(stamps_0d, data_0d, xas_energy_shift,
    xas_signal_label, xas_norm_label, xas_standard_name,
    energy_current, bragg_current, path_to_xas_dir):
    """
    Compute the new bragg value based on the energy shift with the standard.

    Parameters
    ----------
    stamps_0d : array
        Aliases of each 0D sensor in the scan.
    data_0d : array
        Values of each 0D sensor in the scan.
    xas_energy_shift : float
        Energy shift between the measurement and the standard in keV.
    xas_signal_label : str
        Exact name of the sensor used for signal, as it appears in the stamps.
    xas_norm_label : str
        Exact name of the sensor used for normalization, as it appears in the stamps.
    xas_standard_name : str
        Chosen xas standard file, e.g. 'XAS_ref_Ni.txt'.
    energy_current : float
        Current value of the DCM energy in keV.
    bragg_current : float
        Current value of the bragg angle in deg.
    path_to_xas_dir : str
        Path to the folder containing the standards.

    Returns
    -------
    bragg_new : float
        Value of the new bragg in deg.
    xas_energy_standard : array
        List of energies from the standard in keV.
    xas_transmission_standard : array
        List of transmissions from the standard.
    xas_energy_meas : array
        List of energies measured in keV.
    xas_transmission_meas : array
        List of transmissions measured in keV.
    str_to_display : str or None
        String to display after the plot.

    """
    # Extract the energy (in keV) and transmission from the standard files
    xas_energy_standard = np.genfromtxt(path_to_xas_dir+xas_standard_name,
                                        dtype = 'float64', delimiter = '\t')[:,0]/1000.
    xas_transmission_standard = np.genfromtxt(path_to_xas_dir+xas_standard_name,
                                        dtype = 'float64', delimiter = '\t')[:,2]

    # Extract the energy (in keV) measured
    sensor_list = [stamps_0d[i][0] if stamps_0d[i][1] is None else stamps_0d[i][1] for i in range(len(stamps_0d))]
    energydcm_arg = sensor_list.index('energydcm')
    xas_energy_meas = data_0d[energydcm_arg]

    # Extract and normalize the measured transmission data
    signal_arg = sensor_list.index(xas_signal_label)
    if xas_norm_label != 'No norm.':
        norm_arg = sensor_list.index(xas_norm_label)
        xas_transmission_meas = data_0d[signal_arg]/data_0d[norm_arg]
    else:
        xas_transmission_meas = data_0d[signal_arg]

    bragg_new = bragg_current
    str_to_display = None

    if (energy_current!=0 and bragg_current!=0):
        bragg_new = 180./np.pi*np.arcsin(energy_current/(energy_current+xas_energy_shift)*np.sin(bragg_current*np.pi/180.))
        str_to_display = (
            "Current value of braggdcm = %g\n"
            "Current value of energydcm = %g\n"
            "Do at the current energy: setposition braggdcm %g"%(
                bragg_current,
                energy_current,
                bragg_new
                )
        )

    return bragg_new, xas_energy_standard, xas_transmission_standard, \
           xas_energy_meas, xas_transmission_meas, str_to_display


def plot_energy_calib(xas_standard_name, xas_energy_min, xas_energy_max, xas_energy_shift,
        xas_energy_meas, xas_transmission_meas,
        xas_energy_standard, xas_transmission_standard, str_to_display):
    """
    Plot the comparison with the xas standard and print the new bragg.

    Parameters
    ----------
    xas_standard_name : str
        Chosen xas standard file, e.g. 'XAS_ref_Ni.txt'.
    xas_energy_min : float
        Energy minimum for the plot in keV.
    xas_energy_max : float
        Energy maximum for the plot in keV.
    xas_energy_shift : float
        Energy shift between the measurement and the standard in keV.
    xas_energy_meas : array
        List of energies measured in keV.
    xas_transmission_meas : array
        List of transmissions measured in keV.
    xas_energy_standard : array
        List of energies from the standard in keV.
    xas_transmission_standard : array
        List of transmissions from the standard.
    str_to_display : str or None
        String to display after the plot.

    Returns
    -------
    fig : matplotlib figure
        The figure to be saved in pdf.

    """
    fig=plt.figure(1, figsize=(15,5))
    ax1=fig.add_subplot(111)
    ax1.plot(xas_energy_standard, xas_transmission_standard, 'k.-',
        label = xas_standard_name.split('/')[-1])
    ax1.plot([], [], 'r.-', label = 'Data shifted of %s keVs'%xas_energy_shift)
    ax1.set_xlim(xas_energy_min, xas_energy_max)
    ax1.set_xlabel('Energy (keV)', fontsize=14)
    ax1.set_ylabel('Transmission (a.u.)', fontsize=14)
    ax1.tick_params(labelsize=16)
    ax1.yaxis.offsetText.set_fontsize(16)
    ax1.legend()
    ax2 = ax1.twinx()
    ax2.tick_params(labelsize=16)
    ax2.plot(xas_energy_meas+xas_energy_shift, xas_transmission_meas, 'r.-')
    plt.title('Calibration in energy', fontsize = 16)
    plt.show()

    if str_to_display:
        print(str_to_display)

    return fig
