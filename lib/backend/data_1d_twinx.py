"""
Library for data 1D. With two y axis on the same x axis.
"""
import os
import datetime
import numpy as np
import matplotlib.pyplot as plt
import lmfit as lm
from scipy.special import erf
from lib.backend import PyNexus as PN

_RED='\x1b[31;01m'
_BLUE='\x1b[34;01m'
_RESET='\x1b[0m'


######################################################
######################################################
######################################################
############# PROCESS DATA 1D ########################
######################################################
######################################################
######################################################

def process_data_1d(
        nxs_name, path_to_nxs_dir,
        x_label, y_label, y2_label, is_logx=False, is_logy=False, is_logy2=False,
        absorbers='', path_to_save_dir='', is_print_stamps=False, is_plot=False,
        is_save=False, is_print_info=False):
    """
    Call functions for extracting, plotting, and saving 1d data.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    x_label : str
        Exact name of the x sensor, as it appears in the stamps.
    y_label : str
        Exact name of the y sensor, as it appears in the stamps.
    y2_label : str
        Exact name of the y2 sensor, as it appears in the stamps.
    is_logx : bool, optional
        Log scale on the x axis.
    is_logy : bool, optional
        Log scale on the y axis.
    is_logy2 : bool, optional
        Log scale on the y2 axis.
    absorbers : str, optional
        Text to display which absorber was used.
    path_to_save_dir : str
        Path to the directory where the treated files will be saved.
    is_print_stamps : bool, optional
        Print the list of sensors contained in the nexus file.
    is_plot : bool, optional
        Plot the data.
    is_save : bool, optional
        Save the results.
    is_print_info : bool, optional
        Verbose mode.

    Returns
    -------
    x : array
        List of x values.
    y : array
        List of y values.
    y2 : array
        List of y2 values.

    """
    x, y, y2, time_str, stamps_0d, data_0d = \
    extract_data_1d(
        nxs_name, path_to_nxs_dir,
        x_label, y_label, y2_label,
        is_print_stamps, is_print_info
        )

    fig = None

    if is_plot:
        fig = plot_data_1d(
            x, y, y2, x_label, y_label, y2_label, time_str,
            nxs_name, absorbers, is_logx, is_logy, is_logy2,
            )

    if is_save:
        save_data_1d(
            nxs_name, stamps_0d, data_0d, fig,
            path_to_save_dir, is_print_info
            )

    return x, y, y2

def extract_data_1d(
    nxs_name, path_to_nxs_dir,
    x_label, y_label, y2_label,
    is_print_stamps, is_print_info):
    """
    Extract the nexus file and return useful quantities.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    x_label : str
        Exact name of the x sensor, as it appears in the stamps.
    y_label : str
        Exact name of the y sensor, as it appears in the stamps.
    y2_label : str
        Exact name of the y2 sensor, as it appears in the stamps.
    is_print_stamps : bool, optional
        Print the list of sensors contained in the nexus file.
    is_print_info : bool, optional
        Verbose mode.

    Returns
    -------
    x : array
        List of x values.
    y : array
        List of y values.
    y2 : array
        List of y2 values.
    time_str : str
        Starting/ending dates of the scan.
    stamps_0d : array
        Aliases of each 0D sensor in the scan.
    data_0d : array
        Values of each 0D sensor in the scan.

    Raises
    ------
    FileNotFoundError
        If a file or a folder is missing.
    Exception('Could not open Nexus file.'))
        If the Nexus file cannot be accessed.
    Exception('Sensor not found.')
        If the sensor is not found in the sensor list.

    """
    nxs_path = path_to_nxs_dir+nxs_name

    if not os.path.isfile(nxs_path):
        print(_RED+'Scan %s is not present in the file directory.'%(nxs_name)+_RESET)
        print('\t\t file directory : %s'%path_to_nxs_dir)
        raise FileNotFoundError('Missing folder or file.')

    column_epoch = None

    if is_print_info:
        print(_BLUE+" - Open Nexus Data File :"+ _RESET)
        print('\t%s'%nxs_path)

    try:
        nexus = PN.PyNexusFile(nxs_path, fast=True)
    except:
        print(_RED+'\t Could not open Nexus file.'+_RESET)
        raise Exception('Could not open Nexus file.')

    nbpts = int(nexus.get_nbpts())
    if is_print_info:
        print("\t. Number of data points: %s"%nbpts)

    # Extract stamps
    stamps = nexus.extractStamps()
    if is_print_stamps :
        print("\t. Available Counters:")
        for i, stamp in enumerate(stamps):
            if stamp[1] is not None:
                print("\t\t%s  -------> %s"%(i,stamp[1]))
            else:
                print("\t\t%s  -------> %s"%(i,stamp[0]))

    # Extract 0D data
    stamps_0d, data_0d = nexus.extractData('0D')

    nexus.close()

    # Find columns
    for i, stamp_0d in enumerate(stamps_0d):
        if stamp_0d[1] is None:
            if stamp_0d[0].lower()=='sensorstimestamps':
                column_epoch = i

    sensor_list = [stamps_0d[i][0] if stamps_0d[i][1] is None else stamps_0d[i][1] for i in range(len(stamps_0d))]

    if x_label in sensor_list:
        x_arg = sensor_list.index(x_label)
    else:
        print(_RED+'\t Sensor %s is not in the sensor list'%(x_label)+_RESET)
        raise Exception('Sensor not found.')

    if y_label in sensor_list:
        y_arg = sensor_list.index(y_label)
    else:
        print(_RED+'\t Sensor %s is not in the sensor list'%(y_label)+_RESET)
        raise Exception('Sensor not found.')

    if y2_label in sensor_list:
        y2_arg = sensor_list.index(y2_label)
    else:
        print(_RED+'\t Sensor %s is not in the sensor list'%(y2_label)+_RESET)
        raise Exception('Sensor not found.')

    # Find positions of non Nan values based on data_0d[x_arg]
    args = ~np.isnan(data_0d[x_arg])

    # Get first and last position of the non NaN values
    if is_print_info:
        print('\t. Valid data between points %d and %d'
                      %(np.argmax(args), len(args)-np.argmax(args[::-1])-1))

    # Extract time
    time_str = ''
    if column_epoch is not None:
        epoch_time =  data_0d[column_epoch][args]
        time_str = 'Starting time: %s\nEnding time: %s'%(
                   datetime.datetime.fromtimestamp(epoch_time[0]),
                   datetime.datetime.fromtimestamp(epoch_time[-1]))

    # Take only the non NaN values
    x = data_0d[x_arg][args]
    y = data_0d[y_arg][args]
    y2 = data_0d[y2_arg][args]

    return x, y, y2, time_str, stamps_0d, data_0d

def plot_data_1d(
    x, y, y2, x_label, y_label, y2_label, time_str,
    nxs_name, absorbers, is_logx, is_logy, is_logy2):
    """
    Plot 1d data.

    Parameters
    ----------
    x : array
        List of x values.
    y : array
        List of y values.
    y2 : array
        List of y2 values.
    x_label : str
        Exact name of the x sensor, as it appears in the stamps.
    y_label : str
        Exact name of the y sensor, as it appears in the stamps.
    y2_label : str
        Exact name of the y2 sensor, as it appears in the stamps.
    time_str : str
        Starting/ending dates of the scan.
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    absorbers : str, optional
        Text to display which absorber was used.
    is_logx : bool, optional
        Log scale on the x axis.
    is_logy : bool, optional
        Log scale on the y axis.
    is_logy2 : bool, optional
        Log scale on the y2 axis.

    Returns
    -------
    fig : matplotlib figure
        The figure to be saved in pdf.

    """
    # Print absorbers
    if absorbers != '':
        print("Absorbers: %s"%absorbers)

    if time_str != '':
        print(time_str)

    fig = plt.figure(1, figsize=(12,5))
    ax1 = fig.add_subplot(111)

    ax1.plot(x, y, 'bo-')
    ax1.yaxis.label.set_color('blue')
    ax1.tick_params(axis='y', colors='blue')
    ax1.set_xlabel(x_label, fontsize=16)
    ax1.set_ylabel(y_label, fontsize=16)
    ax1.tick_params(labelsize=16)
    ax1.yaxis.offsetText.set_fontsize(16)
    # Avoid strange offsets on the number range
    ax1.get_yaxis().get_major_formatter().set_useOffset(False)
    ax1.get_xaxis().get_major_formatter().set_useOffset(False)

    if is_logy:
        ax1.set_yscale('log')
    if is_logx:
        ax1.set_xscale('log')


    ax2 = ax1.twinx()
    ax2.plot(x, y2, 'rx-')
    ax2.yaxis.label.set_color('red')
    ax2.tick_params(axis='y', colors='red')
    ax2.set_ylabel(y2_label, fontsize=16)
    ax2.tick_params(labelsize=16)
    
    ax2.yaxis.offsetText.set_fontsize(16)
    # Avoid strange offsets on the number range
    ax2.get_yaxis().get_major_formatter().set_useOffset(False)
    ax2.get_xaxis().get_major_formatter().set_useOffset(False)

    if is_logy2:
        ax2.set_yscale('log')

    suptitle = nxs_name[nxs_name.rfind('/')+1:]
    fig.suptitle(suptitle, fontsize='x-large')

    plt.show()

    return fig

def save_data_1d(
    nxs_name, stamps_0d, data_0d, fig,
    path_to_save_dir, is_print_info):
    """
    Save 1D data.

    XXX.dat : the value of each sensor at each point of the scan.
    XXX.pdf : the figure in pdf.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    stamps_0d : array
        Aliases of each 0D sensor in the scan.
    data_0d : array
        Values of each 0D sensor in the scan.
    fig : None or matplotlib figure
        The figure to be saved in pdf. Pass None if not wanted.
    path_to_save_dir : str
        Path to the directory where the treated files will be saved.
    is_print_info : bool
        Verbose mode.

    """
    # Create Save Name
    save_name = path_to_save_dir + nxs_name[:nxs_name.rfind('.nxs')]

    # Save 0D data
    header_0d = [stamp[1] if stamp[1] is not None else stamp[0] for stamp in stamps_0d]
    header_0d = '\t'.join(header_0d)

    np.savetxt(save_name+'.dat', np.transpose(data_0d), header = header_0d,
               comments = '', fmt='%s', delimiter = '\t')

    if is_print_info:
        print('\t. Scalar data saved in:')
        print('\t%s.dat'%save_name)

    # Figure in pdf
    if fig is not None:
        plt.figure(fig)
        plt.savefig(save_name+'.pdf', bbox_inches='tight')
        plt.close()

        if is_print_info:
            print('\t. Figure saved in:')
            print('\t%s.pdf'%save_name)

