"""
Library for GIXD.
"""
import sys
import os
import datetime
import numpy as np
import matplotlib.pyplot as plt
from lib.backend import PyNexus as PN
from lib.frontend import jlb_io

_RED='\x1b[31;01m'
_BLUE='\x1b[34;01m'
_RESET='\x1b[0m'

def calib_thetaz(gamma_channel, is_plot=True):
    """
    Fit and plot gamma vs channel for the calibration of thetaz.

    Parameters
    ----------
    gamma_channel : array
        Numpy array containing the values of each gamma and corresponding channel.
        For example :
        np.array([
        [0,      970],
        [-1,     899],
        [-2,     827]])
    is_plot : bool, optional
        Plot the fit.

    Returns
    -------
    thetaz_factor : float
        Factor for conversion from channel to radian in the vertical direction (rad/channel).

    """
    # x_data: gamma
    # y_data: channel
    x_data = gamma_channel[:,0]
    y_data = gamma_channel[:,1]
    x_plot = np.linspace(x_data.min(), x_data.max(), 100)

    # Fit with y_data = coeff[0]*x_data + coeff[1]
    coeff = np.polyfit(x_data,y_data,1)
    poly1d_fn = np.poly1d(coeff)

    thetaz_factor = (np.pi/coeff[0])/180.0

    if is_plot:
        fig = plt.figure(1, figsize=(10,5))
        ax = fig.add_subplot(111)
        ax.plot(x_data, y_data, 'bo')
        ax.plot(x_plot, poly1d_fn(x_plot), 'r-', lw=2)

        ax.set_title('1D detector channel calibration')
        ax.set_xlabel('Gamma (deg)', fontsize=16)
        ax.set_ylabel('Channels', fontsize=16)

        ax.tick_params(labelsize=16)
        ax.yaxis.offsetText.set_fontsize(16)

        fig.text(0.2, .8, "%3.5g channels per degree"%(coeff[0]), fontsize=14)
        fig.text(0.2, .75, "%3.5g degrees per channel"%(1.0/coeff[0]), fontsize=14)
        fig.text(0.2, .7, "%3.5g radians per channel"%((np.pi/coeff[0])/180.0), fontsize=14)

        plt.show()

    return thetaz_factor

def bin_matrix_vertical(mat, bin_size=10):
    """
    Bin a matrix along the vertical axis.

    Parameters
    ----------
    mat : array
        Numpy array with the matrix to bin.
    bin_size : int, optional
        Size in pixels of the vertical binning.

    Returns
    -------
    (channels_binned, mat_binned) : tupple of arrays
        Channels and matrix after binning.

    """
    mat_binned = []

    for i in range(mat.shape[0]):
        channels_binned = []
        z = []
        j = 0
        while j+bin_size < mat.shape[1]:
            channels_binned.append(j+float(bin_size)/2.0)
            z.append(mat[i, j:j+bin_size].sum())
            j += bin_size
        mat_binned.append(z)

    return (np.array(channels_binned), np.array(mat_binned))

def process_vineyard(nxs_name, path_to_nxs_dir,
        bin_size=10, is_force_gamma=False, fgamma=0.,
        is_gixd_logx=False, is_gixd_logy=False, is_gixd_logz=False,
        nb_levels=50, map_gixd='jet', roi_soller=[510, 350, 130, 692],
        selected_sensors = None,
        is_print_stamps=False, is_plot=False, is_print_info=False, is_raster=False):
    """
    Extract the nexus file, plot and return the channel of the Vineyard's peak.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    bin_size : int, optional
        Size in pixels of the vertical binning (along qz).
    is_force_gamma : bool, optional
        Force gamma to be equal to the value of `fgamma`.
    fgamma : float, optional
        Value of gamma (deg) to be used if is_force_gamma is set to True
        or if gamma is absent from the sensor list.
    is_gixd_logx : bool, optional
        Log scale on the x axis of the integrated profile.
    is_gixd_logy : bool, optional
        Log scale on the y axis of the integrated profile.
    is_gixd_logz : bool, optional
        Log scale on the color scale of the image.
    nb_levels : int, optional
        Number of color levels for the image display.
    map_gixd : str, optional
        Colormap of the image.
    roi_soller : list of int, optional
        ROI of the Soller's slits.
    selected_sensors : list of str, optional
        List of aliases of sensors, for their mean value to be added to the plot.
    is_print_stamps : bool, optional
        Print the list of sensors contained in the nexus file.
    is_plot : bool, optional
        Plot the 2D GIXD image and the integrated profiles.
    is_print_info : bool, optional
        Verbose mode.

    Returns
    -------
    channel0 : int
        Vertical channel corresponding to the Vineyard's peak.

    """
    x, y, x_label, y_label, time_str, sensors_str, _,\
    integ_rod, integ_rod_top, integ_rod_bottom, integ_rod_first_quarter,\
    mat, mat_binned, _, mean_pi, _, mean_gamma, _, _ = extract_gixd_scan(
        nxs_name, path_to_nxs_dir,
        0., 1., 1., 1.,
        bin_size, False,
        is_force_gamma, fgamma, roi_soller,
        selected_sensors,
        is_print_stamps, is_print_info
        )

    # Extract the channel of the Vineyard peak
    integ_over_qxy = mat.sum(axis=0)
    channel0 = int(integ_over_qxy.argmax())

    if is_plot:
        plot_gixd_scan(
            x, y, x_label, y_label, time_str, sensors_str,
            integ_rod, integ_rod_top, integ_rod_bottom, integ_rod_first_quarter,
            mat_binned, mean_pi, mean_gamma,
            nxs_name, '', is_gixd_logx, is_gixd_logy, is_gixd_logz,
            nb_levels, map_gixd, is_raster
            )

        plot_vineyard(integ_over_qxy, channel0)

    return channel0

def plot_vineyard(integ_over_qxy, channel0):
    """
    Plot the profile integrated over qxy that was used to find the Vineyard's peak.

    Parameters
    ----------
    integ_over_qxy : array
        GIXD image integrated over the qxy axis.
    channel0 : int
        Vertical channel corresponding to the Vineyard's peak.

    """
    fig = plt.figure(1, figsize=(12,8))
    ax = fig.add_axes([0.125, 0.13, 0.775, 0.37])
    ax.plot(integ_over_qxy)

    ax.text(channel0*0.95, integ_over_qxy[channel0],
             'Channel of Vineyard Peak ($\\mathregular{\\theta_c}$): %d'%(int(channel0)),
             fontsize='x-large', horizontalalignment='right', color='red')
    plt.plot((channel0, channel0), (integ_over_qxy[channel0]*1.1, integ_over_qxy[channel0]*1.3), 'r-', lw=2)
    ax.set_xlabel('channels', fontsize='large')
    ax.set_ylabel('Q$\\mathregular{_{xy}}$ - Integrated Intensity', fontsize='large')
    plt.show()

    print(_RED+'Data not saved. To save data, run a GIXD on the scan.'+_RESET)
    print(_RED+'Channel0: %g'%channel0+_RESET)

def process_gixd_scan(
        nxs_name, path_to_nxs_dir,
        channel0, thetaz_factor, wavelength, thetac,
        bin_size, is_compute_qz,
        is_force_gamma=False, fgamma=0.,
        selected_sensors = None,
        absorbers='', is_gixd_logx=False, is_gixd_logy=False, is_gixd_logz=False,
        nb_levels=50, map_gixd='jet',
        path_to_save_dir='', list_moy_to_create=[10, 20, 40], roi_soller=[510, 350, 130, 692],
        is_print_stamps=False, is_plot=False, is_save=False, is_print_info=False, is_raster=False):
    """
    Call functions for extracting, plotting, and saving a GIXD scan.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    channel0 : int
        Vertical channel corresponding to the Vineyard's peak.
    thetaz_factor : float
        Factor for conversion from channel to radian in the vertical direction (rad/channel).
    wavelength : float
        Wavelength in nm.
    thetac : float
        Critical angle in rad.
    bin_size : int
        Size in pixels of the vertical binning (along qz).
    is_compute_qz : bool
        Convert pixels to qz in the vertical direction.
    is_force_gamma : bool, optional
        Force gamma to be equal to the value of `fgamma`.
    fgamma : float, optional
        Value of gamma (deg) to be used if is_force_gamma is set to True
        or if gamma is absent from the sensor list.
    selected_sensors : list of str, optional
        List of aliases of sensors, for their mean value to be added to the plot.
    absorbers : str, optional
        Text to display which absorber was used.
    is_gixd_logx : bool, optional
        Log scale on the x axis of the integrated profile.
    is_gixd_logy : bool, optional
        Log scale on the y axis of the integrated profile.
    is_gixd_logz : bool, optional
        Log scale on the color scale of the image.
    nb_levels : int, optional
        Number of color levels for the image display.
    map_gixd : str, optional
        Colormap of the image.
    path_to_save_dir : str, optional
        Path to the directory where the treated files will be saved.
    list_moy_to_create : list of int, optional
        Bin sizes to be used, e.g. [10, 20, 40].
    roi_soller : list of int, optional
        ROI of the Soller's slits.
    is_print_stamps : bool, optional
        Print the list of sensors contained in the nexus file.
    is_plot : bool, optional
        Plot the 2D GIXD image and the integrated profiles.
    is_save : bool, optional
        Save the results.
    is_print_info : bool, optional
        Verbose mode.
    is_raster : bool
        Rasterization of the image.

    Returns
    -------
    x : array
        Either qxy (nm^-1) values, if qxy is available in the list of sensors,
        or delta (deg) if not.
    y : array
        Either qz (nm^-1) values, if is_compute_qz is True,
        or vertical channels if not.
    integ_rod : array
        Rods integrated over the whole vertical axis of the detector.
    integ_rod_top : array
        Rods integrated over the top half vertical axis of the detector.
    integ_rod_bottom : array
        Rods integrated over the bottom half vertical axis of the detector.
    integ_rod_first_quarter : array
        Rods integrated over the bottom quarter vertical axis of the detector.
    mat : array
        Each line corresponds to a position of delta.
        It is the matrix corresponding to the image displayed in plot.
    mat_binned : array
        Matrix after vertical binning.
    channels_binned : array
        Channels after vertical binning.
    mean_pi : float or None
        Average of the surface pressure (mN/m) over the scan (None if pressure not found).
    mean_area : float or None
        Average of the area per molecule (nm^2) over the scan (None if area not found).
    mean_gamma : float or None
        Average of gamma (deg) over the scan (None if gamma not found).

    """
    x, y, x_label, y_label, time_str, sensors_str, column_x,\
    integ_rod, integ_rod_top, integ_rod_bottom, integ_rod_first_quarter,\
    mat, mat_binned, channels_binned, \
    mean_pi, mean_area, mean_gamma, stamps_0d, data_0d = \
    extract_gixd_scan(
        nxs_name, path_to_nxs_dir,
        channel0, thetaz_factor, wavelength, thetac,
        bin_size, is_compute_qz,
        is_force_gamma, fgamma, roi_soller,
        selected_sensors,
        is_print_stamps, is_print_info
        )

    fig = None

    # Avoid crash of the cell if 2D detector is not present
    if x is None:
        is_plot = False
        is_save = False

    if is_plot:
        fig = plot_gixd_scan(
            x, y, x_label, y_label, time_str, sensors_str,
            integ_rod, integ_rod_top, integ_rod_bottom, integ_rod_first_quarter,
            mat_binned, mean_pi, mean_gamma,
            nxs_name, absorbers, is_gixd_logx, is_gixd_logy, is_gixd_logz,
            nb_levels, map_gixd, is_raster
            )

    if is_save:
        save_gixd_scan(
            x, integ_rod, integ_rod_top, integ_rod_bottom, integ_rod_first_quarter,
            mat, list_moy_to_create, mean_gamma, column_x,
            channel0, thetaz_factor, wavelength, thetac,
            nxs_name, stamps_0d, data_0d, fig, path_to_save_dir,
            is_compute_qz, is_print_info
            )

    return x, y,\
           integ_rod, integ_rod_top, integ_rod_bottom, integ_rod_first_quarter,\
           mat, mat_binned, channels_binned,\
           mean_pi, mean_area, mean_gamma


def extract_gixd_scan(
    nxs_name, path_to_nxs_dir,
    channel0, thetaz_factor, wavelength, thetac,
    bin_size, is_compute_qz,
    is_force_gamma, fgamma, roi_soller,
    selected_sensors,
    is_print_stamps, is_print_info):
    """
    Extract the nexus file and return useful quantities for GIXD.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    channel0 : int
        Vertical channel corresponding to the Vineyard's peak.
    thetaz_factor : float
        Factor for conversion from channel to radian in the vertical direction (rad/channel).
    wavelength : float
        Wavelength in nm.
    thetac : float
        Critical angle in rad.
    bin_size : int
        Size in pixels of the vertical binning (along qz).
    is_compute_qz : bool
        Convert pixels to qz in the vertical direction.
    is_force_gamma : bool
        Force gamma to be equal to the value of `fgamma`.
    fgamma : float
        Value of gamma (deg) to be used if is_force_gamma is set to True
        or if gamma is absent from the sensor list.
    roi_soller : list of int
        ROI of the Soller's slits.
    selected_sensors : list of str
        List of aliases of sensors, for their mean value to be added to the plot.
    is_print_stamps : bool
        Print the list of sensors contained in the nexus file.
    is_print_info : bool
        Verbose mode.

    Returns
    -------
    x : array
        Either qxy (nm^-1) values, if qxy is available in the list of sensors,
        or delta (deg) if not.
    y : array
        Either qz (nm^-1) values, if is_compute_qz is True,
        or vertical channels if not.
    x_label : str
        'qxy' or 'delta' (useful for plot).
    y_label : str
        'qz' or 'channels' (useful for plot).
    time_str : str
        Starting/ending dates of the scan.
    sensors_str : str
        Label with mean values of selected sensors.
    column_x :
        Column corresponding to the x values in stamps_0d (useful for save).
    integ_rod : array
        Rods integrated over the whole vertical axis of the detector.
    integ_rod_top : array
        Rods integrated over the top half vertical axis of the detector.
    integ_rod_bottom : array
        Rods integrated over the bottom half vertical axis of the detector.
    integ_rod_first_quarter : array
        Rods integrated over the bottom quarter vertical axis of the detector.
    mat : array
        Matrix with each line corresponding to a position of delta.
    mat_binned : array
        Matrix after vertical binning.
    channels_binned : array
        Channels after vertical binning.
    mean_pi : float or None
        Average of the surface pressure (mN/m) over the scan (None if pressure not found).
    mean_area : float or None
        Average of the area per molecule (nm^2) over the scan (None if area not found).
    mean_gamma : float or None
        Average of gamma (deg) over the scan (None if gamma not found).
    stamps_0d : array
        Aliases of each 0D sensor in the scan.
    data_0d : array
        Values of each 0D sensor in the scan.

    Raises
    ------
    FileNotFoundError
        If a file or a folder is missing.
    SystemExit('Could not open Nexus file.'))
        If the Nexus file cannot be accessed.
    SystemExit('delta or qxy not found.')
        If delta and qxy are not found in the sensor list.
    SystemExit('gamma not found.')
        If gamma is not found in the sensor list and is_compute_qz is True.

    """
    nxs_path = path_to_nxs_dir+nxs_name

    if not os.path.isfile(nxs_path):
        print(_RED+'Scan %s is not present in the file directory.'%(nxs_name)+_RESET)
        print('\t\t file directory : %s'%path_to_nxs_dir)
        raise FileNotFoundError('Missing folder or file.')

    column_z = None
    column_qxy = None
    column_delta = None
    column_pi = None
    column_area = None
    column_gamma = None
    column_epoch = None

    if is_print_info:
        print(_BLUE+" - Open Nexus Data File :"+ _RESET)
        print('\t%s'%nxs_path)

    try:
        nexus = PN.PyNexusFile(nxs_path, fast=True)
    except:
        print(_RED+'\t Could not open Nexus file.'+_RESET)
        raise Exception('Could not open Nexus file.')

    nbpts = int(nexus.get_nbpts())
    if is_print_info:
        print("\t. Number of data points: %s"%nbpts)

    # Extract stamps
    stamps = nexus.extractStamps()
    if is_print_stamps :
        print("\t. Available Counters:")
    for i, stamp in enumerate(stamps):
        if stamp[1] is not None:
            if is_print_stamps :
                print("\t\t%s  -------> %s"%(i,stamp[1]))
            if stamp[1].lower() in ['pilatus', 'ufxc', 'ufx', 'pilatus_image', 'ufxc_image']:
                column_z = i
                alias_detector = stamp[1].lower()
        else:
            if is_print_stamps :
                print("\t\t%s  -------> %s"%(i,stamp[0]))

    # Extract 0D data
    stamps_0d, data_0d = nexus.extractData('0D')

    # Find columns
    for i, stamp_0d in enumerate(stamps_0d):
        if stamp_0d[1] is not None:
            if stamp_0d[1].lower()=='delta':
                column_delta = i
            if stamp_0d[1].lower()=='qxy':
                column_qxy = i
            if stamp_0d[1].lower()=='surfacepressure':
                column_pi = i
            if stamp_0d[1].lower()=='areapermolecule':
                column_area = i
            if stamp_0d[1].lower()=='gamma':
                column_gamma = i
        else:
            if stamp_0d[0].lower()=='sensorstimestamps':
                column_epoch = i

    # Check that 2D images are present
    is_images_present = False
    if column_z is not None:
        is_images_present = True
        if is_print_info:
            print('\t. 2D detector data found, (column %d, alias %s)'%(column_z, stamps[column_z][1]))
    else:
        print(_RED+'\t. No 2D detector data found'+_RESET)
        nexus.close()
        #raise Exception('pilatus not found.')


    # Check what is the x input (qxy or delta)
    if column_qxy is None:
        if column_delta is None:
            # If both qxy and delta are missing, the scan cannot be processed
            print(_RED+'\t. No usual actuator for GIXD found, stop here'+_RESET)
            nexus.close()
            raise Exception('delta or qxy not found.')

        column_x = column_delta
        if is_print_info:
            print('\t. delta data found, (column %d, alias %s)'%(column_x, stamps[column_x][1]))

    else:
        column_x = column_qxy
        if is_print_info:
            print('\t. qxy data found, (column %d, alias %s)'%(column_x, stamps[column_x][1]))

    # Find positions of non Nan values based on data[0]
    args = ~np.isnan(data_0d[0])

    # Get first and last position of the non NaN values
    if is_print_info:
        print('\t. Valid data between points %d and %d'
                      %(np.argmax(args), len(args)-np.argmax(args[::-1])-1))

    # Compute and print mean values
    if column_pi is not None:
        mean_pi = data_0d[column_pi][args].mean()
        if is_print_info:
            print('\t. Surface pressure data found, mean value %3.4g ± %3.4g mN/m'
                          %(mean_pi,data_0d[column_pi][args].std() ))
    else:
        print('\t. No surface pressure data found')
        mean_pi = None

    if column_area is not None:
        mean_area = data_0d[column_area][args].mean()
        if is_print_info:
            print('\t. Area per molecule data found, mean value %3.4g ± %3.4g nm2 per molecule'
                          %(mean_area, data_0d[column_area][args].std()))
    else:
        print('\t. No area per molecule data found')
        mean_area = None

    # Take the value of gamma from the sensors list, or the forced one
    if (column_gamma is not None) and (not is_force_gamma):
        mean_gamma = data_0d[column_gamma][args].mean()
        if is_print_info:
            print('\t. Gamma motor data found, mean value %3.4g deg'%(mean_gamma))
    else:
        mean_gamma = fgamma
        if is_force_gamma:
            if is_print_info:
                print('\t. Gamma is forced to the value: gamma = %3.4g deg'%fgamma)
        else:
            if is_print_info:
                print('\t. Gamma not found, forced to the value: gamma = %3.4g deg'%fgamma)

    # Extract the value of the sensors in the list selected_sensors
    sensors_str = ''
    if selected_sensors is not None:
        for sensor in selected_sensors:
            for i, stamp_0d in enumerate(stamps_0d):
                if stamp_0d[1] is not None:
                    if stamp_0d[1].lower()==sensor:
                        sensor_value = np.mean(data_0d[i][args])
                        sensors_str += f'{sensor} = {sensor_value}\n'

    # Extract time
    time_str = ''
    if column_epoch is not None:
        epoch_time =  data_0d[column_epoch][args]
        time_str = 'Starting time: %s\nEnding time: %s'%(
                   datetime.datetime.fromtimestamp(epoch_time[0]),
                   datetime.datetime.fromtimestamp(epoch_time[-1]))

    if is_images_present:

        # Load images
        _, images = nexus.extractData('2D')
        nexus.close()

        # Get positions of the dead pixels and set them to zero
        # Start from the known path of a module
        if alias_detector in ['pilatus', 'pilatus_image']:
            file_dead_pixels_pilatus = os.path.dirname(jlb_io.__file__)\
                                       +'/../params/dead_pixels/dead_pixels_pilatus.dat'
            dead_pixels = jlb_io.extract_dead_pixels(file_dead_pixels_pilatus)
        elif alias_detector in ['ufxc', 'ufx', 'ufxc_image']:
            file_dead_pixels_ufxc = os.path.dirname(jlb_io.__file__)\
                                       +'/../params/dead_pixels/dead_pixels_ufxc.dat'
            dead_pixels = jlb_io.extract_dead_pixels(file_dead_pixels_ufxc)

        integ_rod = []
        integ_rod_top = []
        integ_rod_bottom = []
        integ_rod_first_quarter = []
        mat = []

        # Loop over all the non Nan values
        for i in np.arange(len(args))[args]:
            sys.stdout.write('Treat image %d/%d \r'%(i+1, nbpts))
            sys.stdout.flush()

            image = images[0][i]

            for ii,jj in dead_pixels:
                image[ii,jj] = 0.0

            # Keep only the ROI corresponding to the Soller's slits
            image = image[roi_soller[1]:roi_soller[1]+roi_soller[3],
                          roi_soller[0]:roi_soller[0]+roi_soller[2]]

            # Replace the intensity of the dead zones with a value of 0
            image = np.where(image<0., 0., image)

            # Rod (integration along the horizontal axis)
            rod = image.sum(axis=1)

            # Final image with rods stacked (dim: nb_angle x vertical_dim_roi)
            mat.append(rod)

            # Integrated rods
            integ_rod.append(rod.sum())

            # Integrate the rods on different parts of the detector only
            integ_rod_top.append(rod[0:int(roi_soller[3]/2)].sum())
            integ_rod_bottom.append(rod[int(roi_soller[3]/2):roi_soller[3]].sum())
            integ_rod_first_quarter.append(rod[int(3*roi_soller[3]/4):roi_soller[3]].sum())

            sys.stdout.write(30*' '+'\r')
            sys.stdout.flush()

        # Convert to numpy arrays
        integ_rod = np.array(integ_rod)
        integ_rod_top = np.array(integ_rod_top)
        integ_rod_bottom = np.array(integ_rod_bottom)
        integ_rod_first_quarter = np.array(integ_rod_first_quarter)
        mat = np.array(mat)

        # Extract x values (qxy or delta)
        x = data_0d[column_x]
        x = x[args]

        # Bin the matrix along the vertical axis
        channels_binned, mat_binned = bin_matrix_vertical(mat, bin_size)

        # Extract y values (qz or vertical channels)
        if is_compute_qz:
            # Compute and return qz
            thetaz = thetac+(mean_gamma*np.pi/180.0)+(channel0-channels_binned)*thetaz_factor
            y = 2.0*np.pi*np.sin(thetaz)/wavelength
        else:
            # Return the vertical channels
            y = channels_binned

        # Nature and unit of the axis
        if column_qxy is None:
            # x axis corresponds to delta
            x_label = str(stamps_0d[column_x][1])
        else:
            # x axis corresponds to qxy
            if stamps_0d[column_x][2] is None:
                # In case of flyscan, there is no alias unit
                x_label = str(stamps_0d[column_x][1]+' (nm-1)')
            else:
                x_label = str(stamps_0d[column_x][1]+' ('+stamps_0d[column_x][2]+')')

        if is_compute_qz:
            y_label = 'qz (nm-1)'
        else:
            y_label = r'$vertical\ channels$'

    else:
        integ_rod = None
        integ_rod_top = None
        integ_rod_bottom = None
        integ_rod_first_quarter = None
        mat = None
        mat_binned = None
        channels_binned = None
        x = None
        y = None
        x_label = None
        y_label = None

    return x, y, x_label, y_label, time_str, sensors_str, column_x,\
           integ_rod, integ_rod_top, integ_rod_bottom, integ_rod_first_quarter,\
           mat, mat_binned, channels_binned,\
           mean_pi, mean_area, mean_gamma, stamps_0d, data_0d

def plot_gixd_scan(
    x, y, x_label, y_label, time_str, sensors_str,
    integ_rod, integ_rod_top, integ_rod_bottom, integ_rod_first_quarter,
    mat_binned, mean_pi, mean_gamma,
    nxs_name, absorbers, is_gixd_logx, is_gixd_logy, is_gixd_logz,
    nb_levels, map_gixd, is_raster):
    """
    Plot GIXD data.

    Parameters
    ----------
    x : array
        Either qxy (nm^-1) values, if qxy is available in the list of sensors,
        or delta (deg) if not.
    y : array
        Either qz (nm^-1) values, if is_compute_qz is True,
        or vertical channels if not.
    x_label : str
        'qxy' or 'delta' (useful for plot).
    y_label : str
        'qz' or 'channels' (useful for plot).
    time_str : str
        Starting/ending dates of the scan.
    sensors_str : str
        Label with mean values of selected sensors.
    column_x :
        Column corresponding to the x values in stamps_0d (useful for save).
    integ_rod : array
        Rods integrated over the whole vertical axis of the detector.
    integ_rod_top : array
        Rods integrated over the top half vertical axis of the detector.
    integ_rod_bottom : array
        Rods integrated over the bottom half vertical axis of the detector.
    integ_rod_first_quarter : array
        Rods integrated over the bottom quarter vertical axis of the detector.
    mat_binned : array
        Matrix after vertical binning.
    mean_pi : float or None
        Average of the surface pressure (mN/m) over the scan (None if pressure not found).
    mean_gamma : float or None
        Average of gamma (deg) over the scan (None if gamma not found).
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    absorbers : str
        Text to display which absorber was used.
    is_gixd_logx : bool
        Log scale on the x axis of the integrated profile.
    is_gixd_logy : bool
        Log scale on the y axis of the integrated profile.
    is_gixd_logz : bool
        Log scale on the color scale of the image.
    nb_levels : int
        Number of color levels for the image display.
    map_gixd : str
        Colormap of the image.
    is_raster : bool
        Rasterization of the image.

    Returns
    -------
    fig : matplotlib figure
        The figure to be saved in pdf.

    """
    # Print absorbers
    if absorbers != '':
        print("Absorbers: %s"%absorbers)

    if time_str != '':
        print(time_str)

    if sensors_str != '':
        print(sensors_str)

    # Error bars along y
    error = np.sqrt(integ_rod)

    # Create fig
    fig = plt.figure(nxs_name, figsize=(12,5))
    fig.subplots_adjust(hspace=0.4, wspace=0.4, bottom=0.16)
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)

    # Plot the integrated spectra
    if (not is_gixd_logx) and (not is_gixd_logy):
        ax1.errorbar(x, integ_rod, yerr=error, fmt='ro', label="full")
        ax1.plot(x, integ_rod_top, 'k-', label='top')
        ax1.plot(x, integ_rod_bottom, 'b-', label='bottom')
        ax1.plot(x, integ_rod_first_quarter, 'r-', label='bottom quarter')
        ax1.legend()
    elif is_gixd_logx and (not is_gixd_logy):
        ax1.semilogx(x, integ_rod, 'ro')
    elif (not is_gixd_logx) and is_gixd_logy:
        ax1.semilogy(x, integ_rod, 'ro')
    elif is_gixd_logx and is_gixd_logy:
        ax1.loglog(x, integ_rod, 'ro')
    ax1.set_xlabel(x_label, labelpad=13, fontsize='large')
    ax1.set_ylabel("qz integrated intensity", labelpad=13, fontsize='large')

    # Plot the matrix
    if is_gixd_logz:
        ax2.contourf(x, y, np.log(mat_binned.transpose()),  zorder=-10)
    else:
        z_max = mat_binned.max()
        z_min = mat_binned.min()
        ax2.contourf(x, y, (mat_binned.transpose()),
            levels=np.linspace(z_min, z_max, nb_levels),cmap=map_gixd,  zorder=-10)

    if is_raster:
        # Apply the rasterization to all plot with a zorder below -1
        plt.gca().set_rasterization_zorder(-1)

    ax2.set_ylabel(y_label, fontsize='large')
    ax2.set_xlabel(x_label, labelpad=13, fontsize='large')

    if mean_pi is not None:
        fig.text(.04, .05, r'$\pi = %3.4gmN.m^{-1}$'%(mean_pi),
                 fontsize='large', color='red')
    if mean_gamma is not None:
        fig.text(.96, .05, r'$\gamma = %3.4g deg$'%(mean_gamma),
                 fontsize='large', color='red', horizontalalignment='right')

    suptitle = nxs_name[nxs_name.rfind('/')+1:]
    fig.suptitle(suptitle, fontsize='x-large')

    plt.show()

    return fig

def save_gixd_scan(
    x, integ_rod, integ_rod_top, integ_rod_bottom, integ_rod_first_quarter,
    mat, list_moy_to_create, mean_gamma, column_x,
    channel0, thetaz_factor, wavelength, thetac,
    nxs_name, stamps_0d, data_0d, fig, path_to_save_dir,
    is_compute_qz, is_print_info):
    """
    Save GIXD data.

    XXX_1D.mat: the matrix corresponding to the image displayed.
    Each line corresponds to a position of delta.

    XXX_1D.dat: the value of each sensor at each point of the scan.
    It contains also integration along qz:
        - QzIntegrated : over the whole detector,
        - QzIntegratedTop : over its top half,
        - QzIntegratedBottom : over its bottom half,
        - QzIntegratedBottomQuarter : over its bottom quarter.

    XXX.pdf : the figure in pdf.

    Binned data:
        - XXX_1D.matNN : binning of the matrix, with NN the number of points per bin.
        - XXX_1D_qz.datNN : to convert bin number to qz in XXX_1D.matNN.
        - XXX_1D.moyNN : a more convenient way to represent the binned matrices with a 3 columns (qxy, qz, intensity) display.

    Parameters
    ----------
    x : array
        Either qxy (nm^-1) values, if qxy is available in the list of sensors,
        or delta (deg) if not.
    integ_rod : array
        Rods integrated over the whole vertical axis of the detector.
    integ_rod_top : array
        Rods integrated over the top half vertical axis of the detector.
    integ_rod_bottom : array
        Rods integrated over the bottom half vertical axis of the detector.
    integ_rod_first_quarter : array
        Rods integrated over the bottom quarter vertical axis of the detector.
    mat : array
        Matrix with each line corresponding to a position of delta.
    list_moy_to_create : list of int, optional
        Bin sizes to be used, e.g. [10, 20, 40].
    mean_gamma : float or None
        Average of gamma (deg) over the scan (None if gamma not found).
    column_x : int
        Column corresponding to the x values in stamps_0d (useful for save).
    channel0 : int
        Vertical channel corresponding to the Vineyard's peak.
    thetaz_factor : float
        Factor for conversion from channel to radian in the vertical direction (rad/channel).
    wavelength : float
        Wavelength in nm.
    thetac : float
        Critical angle in rad.
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    stamps_0d : array
        Aliases of each 0D sensor in the scan.
    data_0d : array
        Values of each 0D sensor in the scan.
    fig : None or matplotlib figure
        The figure to be saved in pdf. Pass None if not wanted.
    path_to_save_dir : str
        Path to the directory where the treated files will be saved.
    is_compute_qz : bool
        Convert pixels to qz in the vertical direction.
    is_print_info : bool
        Verbose mode.

    """
    nbpts = len(data_0d[0])

    # Find positions of non Nan values based on data[0]
    args = ~np.isnan(data_0d[0])

    # Create Save Name
    save_name = path_to_save_dir + nxs_name[:nxs_name.rfind('.nxs')] + '_1D'

    # Save the original matrix
    np.savetxt(save_name+'.mat', mat)
    if is_print_info:
        print('\t. Original, non binned, matrix saved in:')
        print('\t%s.mat'%save_name)

    # Take care of scalar data
    tosave = np.zeros((integ_rod.shape[0], 4), float)
    tosave[:,0] = integ_rod
    tosave[:,1] = integ_rod_top
    tosave[:,2] = integ_rod_bottom
    tosave[:,3] = integ_rod_first_quarter

    # Concatenate scalar data
    data_0d = np.array(data_0d).transpose()
    data_0d = data_0d[args,:]
    data_0d_tosave = np.concatenate((data_0d,tosave), axis=1)

    with open(save_name+'.dat', "w") as f:
        # Create and save header
        s = ""
        for stamp_0d in stamps_0d:
            if stamp_0d[1] is not None:
                s += stamp_0d[1]+'\t'
            else:
                s += stamp_0d[0]+'\t'
        s += 'QzIntegrated \t QzIntegratedTop \t QzIntegratedBottom \tQzIntegratedBottomQuarter\n'
        f.write(s)

        # Save data
        for i in range(data_0d_tosave.shape[0]):
            s = ""
            for j in range(data_0d_tosave.shape[1]):
                s += str(data_0d_tosave[i,j])+'\t'
            f.write(s+'\n')

    if is_print_info:
        print('\t. Scalar data saved in:')
        print('\t%s.dat'%save_name)

    # Save as moy
    for bin_size in list_moy_to_create:

        # Bin the matrix
        channels_binned, mat_binned= bin_matrix_vertical(mat, bin_size)

        # Extract y values (qz or vertical channels)
        if is_compute_qz:
            # Compute and return qz
            thetaz = thetac+(mean_gamma*np.pi/180.0)+(channel0-channels_binned)*thetaz_factor
            y = 2.0*np.pi*np.sin(thetaz)/wavelength
        else:
            # Return the vertical channels
            y = channels_binned

        # Create the moy images
        moy = np.zeros((nbpts*y.shape[0],3), float)
        index = 0
        for i in range(data_0d.shape[0]):
            for j in range(y.shape[0]):
                moy[index,0] = x[i]
                moy[index,1] = y[j]
                moy[index,2] = mat_binned[i,j]
                index += 1

        with open(save_name+'.moy'+str(bin_size), 'w') as f:
            f.write(stamps_0d[column_x][1]+'\t'+'Qz \t Intensity\n')
            for i in range(moy.shape[0]):
                f.write(str(moy[i,0])+'\t'+str(moy[i,1])+'\t'+str(moy[i,2])+'\n')

        # Save the matrix
        np.savetxt(save_name+'.mat'+str(bin_size), mat_binned)

        # Save the Qz
        if is_compute_qz:
            np.savetxt(save_name+'_qz.dat'+str(bin_size), y)
            if is_print_info:
                print('\t. qz values saved in:')
                print('\t%s_qz.dat%s'%(save_name, bin_size))

        if is_print_info:
            print('\t. Binned matrix saved in:')
            print('\t%s.mat%s'%(save_name, bin_size))

            print('\t. XYZ data saved in:')
            print('\t%s.moy%s'%(save_name, bin_size))

    # Figure in pdf
    if fig is not None:
        plt.figure(fig)
        plt.savefig(save_name+'.pdf', bbox_inches='tight')
        plt.close()

        if is_print_info:
            print('\t. Figure saved in:')
            print('\t%s.pdf'%save_name)
