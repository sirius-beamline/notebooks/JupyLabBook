"""
Library for GIXS.
"""
import os
import datetime
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib import colors
import matplotlib.patches as patches
from PIL import Image
from lib.backend import PyNexus as PN
from lib.frontend import jlb_io

_RED='\x1b[31;01m'
_BLUE='\x1b[34;01m'
_RESET='\x1b[0m'

def process_gixs_scan(
        nxs_name, path_to_nxs_dir,
        wavelength, distance_detec,
        pixel_poni_x, pixel_poni_y, pixel_size,
        is_force_gamma=False, is_force_delta=False, is_force_thetai=False,
        fgamma=0., fdelta=0., fthetai=0., roi_2d=[-1, -1, -1, -1], clim=(-1,-1),
        selected_sensors = None,
        absorbers='', is_gixs_logz=True, map_gixs='viridis',
        path_to_save_dir='', is_print_stamps=False, is_plot=False,
        is_save_sum=False, is_save_each=False, is_print_info=False):
    """
    Call functions for extracting, plotting, and saving a GIXS scan.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    wavelength : float
        Wavelength in nm.
    distance_detec : float
        Distance between the detector and the center of the sample in mm.
    pixel_poni_x : float
        horizontal coordinate of the Point Of Normal Incidence in pixels. Measured on the
        direct beam at delta=0 and gamma=0.
    pixel_poni_y : float
        vertical coordinate of the Point Of Normal Incidence in pixels. Measured on the
        direct beam at delta=0 and gamma=0.
    pixel_size : float
        pixel size in mm.
    is_force_gamma : bool, optional
        Force gamma to be equal to the value of `fgamma`.
    is_force_delta : bool, optional
        Force delta to be equal to the value of `fdelta`.
    is_force_thetai : bool, optional
        Force thetai to be equal to the value of `fthetai`.
    fgamma : float, optional
        Value of gamma (deg) to be used if is_force_gamma is set to True
        or if gamma is absent from the sensor list.
    fdelta : float, optional
        Value of delta (deg) to be used if is_force_delta is set to True
        or if delta is absent from the sensor list.
    fthetai : float, optional
        Value of thetai (deg) to be used if is_force_thetai is set to True
        or if alphax is absent from the sensor list.
    roi_2d : list of int, optional
        ROI for the integrated spectra [x0, y0, size_x, size_y].
    clim : tuple, optional
        Lim min. and max. of the color scale.
    selected_sensors : list of str, optional
        List of aliases of sensors, for their mean value to be added to the plot.
    absorbers : str, optional
        Text to display which absorber was used.
    is_gixs_logz : bool, optional
        Log scale on the color scale of the image.
    map_gixs : str, optional
        Colormap of the image.
    path_to_save_dir : str, optional
        Path to the directory where the treated files will be saved.
    is_print_stamps : bool, optional
        Print the list of sensors contained in the nexus file.
    is_plot : bool, optional
        Plot the 2D GIXS image and the integrated profiles.
    is_save_sum : bool, optional
        Save the sum of the images.
    is_save_each : bool, optional
        Save each individual image and the sum.
    is_print_info : bool, optional
        Verbose mode.

    Returns
    -------
    images_sum : array
        Pilatus images integrated over the scan.
    integ_qxy : array
        Profile integrated along the horizontal axis.
    integ_qz : array
        Profile integrated along the vertical axis.
    qxy_list : array
        List of qxy in the profile.
    qz_list : array
        List of qz in the profile.

    """
    images, images_sum, qxy_grid, qz_grid, integ_qxy, integ_qz,\
    qxy_list, qz_list, gamma_str, delta_str, thetai_str, time_str, sensors_str,\
    stamps_0d, data_0d = \
    extract_gixs_scan(
        nxs_name, path_to_nxs_dir,
        wavelength, distance_detec,
        pixel_poni_x, pixel_poni_y, pixel_size,
        is_force_gamma, is_force_delta, is_force_thetai,
        fgamma, fdelta, fthetai,
        selected_sensors,
        is_print_stamps, is_print_info
        )

    fig = None

    # Avoid crash of the cell if Pilatus images are not present
    if images is None:
        is_plot = False
        is_save_each = False
        is_save_sum = False

    if is_plot:
        fig = plot_gixs_scan(
            images_sum, roi_2d, clim, qxy_grid, qz_grid, qxy_list, qz_list,
            nxs_name, absorbers, is_gixs_logz, map_gixs,
            gamma_str, delta_str, thetai_str, time_str,
            sensors_str
            )

    if is_save_each:
        # Save the sum and each individual image in a dedicated folder
        save_gixs_scan(
            images, images_sum,
            integ_qxy, integ_qz, qxy_list, qz_list,
            nxs_name, stamps_0d, data_0d, fig,
            path_to_save_dir, is_print_info
            )

    elif is_save_sum:
        # Save only the sum
        save_gixs_scan(
            None, images_sum,
            integ_qxy, integ_qz, qxy_list, qz_list,
            nxs_name, stamps_0d, data_0d, fig,
            path_to_save_dir, is_print_info
            )

    # Do not return `images`, as this might be a very big object
    return images_sum, integ_qxy, integ_qz, qxy_list, qz_list


def extract_gixs_scan(
        nxs_name, path_to_nxs_dir,
        wavelength, distance_detec,
        pixel_poni_x, pixel_poni_y, pixel_size,
        is_force_gamma, is_force_delta, is_force_thetai,
        fgamma, fdelta, fthetai,
        selected_sensors,
        is_print_stamps, is_print_info):
    """
    Extract the nexus file and return useful quantities for GIXS.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    wavelength : float
        Wavelength in nm.
    distance_detec : float
        Distance between the detector and the center of the sample in mm.
    pixel_poni_x : float
        horizontal coordinate of the Point Of Normal Incidence in pixels. Measured on the
        direct beam at delta=0 and gamma=0.
    pixel_poni_y : float
        vertical coordinate of the Point Of Normal Incidence in pixels. Measured on the
        direct beam at delta=0 and gamma=0.
    pixel_size : float
        pixel size in mm.
    is_force_gamma : bool
        Force gamma to be equal to the value of `fgamma`.
    is_force_delta : bool
        Force delta to be equal to the value of `fdelta`.
    is_force_thetai : bool
        Force thetai to be equal to the value of `fthetai`.
    fgamma : float
        Value of gamma (deg) to be used if is_force_gamma is set to True
        or if gamma is absent from the sensor list.
    fdelta : float
        Value of delta (deg) to be used if is_force_delta is set to True
        or if delta is absent from the sensor list.
    fthetai : float
        Value of thetai (deg) to be used if is_force_thetai is set to True
        or if alphax is absent from the sensor list.
    selected_sensors : list of str
        List of aliases of sensors, for their mean value to be added to the plot.
    is_print_stamps : bool
        Print the list of sensors contained in the nexus file.
    is_print_info : bool
        Verbose mode.

    Returns
    -------
    images : array
        Array of individual Pilatus images.
    images_sum : array
        Pilatus images integrated over the scan.
    qxy_grid : array
        2D grid of qxy for 2D plots.
    qz_grid : array
        2D grid of qz for 2D plots.
    integ_qxy : array
        Profile integrated along the horizontal axis.
    integ_qz : array
        Profile integrated along the vertical axis.
    qxy_list : array
        List of qxy in the profile.
    qz_list : array
        List of qz in the profile.
    gamma_str : str
        Label with value of gamma.
    delta_str : str
        Label with value of delta.
    thetai_str : str
        Label with value of thetai.
    time_str : str
        Starting/ending dates of the scan.
    sensors_str : str
        Label with mean values of selected sensors.
    stamps_0d : array
        Aliases of each 0D sensor in the scan.
    data_0d : array
        Values of each 0D sensor in the scan.

    Raises
    ------
    FileNotFoundError
        If a file or a folder is missing.
    SystemExit('Could not open Nexus file.'))
        If the Nexus file cannot be accessed.

    """
    nxs_path = path_to_nxs_dir+nxs_name

    if not os.path.isfile(nxs_path):
        print(_RED+'Scan %s is not present in the file directory.'%(nxs_name)+_RESET)
        print('\t\t file directory : %s'%path_to_nxs_dir)
        raise FileNotFoundError('Missing folder or file.')

    column_z = None
    column_gamma = None
    column_delta = None
    column_thetai = None
    column_epoch = None

    if is_print_info:
        print(_BLUE+" - Open Nexus Data File :"+ _RESET)
        print('\t%s'%nxs_path)

    try:
        nexus = PN.PyNexusFile(nxs_path, fast=True)
    except:
        print(_RED+'\t Could not open Nexus file.'+_RESET)
        raise Exception('Could not open Nexus file.')

    nbpts = int(nexus.get_nbpts())
    if is_print_info:
        print("\t. Number of data points: %s"%nbpts)

    # Extract stamps
    stamps = nexus.extractStamps()
    if is_print_stamps :
        print("\t. Available Counters:")
    for i, stamp in enumerate(stamps):
        if stamp[1] is not None:
            if is_print_stamps :
                print("\t\t%s  -------> %s"%(i,stamp[1]))
            if stamp[1].lower()=='pilatus':
                column_z = i
        else:
            if is_print_stamps :
                print("\t\t%s  -------> %s"%(i,stamp[0]))

    # Extract 0D data
    stamps_0d, data_0d = nexus.extractData('0D')

    # Find columns
    for i, stamp_0d in enumerate(stamps_0d):
        if stamp_0d[1] is not None:
            if stamp_0d[1].lower()=='delta':
                column_delta = i
            if stamp_0d[1].lower()=='gamma':
                column_gamma = i
            if stamp_0d[1].lower()=='alphax':
                column_thetai = i

        else:
            if stamp_0d[0].lower()=='sensorstimestamps':
                column_epoch = i

    # Check that Pilatus images are present
    is_images_present = False
    if column_z is not None:
        is_images_present = True
        if is_print_info:
            print('\t. Pilatus data found, (column %d, alias %s)'%(column_z, stamps[column_z][1]))
    else:
        print(_RED+'\t. No pilatus data found'+_RESET)
        nexus.close()
        #raise Exception('pilatus not found.')

    # Find positions of non Nan values based on data[0]
    args = ~np.isnan(data_0d[0])

    # Get first and last position of the non NaN values
    if is_print_info:
        print('\t. Valid data between points %d and %d'
                      %(np.argmax(args), len(args)-np.argmax(args[::-1])-1))

    # Take the value of gamma from the sensors list, or the forced one
    if (column_gamma is not None) and (not is_force_gamma):
        gamma = np.mean(data_0d[column_gamma][args])
        gamma_str = 'gamma found: gamma = %3.4g deg'%(gamma)
    else:
        gamma = fgamma
        if is_force_gamma:
            gamma_str = 'gamma is forced to the value: gamma = %3.4g deg'%gamma
        else:
            gamma_str = 'gamma not found, forced to the value: gamma = %3.4g deg'%gamma

    # Take the value of delta from the sensors list, or the forced one
    if (column_delta is not None) and (not is_force_delta):
        delta = np.mean(data_0d[column_delta][args])
        delta_str = 'delta found: delta = %3.4g deg'%(delta)
    else:
        delta = fdelta
        if is_force_delta:
            delta_str = 'delta is forced to the value: delta = %3.4g deg'%delta
        else:
            delta_str = 'delta not found, forced to the value: delta = %3.4g deg'%delta

    # Take the value of thetai from the sensors list, or the forced one
    if (column_thetai is not None) and (not is_force_thetai):
        thetai = np.mean(data_0d[column_thetai][args])
        thetai_str = 'thetai (alphax) found: thetai = %3.4g deg'%thetai
    else:
        thetai = fthetai
        if is_force_thetai:
            thetai_str = 'thetai is forced to the value: thetai = %3.4g deg'%thetai
        else:
            thetai_str = 'thetai (alphax) not found, forced to the value: thetai = %3.4g deg'%(thetai)

    # Extract the value of the sensors in the list selected_sensors
    sensors_str = ''
    if selected_sensors is not None:
        for sensor in selected_sensors:
            for i, stamp_0d in enumerate(stamps_0d):
                if stamp_0d[1] is not None:
                    if stamp_0d[1].lower()==sensor:
                        sensor_value = np.mean(data_0d[i][args])
                        sensors_str += f'{sensor} = {sensor_value}\n'

    if is_print_info:
        print(gamma_str)
        print(delta_str)
        print(thetai_str)
        print(sensors_str)

    # Extract time
    time_str = ''
    if column_epoch is not None:
        epoch_time =  data_0d[column_epoch][args]
        time_str = 'Starting time: %s\nEnding time: %s'%(
                   datetime.datetime.fromtimestamp(epoch_time[0]),
                   datetime.datetime.fromtimestamp(epoch_time[-1]))

    if is_images_present:

        # Load images
        _, images = nexus.extractData('2D')
        nexus.close()
        images = np.asarray(images)[0]

        # Get positions of the dead pixels and set them to zero
        # Start from the known path of a module
        file_dead_pixels_pilatus = os.path.dirname(jlb_io.__file__)\
                                       +'/../params/dead_pixels/dead_pixels_pilatus.dat'
        dead_pixels = jlb_io.extract_dead_pixels(file_dead_pixels_pilatus)

        for ii,jj in dead_pixels:
            images[:,ii,jj] = 0.0

        # Sum the images over the scan
        images_sum = images.sum(axis=0)

        # Replace dead zones with an intensity of -2.
        images_sum = np.where(images_sum<0., -2., images_sum)

        pixels_x = np.arange(0,np.shape(images_sum)[1],1)
        pixels_y = np.arange(0,np.shape(images_sum)[0],1)

        xx, yy = np.meshgrid(pixels_x, pixels_y)

        # alphai (incident angle) in rad
        # The sign of alphai is correct if the motor thetah is at 0.
        alphai = thetai*np.pi/180.

        pixel_direct_x = pixel_poni_x-distance_detec/pixel_size*np.tan(delta*np.pi/180.)
        pixel_direct_y = pixel_poni_y+distance_detec/pixel_size*np.tan(gamma*np.pi/180.)

        # 2*theta in rad
        twotheta = np.arctan(pixel_size*(xx-pixel_direct_x)/distance_detec)

        # alpha_f in rad
        alphaf = np.arctan( (pixel_size*(pixel_direct_y-yy))/distance_detec ) - alphai

        # q in nm^-1
        k0 = 2*np.pi/wavelength
        qz = k0*(np.sin(alphaf)+np.sin(alphai))
        # Careful, qxy is here computed in the approx. of small exit angles
        qxy = 2*k0*np.sin(twotheta/2.)

        # True values of q (not used here)
        #qx = k0*(np.cos(alphaf)*np.cos(twotheta)-np.cos(alphai))
        #qy = k0*np.cos(alphaf)*np.sin(twotheta)
        #qxy = np.sqrt(np.square(qx)+np.square(qy))
        #q = np.sqrt(np.square(qxy)+np.square(qz))

        # Profiles
        # Put all the negative pixels to zero before integration
        integ_qxy = np.where(images_sum>0, images_sum, 0.).sum(axis=1)
        integ_qz = np.where(images_sum>0, images_sum, 0.).sum(axis=0)

        pixel_x_list = np.arange(0,np.shape(images_sum)[1])
        pixel_y_list = np.arange(0,np.shape(images_sum)[0])

        alphaf_list = np.arctan( (pixel_size*(pixel_direct_y-pixel_y_list))/distance_detec ) - alphai
        qz_list = 2*np.pi/wavelength*(np.sin(alphaf_list)+np.sin(alphai))
        twotheta_list = np.arctan(pixel_size*(pixel_x_list-pixel_direct_x)/distance_detec)
        # Careful, approx. qxy=4*pi/lambda*sin(2*theta/2)
        qxy_list = 4*np.pi/wavelength*np.sin(twotheta_list/2.)

        # The grid is used for the full image
        qz_grid = qz
        qxy_grid = qxy
        
    else:
        images = None
        images_sum = None
        qxy_grid = None
        qz_grid = None
        integ_qxy = None
        integ_qz = None
        qxy_list = None
        qz_list = None


    return images, images_sum, qxy_grid, qz_grid, integ_qxy, integ_qz,\
           qxy_list, qz_list, gamma_str, delta_str, thetai_str, time_str, sensors_str,\
           stamps_0d, data_0d

def plot_gixs_scan(
    images_sum, roi_2d, clim, qxy_grid, qz_grid, qxy_list, qz_list,
    nxs_name, absorbers, is_gixs_logz, map_gixs,
    gamma_str, delta_str, thetai_str, time_str,
    sensors_str):
    """
    Plot GIXS data.

    Parameters
    ----------
    images_sum : array
        Pilatus images integrated over the scan.
    roi_2d : list of int
        ROI for the integrated spectra [x0, y0, size_x, size_y].
    clim : tuple
        Lim min. and max. of the color scale.
    qxy_grid : array
        2D grid of qxy for 2D plots.
    qz_grid : array
        2D grid of qz for 2D plots.
    qxy_list : array
        List of qxy in the full profile.
    qz_list : array
        List of qz in the full profile.
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    absorbers : str
        Text to display which absorber was used.
    is_gixs_logz : bool
        Log scale on the color scale of the image.
    map_gixs : str
        Colormap of the image.
    gamma_str : str
        Label with value of gamma.
    delta_str : str
        Label with value of delta.
    thetai_str : str
        Label with value of thetai.
    time_str : str
        Starting/ending dates of the scan.
    sensors_str : str
        Label with mean values of selected sensors.

    Returns
    -------
    fig : matplotlib figure
        The figure to be saved in pdf.

    """
    # Print absorbers
    if absorbers != '':
        print("Absorbers: %s"%absorbers)

    if time_str != '':
        print(time_str)

    print(_RED+gamma_str+_RESET)
    print(_RED+delta_str+_RESET)
    print(_RED+thetai_str+_RESET)

    if sensors_str != '':
        print(sensors_str)

    # If there is a negative value in the ROI,
    # take the whole detector
    if any(t < 0 for t in roi_2d):
        roi_2d = [0, 0, np.shape(images_sum)[1]-1, np.shape(images_sum)[0]-1]

    # Apply the ROI for the integrated spectra
    images_roi = images_sum[roi_2d[1]:roi_2d[1]+roi_2d[3],
                            roi_2d[0]:roi_2d[0]+roi_2d[2]]

    integ_qxy_roi = np.where(images_roi>0, images_roi, 0.).sum(axis=1)
    integ_qz_roi = np.where(images_roi>0, images_roi, 0.).sum(axis=0)

    # Apply the ROI lists of qxy and qz in the integrated spectra
    qxy_list_roi = qxy_list[roi_2d[0]:roi_2d[0]+roi_2d[2]]
    qz_list_roi = qz_list[roi_2d[1]:roi_2d[1]+roi_2d[3]]

    # Create fig
    fig = plt.figure(figsize=(15,15))
    fig.subplots_adjust(top=0.95)
    fig.suptitle(nxs_name.split('\\')[-1], fontsize='x-large')

    # Divide the grid in 2x2
    outer = gridspec.GridSpec(2, 2, wspace=0.2)

    # Divide the left row in 2x1
    inner = gridspec.GridSpecFromSubplotSpec(2, 1,
                    subplot_spec=outer[0], hspace=0.5)

    #############################################
    # Plot a profile along y (integrated over x)
    ax1 = fig.add_subplot(inner[0])

    if is_gixs_logz:
        ax1.set_yscale('log')
    ax1.set(xlabel = 'qz (nm^-1)', ylabel = 'integration along qxy (ROI)')

    # Compute best limits
    ax1.set_ylim(0.8*np.min(integ_qxy_roi[integ_qxy_roi>0]),1.2*np.max(integ_qxy_roi))
    
    ax1.plot(qz_list_roi, integ_qxy_roi)

    ###################################################
    # Plot a profile along x (integrated over y)
    ax2 = fig.add_subplot(inner[1])

    if is_gixs_logz:
        ax2.set_yscale('log')
    ax2.set(xlabel = 'qxy (nm^-1)', ylabel = 'integration along qz (ROI)')

    # Compute best limits
    ax2.set_ylim(0.8*np.min(integ_qz_roi[integ_qz_roi>0]),1.2*np.max(integ_qz_roi))
    
    ax2.plot(qxy_list_roi, integ_qz_roi)

    #Divide the right row in 1x1
    inner = gridspec.GridSpecFromSubplotSpec(1, 1,
                    subplot_spec=outer[1], wspace=0.1, hspace=0.1)

    #####################################################
    # Show the full image integrated over the scan
    ax2 = fig.add_subplot(inner[0])

    if is_gixs_logz:
        if (clim[0] != -1 and clim[1] != -1):
            ax2.pcolormesh(qxy_grid, qz_grid, images_sum, cmap = map_gixs, shading = 'auto',
                   norm = colors.LogNorm(vmin=clim[0], vmax=clim[1]), rasterized=True)
        else:
            ax2.pcolormesh(qxy_grid, qz_grid, images_sum, cmap = map_gixs, shading = 'auto',
                           norm = colors.LogNorm(), rasterized=True)                          
    else:
        if (clim[0] != -1 and clim[1] != -1):
            ax2.pcolormesh(qxy_grid, qz_grid, images_sum, cmap = map_gixs, shading = 'auto',
                           rasterized=True, vmin=clim[0], vmax=clim[1])     
        else:
            ax2.pcolormesh(qxy_grid, qz_grid, images_sum, cmap = map_gixs, shading = 'auto',
                           rasterized=True)
    

    ax2.set_xlabel('qxy (nm^-1)', fontsize='large')
    ax2.set_ylabel('qz (nm^-1)', fontsize='large')
    
    # Add the ROI
    rect = patches.Rectangle( (qxy_list_roi[0], qz_list_roi[1]),
                               qxy_list_roi[-1]-qxy_list_roi[0],
                               qz_list_roi[-1]-qz_list_roi[0],
                              linewidth=1, edgecolor='r', facecolor='none')
    ax2.add_patch(rect)
    
    plt.show()

    return fig

def save_gixs_scan(
    images, images_sum,
    integ_qxy, integ_qz, qxy_list, qz_list,
    nxs_name, stamps_0d, data_0d, fig,
    path_to_save_dir, is_print_info):
    """
    Save GIXS data.

    XXX_pilatus_sum.mat: the matrix corresponding to the image displayed, in ascii.
    XXX_pilatus_sum.tiff: the matrix corresponding to the image displayed, in tiff.
    XXX_integrated_qxy.dat: the horizontal integration of the whole detector as a function of qz (nm-1).
    XXX_integrated_qz.dat: the vertical integration of the whole detector as a function of qxy (nm-1).
    XXX_images/XXX_pliatus_N.tiff : each image of the scan, in tiff.
    XXX.dat : the value of each sensor at each point of the scan.
    XXX.pdf : the figure in pdf.

    Parameters
    ----------
    images : array
        Array of individual Pilatus images.
    images_sum : array
        Pilatus images integrated over the scan.
    integ_qxy : array
        Profile integrated along the horizontal axis.
    integ_qz : array
        Profile integrated along the vertical axis.
    qxy_list : array
        List of qxy in the profile.
    qz_list : array
        List of qz in the profile.
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    stamps_0d : array
        Aliases of each 0D sensor in the scan.
    data_0d : array
        Values of each 0D sensor in the scan.
    fig : None or matplotlib figure
        The figure to be saved in pdf. Pass None if not wanted.
    path_to_save_dir : str
        Path to the directory where the treated files will be saved.
    is_print_info : bool
        Verbose mode.

    """
    # Create Save Name
    save_name = path_to_save_dir + nxs_name[:nxs_name.rfind('.nxs')]

    # Save the profiles and the raw image
    np.savetxt(save_name+'_integrated_qz.dat', np.transpose([qxy_list, integ_qz]),
               delimiter = '\t', comments='', header ='Qxy'+'\t'+'QzIntegrated')
    np.savetxt(save_name+'_integrated_qxy.dat', np.transpose([qz_list, integ_qxy]),
               delimiter = '\t', comments='', header ='Qz'+'\t'+'QxyIntegrated')
    np.savetxt(save_name+'_pilatus_sum.mat', images_sum)

    im = Image.fromarray(images_sum)
    im.save(save_name+'_pilatus_sum.tiff')

    # Figure in pdf
    if fig is not None:
        plt.figure(fig)
        plt.savefig(save_name+'.pdf', bbox_inches='tight')
        plt.close()

    # Save each image individually in a folder
    if images is not None:
        path_to_dir = path_to_save_dir+nxs_name[:nxs_name.rfind('.nxs')]+'_images'
        if not os.path.exists(path_to_dir):
            os.mkdir(path_to_dir)

        for index, image in enumerate(images):
            im = Image.fromarray(image)
            im.save(path_to_dir+'/'+nxs_name[:nxs_name.rfind('.nxs')]+'_pilatus_'+str(index)+'.tiff')

    # Save 0D data
    header_0d = [stamp[1] if stamp[1] is not None else stamp[0] for stamp in stamps_0d]
    header_0d = '\t'.join(header_0d)

    np.savetxt(save_name+'.dat', np.transpose(data_0d), header = header_0d,
               comments = '', fmt='%s', delimiter = '\t')

    if is_print_info:
        print('\t. Integrated qz vs qxy:')
        print('\t %s_integrated_qz.dat'%save_name)
        print('\t. Integrated qxy vs qz:')
        print('\t%s_integrated_qxy.dat'%save_name)
        print('\t. Original matrix saved in:')
        print('\t%s.mat'%save_name)
        print('\t. Tiff saved in:')
        print('\t%s.tiff'%save_name)
        print('\t. Dat file saved in:')
        print('\t%s.dat'%save_name)
        if images is not None:
            print('\t. Individual images saved in:')
            print('\t'+path_to_dir+'/')
        if fig is not None:
            print('\t. Figure saved in:')
            print('\t%s.pdf'%save_name)
