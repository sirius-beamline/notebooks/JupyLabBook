"""
Library for isotherms.
"""
import os
import datetime
import numpy as np
import matplotlib.pyplot as plt
from lib.backend import PyNexus as PN

_RED='\x1b[31;01m'
_BLUE='\x1b[34;01m'
_RESET='\x1b[0m'

def process_isotherm_scan(
        nxs_name, path_to_nxs_dir,
        first_point_plot=0, last_point_plot=-1,
        path_to_save_dir='', is_print_stamps=False, is_plot=False,
        is_save=False, is_print_info=False):
    """
    Call functions for extracting, plotting, and saving an isotherm.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    first_point_plot : int, optional
        First point of the plots.
    last_point_plot : int, optional
        Last point of the plots.
    path_to_save_dir : str, optional
        Path to the directory where the treated files will be saved.
    is_print_stamps : bool, optional
        Print the list of sensors contained in the nexus file.
    is_plot : bool, optional
        Plot the isotherm and the integrated profiles.
    is_save : bool, optional
        Save the results.
    is_print_info : bool, optional
        Verbose mode.

    Returns
    -------
    area : array
        List of area values.
    pressure : array
        List of pressure values.
    time : array
        List of time values.

    """
    area, pressure, time, time_str, stamps_0d, data_0d = \
    extract_isotherm_scan(
        nxs_name, path_to_nxs_dir, is_print_stamps, is_print_info
        )

    fig = None

    if is_plot:
        fig = plot_isotherm_scan(
            area, pressure, time,
            first_point_plot, last_point_plot,
            nxs_name, time_str
            )

    if is_save:
        save_isotherm_scan(
            nxs_name, stamps_0d, data_0d, fig,
            path_to_save_dir, is_print_info
            )

    return area, pressure, time


def extract_isotherm_scan(nxs_name, path_to_nxs_dir, is_print_stamps, is_print_info):
    """
    Extract the nexus file and return useful quantities.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    is_print_stamps : bool
        Print the list of sensors contained in the nexus file.
    is_print_info : bool
        Verbose mode.

    Returns
    -------
    area : array
        List of area values.
    pressure : array
        List of pressure values.
    time : array
        List of time values.
    time_str : str
        Starting/ending dates of the scan.
    stamps_0d : array
        Aliases of each 0D sensor in the scan.
    data_0d : array
        Values of each 0D sensor in the scan.

    Raises
    ------
    FileNotFoundError
        If a file or a folder is missing.
    SystemExit('Could not open Nexus file.'))
        If the Nexus file cannot be accessed.
    SystemExit('Required sensor not found.')
        If a required sensor is not found in the sensor list.

    """
    nxs_path = path_to_nxs_dir+nxs_name

    if not os.path.isfile(nxs_path):
        print(_RED+'Scan %s is not present in the file directory.'%(nxs_name)+_RESET)
        print('\t\t file directory : %s'%path_to_nxs_dir)
        raise FileNotFoundError('Missing folder or file.')

    column_pi = None
    column_area = None
    column_time = None
    column_epoch = None

    if is_print_info:
        print(_BLUE+" - Open Nexus Data File :"+ _RESET)
        print('\t%s'%nxs_path)

    try:
        nexus = PN.PyNexusFile(nxs_path, fast=True)
    except:
        print(_RED+'\t Could not open Nexus file.'+_RESET)
        raise Exception('Could not open Nexus file.')

    nbpts = int(nexus.get_nbpts())
    if is_print_info:
        print("\t. Number of data points: %s"%nbpts)

    # Extract stamps
    stamps = nexus.extractStamps()
    if is_print_stamps :
        print("\t. Available Counters:")
        for i, stamp in enumerate(stamps):
            if stamp[1] is not None:
                print("\t\t%s  -------> %s"%(i,stamp[1]))
            else:
                print("\t\t%s  -------> %s"%(i,stamp[0]))

    # Extract 0D data
    stamps_0d, data_0d = nexus.extractData('0D')

    nexus.close()

    # Find columns
    for i, stamp_0d in enumerate(stamps_0d):
        if stamp_0d[1] is not None:
            if stamp_0d[1].lower()=='surfacepressure':
                column_pi = i
            if stamp_0d[1].lower()=='areapermolecule':
                column_area = i
        else:
            if stamp_0d[0].find('sensorsRelTimestamps')>-1:
                column_time = i
            if stamp_0d[0].lower()=='sensorstimestamps':
                column_epoch = i

    # Find positions of non Nan values based on data[0]
    args = ~np.isnan(data_0d[0])

    # Get first and last position of the non NaN values
    if is_print_info:
        print('\t. Valid data between points %d and %d'
                      %(np.argmax(args), len(args)-np.argmax(args[::-1])-1))

    # Extract time
    time_str = ''
    if column_epoch is not None:
        epoch_time =  data_0d[column_epoch][args]
        time_str = 'Starting time: %s\nEnding time: %s'%(
                   datetime.datetime.fromtimestamp(epoch_time[0]),
                   datetime.datetime.fromtimestamp(epoch_time[-1]))

    # Extract relevant quantities
    if column_pi is not None:
        pressure = data_0d[column_pi][args]
        if is_print_info:
            print('\t. Surface pressure data found.')
    else:
        print('\t. No surface pressure data found')
        raise Exception('Required sensor not found.')

    if column_area is not None:
        area = data_0d[column_area][args]
        if is_print_info:
            print('\t. Area per molecule data found.')
    else:
        print('\t. No area per molecule data found')
        raise Exception('Required sensor not found.')

    if column_time is not None:
        time = data_0d[column_time][args]
        if is_print_info:
            print('\t. Time found.')
    else:
        print('\t. No time data found')
        raise Exception('Required sensor not found.')

    return area, pressure, time, time_str, stamps_0d, data_0d

def plot_isotherm_scan(
    area, pressure, time,
    first_point_plot, last_point_plot,
    nxs_name, time_str):
    """
    Plot the isotherm.

    Parameters
    ----------
    area : array
        List of area values.
    pressure : array
        List of pressure values.
    time : array
        List of time values.
    first_point_plot : int
        First point of the plots.
    last_point_plot : int
        Last point of the plots.
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    time_str : str
        Starting/ending dates of the scan.

    Returns
    -------
    fig : matplotlib figure
        The figure to be saved in pdf.

    """
    if time_str != '':
        print(time_str)

    # Apply the user limits
    area = area[first_point_plot:last_point_plot]
    pressure = pressure[first_point_plot:last_point_plot]
    time = time[first_point_plot:last_point_plot]

    fig = plt.figure(1, figsize=(12,5))
    fig.subplots_adjust(hspace=0.4, wspace=0.4, bottom=0.16)
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)
    ax3 = ax2.twinx()

    ax1.plot(area, pressure, 'r-', lw=2)
    ax2.plot(time, area, 'k-', lw=2)
    ax3.plot(time, pressure, 'b-', lw=2)

    suptitle = nxs_name[nxs_name.rfind('/')+1:nxs_name.rfind('.')]

    fig.suptitle(suptitle, fontsize=20)

    ax1.set_xlabel('Area per Molecule (nm$\\mathregular{^2}$)', fontsize=14)
    ax1.set_ylabel('Surface pressure (mN/m)', fontsize=14)

    ax2.set_xlabel('Time (sec)', fontsize=14)
    ax2.set_ylabel('Area per Molecule (nm$\\mathregular{^2}$)', fontsize=14)
    ax3.set_ylabel('Surface pressure (mN/m)', fontsize=14, color='b')

    plt.show()

    return fig

def save_isotherm_scan(
    nxs_name, stamps_0d, data_0d, fig,
    path_to_save_dir, is_print_info):
    """
    Save isotherm data.

    XXX.dat : the value of each sensor at each point of the scan.
    XXX.pdf : the figure in pdf.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    stamps_0d : array
        Aliases of each 0D sensor in the scan.
    data_0d : array
        Values of each 0D sensor in the scan.
    fig : None or matplotlib figure
        The figure to be saved in pdf. Pass None if not wanted.
    path_to_save_dir : str
        Path to the directory where the treated files will be saved.
    is_print_info : bool
        Verbose mode.

    """
    # Create Save Name
    save_name = path_to_save_dir + nxs_name[:nxs_name.rfind('.nxs')]

    # Save 0D data
    header_0d = [stamp[1] if stamp[1] is not None else stamp[0] for stamp in stamps_0d]
    header_0d = '\t'.join(header_0d)

    np.savetxt(save_name+'.dat', np.transpose(data_0d), header = header_0d,
               comments = '', fmt='%s', delimiter = '\t')

    if is_print_info:
        print('\t. Scalar data saved in:')
        print('\t%s.dat'%save_name)

    # Figure in pdf
    if fig is not None:
        plt.figure(fig)
        plt.savefig(save_name+'.pdf', bbox_inches='tight')
        plt.close()

        if is_print_info:
            print('\t. Figure saved in:')
            print('\t%s.pdf'%save_name)
