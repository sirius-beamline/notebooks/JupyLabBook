"""
Library for XRF.
"""
import os
import datetime
import io
from contextlib import redirect_stdout
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
from lib.backend import PyNexus as PN

_RED='\x1b[31;01m'
_BLUE='\x1b[34;01m'
_RESET='\x1b[0m'


def check_sdd_elems(nxs_name, path_to_nxs_dir, list_sdd_elems):
    """
    Check is all the SDD elems are in the Nexus file (ICR, OCR, spectrum).

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    list_sdd_elems : list of int
        List of SDD elements, e.g. [0, 1, 2].

    Returns
    -------
    is_icr_found : bool
        True if all the required ICR are present in the Nexus file.
    is_ocr_found : bool
        True if all the required OCR are present in the Nexus file.
    is_spectrum_found : bool
        True if all the required spectrums are present in the Nexus file.

    Raises
    ------
    SystemExit('Could not open Nexus file.'))
        If the Nexus file cannot be accessed.

    """
    try:
        nexus = PN.PyNexusFile(path_to_nxs_dir+nxs_name, fast=True)
    except:
        print(_RED+'\t Could not open Nexus file.'+_RESET)
        raise Exception('Could not open Nexus file.')

    # Extract stamps
    stamps, _ = nexus.extractData()
    # Make it lowercase
    stamps = [elem[1].lower() if elem[1] is not None else elem[0].lower() for elem in stamps]
    nexus.close()

    is_icr_found = True
    is_ocr_found = True
    is_spectrum_found = True

    for sdd_elem in list_sdd_elems:
        if 'fluoicr0'+str(sdd_elem) not in stamps:
            is_icr_found = is_icr_found*False
        if 'fluoocr0'+str(sdd_elem) not in stamps:
            is_ocr_found = is_ocr_found*False
        if 'fluospectrum0'+str(sdd_elem) not in stamps:
            is_spectrum_found = is_spectrum_found*False

    return is_icr_found, is_ocr_found, is_spectrum_found

def process_xrf_scan(nxs_name, path_to_nxs_dir,
        list_sdd_elems, channel_xrf_first=0, channel_xrf_last=2048,
        sdd_gain=10., sdd_ev0=0., arr_peaks=[(None,None)],
        selected_sensors = None,
        is_use_ev=False, is_xrf_log=True,
        absorbers='', path_to_save_dir='', is_print_stamps=False,
        is_plot_spectrogram=False, is_plot_sum=False, is_plot_first_last=False,
        is_save=False, is_print_info=False):
    """
    Call functions for extracting, plotting, and saving a XRF scan.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    list_sdd_elems : list of int
        List of SDD elements, e.g. [0, 1, 2]
    channel_xrf_first : int, optional
        Spectrums are extracted between channel_xrf_first and channel_xrf_last.
    channel_xrf_last : int, optional
        Spectrums are extracted between channel_xrf_first and channel_xrf_last.
    sdd_gain : float, optional
        Channels are converted to eVs following eVs = sdd_gain*channel+sdd_ev0
    sdd_ev0 : float, optional
        Channels are converted to eVs following eVs = sdd_gain*channel+sdd_ev0
    arr_peaks : array of tuples, optional
        Peaks to display, for ex. arr_peaks = [('Elastic', '12000.'), ('Compton', '11670.')]
    selected_sensors : list of str, optional
        List of aliases of sensors, for their mean value to be added to the plot.
    is_use_ev : bool, optional
        Convert the channels to eVs.
    is_xrf_log : bool, optional
        Log on the intensity in the plots.
    absorbers : str, optional
        Text to display which absorber was used.
    path_to_save_dir : str, optional
        Path to the directory where the treated files will be saved.
    is_print_stamps : bool, optional
        Print the list of sensors contained in the nexus file.
    is_plot_spectrogram : bool, optional
        Plot the spectrogram.
    is_plot_sum : bool, optional
        Plot the sum of all the spectrums present in the scan.
    is_plot_first_last : bool, optional
        Plot the first and last spectrums.
    is_save : bool, optional
        Save the results.
    is_print_info : bool, optional
        Verbose mode.

    Returns
    -------
    channels : array
        The extracted channels.
    energies : array
        The extracted channels converted to eVs.
    spectrums : array
        The extracted spectrums (between point 0 and last point with signal).

    """
    channels, energies, spectrums, first_non_zero_spectrum, \
    last_non_zero_spectrum, time_str, sensors_str, stamps_0d, data_0d= \
    extract_xrf_scan(
        nxs_name, path_to_nxs_dir,
        list_sdd_elems, channel_xrf_first, channel_xrf_last,
        sdd_gain, sdd_ev0,
        selected_sensors,
        is_print_stamps, is_print_info
        )

    fig_spectrogram = None
    fig_sum = None
    fig_first_last = None
    is_first_plot = True

    if is_plot_spectrogram:
        fig_spectrogram = plot_xrf_spectrogram(
            channels, energies, spectrums, \
            last_non_zero_spectrum, time_str, sensors_str, is_use_ev, is_first_plot, \
            is_xrf_log, absorbers, nxs_name
            )
        is_first_plot = False

    if is_plot_sum:
        fig_sum = plot_xrf_sum(
            channels, energies, spectrums, arr_peaks, \
            time_str, sensors_str, is_use_ev, is_first_plot, \
            is_xrf_log, absorbers, nxs_name
            )
        is_first_plot = False

    if is_plot_first_last:
        fig_first_last = plot_xrf_first_last(
            channels, energies, spectrums, arr_peaks, \
            first_non_zero_spectrum, \
            time_str, sensors_str, is_use_ev, is_first_plot, \
            is_xrf_log, absorbers, nxs_name
            )
        is_first_plot = False

    if is_save:
        save_xrf_scan(
            nxs_name, path_to_nxs_dir, stamps_0d, data_0d,
            fig_spectrogram, fig_sum, fig_first_last,
            path_to_save_dir, is_print_info
            )

    return channels, energies, spectrums

def extract_xrf_scan(
        nxs_name, path_to_nxs_dir,
        list_sdd_elems, channel_xrf_first, channel_xrf_last,
        sdd_gain, sdd_ev0,
        selected_sensors,
        is_print_stamps, is_print_info):
    """
    Extract the nexus file and return useful quantities.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    list_sdd_elems : list of int
        List of SDD elements, e.g. [0, 1, 2].
    channel_xrf_first : int
        Spectrums are extracted between channel_xrf_first and channel_xrf_last.
    channel_xrf_last : int
        Spectrums are extracted between channel_xrf_first and channel_xrf_last.
    sdd_gain : float
        Channels are converted to eVs following eVs = sdd_gain*channel+sdd_ev0
    sdd_ev0 : float
        Channels are converted to eVs following eVs = sdd_gain*channel+sdd_ev0
    selected_sensors : list of str
        List of aliases of sensors, for their mean value to be added to the plot.
    is_print_stamps : bool
        Print the list of sensors contained in the nexus file.
    is_print_info : bool
        Verbose mode.

    Returns
    -------
    channels : array
        The extracted channels.
    energies : array
        The extracted channels converted to eVs.
    spectrums : array
        The extracted spectrums (between point 0 and last point with signal).
    first_non_zero_spectrum : int
        Argument of the first spectrum with signal.
    last_non_zero_spectrum : int
        Argument of the last spectrum with signal.
    time_str : str
        Starting/ending dates of the scan.
    sensors_str : str
        Label with mean values of selected sensors.
    stamps_0d : array
        Aliases of each 0D sensor in the scan.
    data_0d : array
        Values of each 0D sensor in the scan.

    Raises
    ------
    FileNotFoundError
        If a file or a folder is missing.
    SystemExit('Could not open Nexus file.'))
        If the Nexus file cannot be accessed.
    SystemExit('Required sensor not found.')
        If a required sensor is not found in the sensor list.

    """
    nxs_path = path_to_nxs_dir+nxs_name

    if not os.path.isfile(nxs_path):
        print(_RED+'Scan %s is not present in the file directory.'%(nxs_name)+_RESET)
        print('\t\t file directory : %s'%path_to_nxs_dir)
        raise FileNotFoundError('Missing folder or file.')

    column_epoch = None

    if is_print_info:
        print(_BLUE+" - Open Nexus Data File :"+ _RESET)
        print('\t%s'%nxs_path)

    try:
        nexus = PN.PyNexusFile(nxs_path, fast=True)
    except:
        print(_RED+'\t Could not open Nexus file.'+_RESET)
        raise Exception('Could not open Nexus file.')

    nbpts = int(nexus.get_nbpts())
    if is_print_info:
        print("\t. Number of data points: %s"%nbpts)

    # Extract 0D data
    # Only for the return
    stamps_0d, data_0d = nexus.extractData('0D')

    # Here we work also with the full data list
    stamps, data = nexus.extractData()
    nexus.close()

    # Extract stamps
    if is_print_stamps :
        print("\t. Available Counters:")
        for i, stamp in enumerate(stamps):
            if stamp[1] is not None:
                print("\t\t%s  -------> %s"%(i,stamp[1]))
            else:
                print("\t\t%s  -------> %s"%(i,stamp[0]))

    # Find columns
    for i, stamp_0d in enumerate(stamps_0d):
        if stamp_0d[0].lower()=='sensorstimestamps':
            column_epoch = i

    # Extract time
    time_str = ''
    if column_epoch is not None:
        # Find positions of non Nan values
        args = ~np.isnan(data_0d[column_epoch])
        epoch_time =  data_0d[column_epoch][args]
        time_str = 'Starting time: %s\nEnding time: %s'%(
                   datetime.datetime.fromtimestamp(epoch_time[0]),
                   datetime.datetime.fromtimestamp(epoch_time[-1]))
        # Get first and last position of the non NaN values
        if is_print_info:
            print('\t. Valid data between points %d and %d'
                      %(np.argmax(args), len(args)-np.argmax(args[::-1])-1))

    # Extract the value of the sensors in the list selected_sensors
    sensors_str = ''
    # Find positions of non Nan values based on data[0]
    args = ~np.isnan(data_0d[0])
    if selected_sensors is not None:
        for sensor in selected_sensors:
            for i, stamp_0d in enumerate(stamps_0d):
                if stamp_0d[1] is not None:
                    if stamp_0d[1].lower()==sensor:
                        sensor_value = np.mean(data_0d[i][args])
                        sensors_str += f'{sensor} = {sensor_value}\n'


    # Check the sensors
    is_icr_found, is_ocr_found, is_spectrum_found = \
    check_sdd_elems(
            nxs_name=nxs_name,
            path_to_nxs_dir=path_to_nxs_dir,
            list_sdd_elems=list_sdd_elems
            )

    if (is_icr_found and is_ocr_found and is_spectrum_found):

        # The spectrums summed over the SDD elements
        spectrums_full = np.zeros((nbpts, 2048))

        for sdd_elem in list_sdd_elems:

            for i, stamp in enumerate(stamps):
                if (stamp[1] is not None and stamp[1].lower() == "fluospectrum0"+str(sdd_elem)):
                    spectrum_single_sdd_elem = data[i]

            spectrums_full += spectrum_single_sdd_elem

        # Filter the spectrums (check on which points there was signal)
        ind_non_zero_spectrums = np.where(np.sum(spectrums_full, axis = 1)>10.)[0]
        first_non_zero_spectrum = ind_non_zero_spectrums[0]
        last_non_zero_spectrum = ind_non_zero_spectrums[-1]

        channels = np.arange(int(channel_xrf_first), int(channel_xrf_last+1))
        energies = channels*sdd_gain+sdd_ev0

        # The spectrums list starts at the first point of the scan
        # and stops at the last with signal
        spectrums = spectrums_full[0:last_non_zero_spectrum+1,
                                   int(channel_xrf_first):int(channel_xrf_last+1)]

    else:
        if not is_icr_found:
            print(_RED+'ICR not found in data.'+_RESET)
        if not is_ocr_found:
            print(_RED+'OCR not found in data.'+_RESET)
        if not is_spectrum_found:
            print(_RED+'Spectrum not found in data.'+_RESET)
        raise Exception('Required sensor not found.')

    return channels, energies, spectrums, first_non_zero_spectrum, \
           last_non_zero_spectrum, time_str, sensors_str, stamps_0d, data_0d

def plot_xrf_spectrogram(
    channels, energies, spectrums,
    last_non_zero_spectrum, time_str, sensors_str, is_use_ev, is_first_plot,
    is_xrf_log, absorbers , nxs_name):
    """
    Plot spectrogram of a XRF scan.

    Parameters
    ----------
    channels : array
        The extracted channels.
    energies : array
        The extracted channels converted to eVs.
    spectrums : array
        The extracted spectrums (between point 0 and last point with signal).
    last_non_zero_spectrum : int
        Argument of the last spectrum with signal.
    time_str : str
        Starting/ending dates of the scan.
    sensors_str : str
        Label with mean values of selected sensors.
    is_use_ev : bool
        Convert the channels to eVs.
    is_first_plot : bool
        True if this is the first plot of the series (to print title and text).
    is_xrf_log : bool
        Log on the intensity in the plots.
    absorbers : str
        Text to display which absorber was used.
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.

    Returns
    -------
    fig_spectrogram : matplotlib figure
        The figure of the spectrogram to be saved in pdf.

    """
    fig_spectrogram = plt.figure(figsize=(12,4.6))
    ax1 = fig_spectrogram.add_subplot(111)

    if is_first_plot:
        ax1.set_title(nxs_name.split('\\')[-1], fontsize='x-large')
        if absorbers != '':
            print("Absorbers: %s"%absorbers)
        if time_str != '':
            print(time_str)
        if sensors_str != '':
            print(sensors_str)

    ax1.set_xlabel('Spectrum index', fontsize='large')
    ax1.set_xlim(left=0, right=last_non_zero_spectrum)

    if is_use_ev:
        xx, yy = np.meshgrid(np.arange(0,last_non_zero_spectrum+1), energies)
        ax1.set_ylabel('Energy (eV)', fontsize='large')
    else:
        xx, yy = np.meshgrid(np.arange(0,last_non_zero_spectrum+1), channels)
        ax1.set_ylabel('Channel', fontsize='large')

    if is_xrf_log:
        ax1.pcolormesh(xx, yy, spectrums.transpose(), cmap='viridis',  shading = 'auto',
                       norm = colors.LogNorm(), rasterized=True)
    else:
        ax1.pcolormesh(xx, yy, spectrums.transpose(), cmap='viridis',  shading = 'auto',
                       rasterized=True)

    plt.show()

    return fig_spectrogram

def plot_xrf_sum(
    channels, energies, spectrums, arr_peaks,
    time_str, sensors_str, is_use_ev, is_first_plot,
    is_xrf_log, absorbers , nxs_name):
    """
    Plot sum over all the spectrums of a XRF scan.

    Parameters
    ----------
    channels : array
        The extracted channels.
    energies : array
        The extracted channels converted to eVs.
    spectrums : array
        The extracted spectrums (between point 0 and last point with signal).
    arr_peaks : array of tuples
        Peaks to display, for ex. arr_peaks = [('Elastic', '12000.'), ('Compton', '11670.')]
    time_str : str
        Starting/ending dates of the scan.
    sensors_str : str
        Label with mean values of selected sensors.
    is_use_ev : bool
        Convert the channels to eVs.
    is_first_plot : bool
        True if this is the first plot of the series (to print title and text).
    is_xrf_log : bool
        Log on the intensity in the plots.
    absorbers : str
        Text to display which absorber was used.
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.

    Returns
    -------
    fig_sum : matplotlib figure
        The figure of the sum to be saved in pdf.

    """
    fig_sum = plt.figure(figsize=(10,4.5))
    ax1 = fig_sum.add_subplot(111)

    if is_first_plot:
        ax1.set_title(nxs_name.split('\\')[-1], fontsize='x-large')
        if absorbers != '':
            print("Absorbers: %s"%absorbers)
        if time_str != '':
            print(time_str)
        if sensors_str != '':
            print(sensors_str)

    ax1.set_ylabel('Counts', fontsize='large')

    if is_xrf_log:
        ax1.set_yscale('log')
    if is_use_ev:
        ax1.set_xlabel('Energy (eV)', fontsize='large')
        line1, = ax1.plot(energies, np.sum(spectrums, axis = 0), 'b.-', label='Sum of spectrums')
    else:
        ax1.set_xlabel('Channel', fontsize='large')
        line1, = ax1.plot(channels, np.sum(spectrums, axis = 0), 'b.-', label='Sum of spectrums')

    if arr_peaks[0][0] is not None :
        # Plot the peak positions

        # Prepare a list of colors and linestyles
        colors_axv = iter(['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2',
                           '#7f7f7f', '#bcbd22', '#17becf']*20)
        linestyles_axv = iter(['--', '-.', '-', ':']*40)

        # Rearrange the peaks to plot them by increasing energy
        arr_peaks = np.array(arr_peaks)
        arg_position_peaks = np.argsort([float(elem[1]) for elem in arr_peaks])
        val_position_peaks = arr_peaks[arg_position_peaks][:,1]
        labels_peaks = arr_peaks[arg_position_peaks][:,0]

        axvlines = []
        for i in range(len(arr_peaks)):
            axvlines.append(ax1.axvline(float(val_position_peaks[i]), label = str(labels_peaks[i]),
                                        color = next(colors_axv), linestyle = next(linestyles_axv)))

        axvlegends = ax1.legend(handles=axvlines, fontsize=10, ncol = len(arr_peaks)//16+1,
                                loc='upper right',  borderaxespad=0.3)
        plt.gca().add_artist(axvlegends)

    ax1.legend(handles=[line1], fontsize='large', loc='upper left')
    plt.show()

    return fig_sum

def plot_xrf_first_last(
    channels, energies, spectrums, arr_peaks,
    first_non_zero_spectrum,
    time_str, sensors_str, is_use_ev, is_first_plot,
    is_xrf_log, absorbers , nxs_name):
    """
    Plot first and last spectrums of a XRF scan.

    Parameters
    ----------
    channels : array
        The extracted channels.
    energies : array
        The extracted channels converted to eVs.
    spectrums : array
        The extracted spectrums (between point 0 and last point with signal).
    arr_peaks : array of tuples
        Peaks to display, for ex. arr_peaks = [('Elastic', '12000.'), ('Compton', '11670.')]
    first_non_zero_spectrum : int
        Argument of the first spectrum with signal.
    time_str : str
        Starting/ending dates of the scan.
    sensors_str : str
        Label with mean values of selected sensors.
    is_use_ev : bool
        Convert the channels to eVs.
    is_first_plot : bool
        True if this is the first plot of the series (to print title and text).
    is_xrf_log : bool
        Log on the intensity in the plots.
    absorbers : str
        Text to display which absorber was used.
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.

    Returns
    -------
    fig_first_last: matplotlib figure
        The figure of the first and last spectrums to be saved in pdf.

    """
    fig_first_last = plt.figure(figsize=(12,4.5))
    ax1 = fig_first_last.add_subplot(111)

    if is_first_plot:
        ax1.set_title(nxs_name.split('\\')[-1], fontsize='x-large')
        if absorbers != '':
            print("Absorbers: %s"%absorbers)
        if time_str != '':
            print(time_str)
        if sensors_str != '':
            print(sensors_str)

    ax1.set_ylabel('Counts', fontsize='large')

    if is_xrf_log:
        ax1.set_yscale('log')
    if is_use_ev:
        ax1.set_xlabel('Energy (eV)', fontsize='large')
        line1, = ax1.plot(energies, spectrums[first_non_zero_spectrum], 'b.-', label='First spectrum')
        line2, = ax1.plot(energies, spectrums[-1], 'r.-', label='Last spectrum')
    else:
        ax1.set_xlabel('Channel', fontsize='large')
        line1, = ax1.plot(channels, spectrums[first_non_zero_spectrum], 'b.-', label='First spectrum')
        line2, = ax1.plot(channels, spectrums[-1], 'r.-', label='Last spectrum')

    if arr_peaks[0][0] is not None :
        # Plot the peak positions

        # Prepare a list of colors and linestyles
        colors_axv = iter(['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2',
                           '#7f7f7f', '#bcbd22', '#17becf']*20)
        linestyles_axv = iter(['--', '-.', '-', ':']*40)

        # Rearrange the peaks to plot them by increasing energy
        arr_peaks = np.array(arr_peaks)
        arg_position_peaks = np.argsort([float(elem[1]) for elem in arr_peaks])
        val_position_peaks = arr_peaks[arg_position_peaks][:,1]
        labels_peaks = arr_peaks[arg_position_peaks][:,0]

        axvlines = []
        for i in range(len(arr_peaks)):
            axvlines.append(ax1.axvline(float(val_position_peaks[i]), label = str(labels_peaks[i]),
                                        color = next(colors_axv), linestyle = next(linestyles_axv)))

        axvlegends = ax1.legend(handles=axvlines, fontsize=10, ncol = len(arr_peaks)//16+1,
                                loc='upper right',  borderaxespad=0.3)
        plt.gca().add_artist(axvlegends)

    ax1.legend(handles=[line1, line2], fontsize='large', loc='upper left')
    plt.show()

    return fig_first_last


def save_xrf_scan(
    nxs_name, path_to_nxs_dir, stamps_0d, data_0d,
    fig_spectrogram, fig_sum, fig_first_last,
    path_to_save_dir, is_print_info):
    """
    Save XRF data.

    To avoid passing large variables such as each spectrum (not summed),
    we redo the extraction with PyNexus directly within this function.

    XXX_fluospectrumNN.mat: the matrix corresponding to the spectrums of SDD elements
    NN, in ascii.
    XXX.dat : the value of each sensor at each point of the scan.
    XXX_spectrogram.pdf : the figure of the spectrogram in pdf.
    XXX_sum.pdf : the figure of the sum in pdf.
    XXX_first_last.pdf : the figure of the first and last spectrums in pdf.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    stamps_0d : array
        Aliases of each 0D sensor in the scan.
    data_0d : array
        Values of each 0D sensor in the scan.
    fig_spectrogram: matplotlib  None or matplotlib figure
        The figure of the spectrogram to be saved in pdf.
    fig_sum: matplotlib  None or matplotlib figure
        The figure of the sum to be saved in pdf.
    fig_first_last: matplotlib  None or matplotlib figure
        The figure of the first and last spectrums to be saved in pdf.
    path_to_save_dir : str
        Path to the directory where the treated files will be saved.
    is_print_info : bool
        Verbose mode.

    Raises
    ------
    SystemExit('Could not open Nexus file.'))
        If the Nexus file cannot be accessed.

    """
    # Create Save Name
    save_name = path_to_save_dir + nxs_name[:nxs_name.rfind('.nxs')]

    # Figure in pdf
    if fig_sum is not None:
        plt.figure(fig_sum)
        plt.savefig(save_name+'_sum.pdf', bbox_inches='tight')
        plt.close()
    if fig_spectrogram is not None:
        plt.figure(fig_spectrogram)
        plt.savefig(save_name+'_spectrogram.pdf', bbox_inches='tight')
        plt.close()
    if fig_first_last is not None:
        plt.figure(fig_first_last)
        plt.savefig(save_name+'_first_last.pdf', bbox_inches='tight')
        plt.close()

    ###############################################
    # Save each SDD element individually
    # We use PyNexus to extract the spectrums
    try:
        nexus = PN.PyNexusFile(path_to_nxs_dir+nxs_name, fast=True)
    except:
        print(_RED+'\t Could not open Nexus file.'+_RESET)
        raise Exception('Could not open Nexus file.')

    # Get stamps and data
    stamps, data = nexus.extractData()

    # Extract without printing the sensors in the notebook
    f = io.StringIO()
    with redirect_stdout(f):
        old_nexus_filename = nexus.filename
        nexus.filename = path_to_save_dir + nxs_name
        nexus.saveOneDExtractedData((stamps, data))
        nexus.filename = old_nexus_filename
    out = f.getvalue()
    nexus.close()
    ###############################################

    # Save 0D data
    header_0d = [stamp[1] if stamp[1] is not None else stamp[0] for stamp in stamps_0d]
    header_0d = '\t'.join(header_0d)

    np.savetxt(save_name+'.dat', np.transpose(data_0d), header = header_0d,
               comments = '', fmt='%s', delimiter = '\t')

    if is_print_info:
        print('\t. Dat file saved in:')
        print('\t%s.dat'%save_name)
        print('\t. Spectrum(s) saved in:')
        print('\t%s_fluospectrum*.mat'%save_name)
        if fig_spectrogram is not None:
            print('\t. Figure spectrogram saved in:')
            print('\t%s_spectrogram.pdf'%save_name)
        if fig_sum is not None:
            print('\t. Figure sum saved in:')
            print('\t%s_sum.pdf'%save_name)
        if fig_first_last is not None:
            print('\t. Figure first/last saved in:')
            print('\t%s_first_last.pdf'%save_name)
