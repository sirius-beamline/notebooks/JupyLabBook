"""
Module for processing XRR (on solids and liquids).
"""
import os
import datetime
import numpy as np
import matplotlib.pyplot as plt
from lib.backend import area_detector
from lib.backend import PyNexus as PN

_RED='\x1b[31;01m'
_BLUE='\x1b[34;01m'
_RESET='\x1b[0m'

###############################################################
###############################################################
###############################################################
#######################  XRR LIQUID ###########################
###############################################################
###############################################################
###############################################################


def plot_calib_xrr_liquid(calib_xrr_data, distance_detec):
    """
    Fit and plot the values from the calibration,
    to give the user the coefficient to be used in the XRR scripts (on liquids).

    Parameters
    ----------
    calib_xrr_data : array
        Numpy array containing the values of m4pitch, c10tablepitch, gamma and zs for the calibration.
    distance_detec : float
        Distance between the detector and the center of the sample in mm.

    """
    m4pitch = calib_xrr_data[:,0]
    c10tablepitch = calib_xrr_data[:,1]
    gamma = calib_xrr_data[:,2]
    zs = calib_xrr_data[:,3]

    # Fit with y_data = coef[0]*x_data + coef[1]
    coeff_c10 = np.polyfit(m4pitch, c10tablepitch, 1)
    coeff_zs = np.polyfit(m4pitch, zs, 1)
    coeff_gamma = np.polyfit(m4pitch, gamma, 1)

    # c10tablepitch vs m4pitch
    fig=plt.figure(1, figsize=(13,5))
    ax1=fig.add_subplot(111)
    ax1.plot(m4pitch, c10tablepitch, 'bo')
    ax1.plot(m4pitch, coeff_c10[0]*m4pitch+coeff_c10[1], 'r-', lw=2)
    ax1.set_xlabel('m4pitch (deg)', fontsize=14)
    ax1.set_ylabel('c10tablepitch (deg)', fontsize=14)
    ax1.tick_params(labelsize=14)
    ax1.yaxis.offsetText.set_fontsize(14)
    ax1.text(0.05, 0.9, "coeff_c10tablepitch = %3.4f (deg per deg)"%(coeff_c10[0]), fontsize=14,
             transform=ax1.transAxes)
    plt.show()

    # zs vs m4pitch
    fig=plt.figure(1, figsize=(13,5))
    ax2=fig.add_subplot(111)
    ax2.plot(m4pitch, zs, 'bo')
    ax2.plot(m4pitch, coeff_zs[0]*m4pitch+coeff_zs[1], 'r-', lw=2)
    ax2.set_xlabel('m4pitch (deg)', fontsize=14)
    ax2.set_ylabel('zs (mm)', fontsize=14)
    ax2.tick_params(labelsize=14)
    ax2.yaxis.offsetText.set_fontsize(14)
    ax2.text(0.05, 0.9, "coeff_zs = %3.5f (mm per deg)"%(coeff_zs[0]), fontsize=14,
             transform=ax2.transAxes)
    plt.show()

    # gamma vs m4pitch
    fig=plt.figure(1, figsize=(13,5))
    ax3=fig.add_subplot(111)
    ax3.plot(m4pitch, gamma, 'bo')
    ax3.plot(m4pitch, coeff_gamma[0]*m4pitch+coeff_gamma[1], 'r-', lw=2)
    ax3.set_xlabel('m4pitch (deg)', fontsize=14)
    ax3.set_ylabel('gamma (deg)', fontsize=14)
    ax3.tick_params(labelsize=14)
    ax3.yaxis.offsetText.set_fontsize(14)
    # The coeff to be used in XRR.ipy is not directly the slope of the curve
    coeff_gamma_eff = (coeff_gamma[0]+2)*np.pi*distance_detec/(180.*coeff_zs[0])
    ax3.text(0.05, 0.9, "coeff_gamma = %3.4f "%(coeff_gamma_eff),
             fontsize=14, transform=ax3.transAxes)
    plt.show()

def process_xrr_liquid_scan(
        list_xrr_files, path_to_nxs_dir, direct_scan_name,
        roi_x0, roi_y0, roi_size_x, roi_size_y, summation_roi_size_y,
        m4pitch0, wavelength, fdirect=1.,
        is_bckg_up=True, is_bckg_down=True, is_bckg_left=False, is_bckg_right=False, is_force_direct=True,
        is_track_beam=True, path_to_save_dir='', is_plot_m4pitch=False, is_plot_twotheta=False,
        is_plot_qz=False, is_plot_pos_y_beam=False,
        is_save=False, is_print_info=False):
    """
    Call functions for extracting, plotting, and saving a XRR scan on a liquid.

    Parameters
    ----------
    list_xrr_files : list of str
        List of XRR file names without the '.nxs', e.g. ['SIRIUS_2021_06_18_0568'].
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    direct_scan_name : str
        Nexus filename of the direct scan.
    roi_x0 : int
        x0 of the full-scan ROI.
    roi_y0 : int
        y0 of the full-scan ROI.
    roi_size_x : int
        Size x of the full-scan ROI.
    roi_size_y : int
        Size y of the full-scan ROI.
    summation_roi_size_y : int
        Size y of the summation ROI (centered on the beam).
        It has to be an odd number.
    m4pitch0 : float
        Value of m4pitch0 (m4pitch aligned with the beam) in deg.
    wavelength : float
        Wavelength in nm.
    fdirect : float, optional
        Value of the normalisation to be used if is_force_direct is True
    is_bckg_up : bool, optional
        Take into account the upper ROI for background subtraction.
    is_bckg_down : bool, optional
        Take into account the lower ROI for background subtraction.
    is_bckg_left : bool, optional
        Take into account the left ROI for background subtraction.
    is_bckg_right : bool, optional
        Take into account the right ROI for background subtraction.
    is_force_direct : bool, optional
        Force the normalization to be equal to the value of `fdirect`.
    is_track_beam : bool, optional
        Track the vertical position of the reflected beam.
    path_to_save_dir : str, optional
        Path to the directory where the treated files will be saved.
    is_plot_m4pitch : bool, optional
        Plot the XRR as a function of m4pitch.
    is_plot_twotheta : bool, optional
        Plot the XRR as a function of 2*theta.
    is_plot_qz : bool, optional
        Plot the XRR as a function of qz.
    is_plot_pos_y_beam : bool, optional
        Plot the vertical position of the reflected beam as a function of m4pitch.
    is_save : bool, optional
        Save the results.
    is_print_info : bool, optional
        Verbose mode.

    Returns
    -------
    m4pitch : array
        List of m4pitch (deg).
    theta : array
        List of theta (rad).
    qz : array
        List of qz (nm-1).
    bckg_refl_up : array
        List of integrated intensities of the bckg up.
    bckg_refl_down : array
        List of integrated intensities of the bckg down.
    bckg_refl_left : array
        List of integrated intensities of the bckg left.
    bckg_refl_right : array
        List of integrated intensities of the bckg right.
    bckg_refl : array
        List of averaged chosen backgrounds.
    err_refl : array
        List of error bars of the XRR.
    refl : array
        List of values of the normalized XRR.

    Raises
    ------
    FileNotFoundError
        If a file or a folder is missing.
    SystemExit('Could not open Nexus file.'))
        If the Nexus file cannot be accessed.
    SystemExit('Required sensor not found.')
        If a required sensor is not found in the sensor list.

    """
    # Check if the direct should be extracted from a file,
    # or is given as parameter
    if is_force_direct:
        print('Direct is forced to the value: direct=%g'%fdirect)
        direct_value = fdirect
    else:
        direct_value = extract_direct_xrr_liquid(
                                direct_scan_name, roi_x0, roi_size_x, roi_size_y, summation_roi_size_y,
                                is_bckg_up, is_bckg_down, is_bckg_left, is_bckg_right, is_track_beam,
                                path_to_nxs_dir, is_print_info)
        print('Direct extracted from %s: direct=%g'%(direct_scan_name, direct_value))

    m4pitch, theta, qz, pos_y_beam, bckg_refl_up, bckg_refl_down, \
    bckg_refl_left, bckg_refl_right, bckg_refl, err_refl, refl, \
    time_str = \
    extract_xrr_liquid_scan(
        list_xrr_files, path_to_nxs_dir,
        roi_x0, roi_y0, roi_size_x, roi_size_y, summation_roi_size_y,
        m4pitch0, wavelength, direct_value,
        is_bckg_up, is_bckg_down, is_bckg_left, is_bckg_right,
        is_track_beam, is_print_info
        )

    fig_m4pitch = None
    fig_twotheta = None
    fig_qz = None
    fig_pos_y_beam = None
    is_first_plot = True

    if is_plot_m4pitch:
        fig_m4pitch = plot_xrr_m4pitch(m4pitch, bckg_refl, err_refl, refl,
                                       list_xrr_files, is_first_plot, time_str)
        is_first_plot = False

    if is_plot_twotheta:
        fig_twotheta = plot_xrr_twotheta(theta, bckg_refl, err_refl, refl,
                                         list_xrr_files, is_first_plot, time_str)
        is_first_plot = False

    if is_plot_qz:
        fig_qz = plot_xrr_qz(qz, bckg_refl, err_refl, refl,
                             list_xrr_files, is_first_plot, time_str)
        is_first_plot = False

    if is_plot_pos_y_beam:
        fig_pos_y_beam = plot_xrr_pos_y_beam(m4pitch, pos_y_beam,
                                             list_xrr_files, is_first_plot, time_str)
        is_first_plot = False

    if is_save:
        save_xrr_liquid_scan(
            m4pitch, theta, qz, pos_y_beam,
            bckg_refl_up, bckg_refl_down, bckg_refl_left, bckg_refl_right,
            bckg_refl, err_refl, refl, list_xrr_files,
            fig_m4pitch, fig_twotheta, fig_qz, fig_pos_y_beam,
            path_to_nxs_dir, path_to_save_dir, is_print_info
            )

    return m4pitch, theta, qz, bckg_refl_up, bckg_refl_down, \
           bckg_refl_left, bckg_refl_right, bckg_refl, err_refl, refl


def extract_direct_xrr_liquid(
    direct_scan_name, roi_x0, roi_size_x, roi_size_y, summation_roi_size_y,
    is_bckg_up, is_bckg_down, is_bckg_left, is_bckg_right, is_track_beam,
    path_to_nxs_dir, is_print_info):
    """
    Extract the value of the direct beam.

    Parameters
    ----------
    direct_scan_name : str
        Nexus filename of the direct scan.
    roi_x0 : int
        x0 of the full-scan ROI.
    roi_size_x : int
        Size x of the full-scan ROI.
    roi_size_y : int
        Size y of the full-scan ROI.
    summation_roi_size_y : int
        Size y of the summation ROI (centered on the beam).
        It has to be an odd number.
    is_bckg_up : bool
        Take into account the upper ROI for background subtraction.
    is_bckg_down : bool
        Take into account the lower ROI for background subtraction.
    is_bckg_left : bool
        Take into account the left ROI for background subtraction.
    is_bckg_right : bool
        Take into account the right ROI for background subtraction.
    is_track_beam : bool
        Track the vertical position of the reflected beam.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    is_print_info : bool
        Verbose mode.

    Returns
    -------
    direct_value : float
        Direct after background subtraction and normalization.

    Raises
    ------
    FileNotFoundError
        If a file or a folder is missing.
    SystemExit('Could not open Nexus file.'))
        If the Nexus file cannot be accessed.
    SystemExit('Required sensor not found.')
        If a required sensor is not found in the sensor list.

    """
    # Extract the sum of all images in the time scan
    _, image_direct, _, _, _, _, _, _, _ = area_detector.extract_area_detector_scan(
                    direct_scan_name, path_to_nxs_dir, selected_sensors=None,
                    is_print_stamps=False, is_print_info=False)

    # Replace the intensity of the dead zones with a value of 0
    image_direct = np.where(image_direct<0., 0., image_direct)

    # Integrate along the horizontal axis and find the maximum
    integrated_x_direct = image_direct.sum(axis=1)
    pos_y_direct = np.argmax(integrated_x_direct)

    # Height of the summation ROI
    # Always use an odd height for the summation_roi!
    if is_track_beam:
        dy = summation_roi_size_y
    else:
        dy = roi_size_y

    # Width of the summation ROI
    dx = roi_size_x

    summation_roi_direct = image_direct[pos_y_direct-dy//2 : pos_y_direct+dy//2+1,
                                        roi_x0 : roi_x0+dx]

    bckg_roi_up_direct = image_direct[pos_y_direct-dy-dy//2 : pos_y_direct-dy+dy//2+1,
                                      roi_x0 : roi_x0+dx]

    bckg_roi_down_direct = image_direct[pos_y_direct+dy-dy//2 : pos_y_direct+dy+dy//2+1,
                                        roi_x0 : roi_x0+dx]

    bckg_roi_left_direct = image_direct[pos_y_direct-dy//2 : pos_y_direct+dy//2+1,
                                        roi_x0-dx : roi_x0]

    bckg_roi_right_direct = image_direct[pos_y_direct-dy//2 : pos_y_direct+dy//2+1,
                                         roi_x0+dx : roi_x0+2*dx]

    # Extract info from nexus file
    nxs_path = path_to_nxs_dir+direct_scan_name
    if is_print_info:
        print(_BLUE+" - Open Nexus Data File :"+ _RESET)
        print('\t%s'%nxs_path)
    try:
        nexus = PN.PyNexusFile(nxs_path, fast=True)
    except:
        print(_RED+'\t Could not open Nexus file.'+_RESET)
        raise Exception('Could not open Nexus file.')

    stamps_0d, data_0d = nexus.extractData('0D')

    nbpts = int(nexus.get_nbpts())
    if is_print_info:
        print("\t. Number of data points: %s"%nbpts)

    nexus.close()

    sensor_list = [stamps_0d[i][0].lower() if stamps_0d[i][1] is None \
                   else stamps_0d[i][1].lower() for i in range(len(stamps_0d))]

    integration_time_arg = sensor_list.index('integration_time')
    integration_time = np.mean(data_0d[integration_time_arg])

    # Extract the voltage and gain of the ionization chamber
    if 'ionchamber' in sensor_list:
        ionchamber_arg = sensor_list.index('ionchamber')
    else:
        print(_RED+'\t Sensor %s is not in the sensor list'%('ionchamber')+_RESET)
        raise Exception('Required sensor not found.')
    if 'commandfemtoionchamber' in sensor_list:
        gain_arg = sensor_list.index('commandfemtoionchamber')
    else:
        print(_RED+'\t Sensor %s is not in the sensor list'%('commandfemtoionchamber')+_RESET)
        raise Exception('Required sensor not found.')

    gain = data_0d[gain_arg].astype(float)
    volt_direct = np.mean(data_0d[ionchamber_arg])*10**(-gain[0])

    # Sum the ROI and normalize with the integration time and the number of images
    # int stands for intensity
    int_raw_direct = summation_roi_direct.sum()/integration_time/nbpts

    int_bckg_up_direct = bckg_roi_up_direct.sum()/integration_time/nbpts
    int_bckg_down_direct = bckg_roi_down_direct.sum()/integration_time/nbpts
    int_bckg_left_direct = bckg_roi_left_direct.sum()/integration_time/nbpts
    int_bckg_right_direct = bckg_roi_right_direct.sum()/integration_time/nbpts

    if any([is_bckg_up, is_bckg_down, is_bckg_left, is_bckg_right]):
        # Take the average of the chosen backgrounds ROIs
        int_bckg_direct = (is_bckg_up*int_bckg_up_direct+is_bckg_down*int_bckg_down_direct\
                         +is_bckg_left*int_bckg_left_direct+is_bckg_right*int_bckg_right_direct)\
                         /(is_bckg_up*1.+is_bckg_down*1.+is_bckg_left*1.+is_bckg_right*1.)
    else:
        int_bckg_direct = 0.

    int_sub_direct = int_raw_direct - int_bckg_direct

    direct_value = int_sub_direct/volt_direct

    return direct_value

def extract_xrr_liquid_scan(
    list_xrr_files, path_to_nxs_dir,
    roi_x0, roi_y0, roi_size_x, roi_size_y, summation_roi_size_y,
    m4pitch0, wavelength, direct_value,
    is_bckg_up, is_bckg_down, is_bckg_left, is_bckg_right,
    is_track_beam, is_print_info):
    """
    Extract the XRR from a series of scans.

    Parameters
    ----------
    list_xrr_files : list of str
        List of XRR file names without the '.nxs', e.g. ['SIRIUS_2021_06_18_0568'].
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    roi_x0 : int
        x0 of the full-scan ROI.
    roi_y0 : int
        y0 of the full-scan ROI.
    roi_size_x : int
        Size x of the full-scan ROI.
    roi_size_y : int
        Size y of the full-scan ROI.
    summation_roi_size_y : int
        Size y of the summation ROI (centered on the beam).
        It has to be an odd number.
    m4pitch0 : float
        Value of m4pitch0 (m4pitch aligned with the beam) in deg.
    wavelength : float
        Wavelength in nm.
    direct_value : float
        Direct after background subtraction and normalization.
    is_bckg_up : bool
        Take into account the upper ROI for background subtraction.
    is_bckg_down : bool
        Take into account the lower ROI for background subtraction.
    is_bckg_left : bool
        Take into account the left ROI for background subtraction.
    is_bckg_right : bool
        Take into account the right ROI for background subtraction.
    is_track_beam : bool
        Track the vertical position of the reflected beam.
    is_print_info : bool
        Verbose mode.

    Returns
    -------
    m4pitch : array
        List of m4pitch (deg).
    theta : array
        List of theta (rad).
    qz : array
        List of qz (nm-1).
    pos_y_beam : array
        List of vertical positions of the reflected beam (pix).
    bckg_refl_up : array
        List of integrated intensities of the bckg up.
    bckg_refl_down : array
        List of integrated intensities of the bckg down.
    bckg_refl_left : array
        List of integrated intensities of the bckg left.
    bckg_refl_right : array
        List of integrated intensities of the bckg right.
    bckg_refl : array
        List of averaged chosen backgrounds.
    err_refl : array
        List of error bars of the XRR.
    refl : array
        List of values of the normalized XRR.
    time_str : str
        Starting/ending dates of the whole XRR scan.

    Raises
    ------
    FileNotFoundError
        If a file or a folder is missing.
    SystemExit('Could not open Nexus file.'))
        If the Nexus file cannot be accessed.
    SystemExit('Required sensor not found.')
        If a required sensor is not found in the sensor list.

    """
    # Summation ROI
    # Height of the summation ROI
    dy = summation_roi_size_y
    if dy%2 == 0:
        print("\033[1msummation_roi_size_y has to be an odd number!\033[0m")
    # Width of the summation ROI
    dx = roi_size_x

    ######################################
    # Extract the reflected beams
    ######################################

    int_raw_refl = np.array([])
    int_bckg_up = np.array([])
    int_bckg_down = np.array([])
    int_bckg_left = np.array([])
    int_bckg_right = np.array([])
    m4pitch = np.array([])
    pos_y_beam = np.array([])
    volt_refl = np.array([])

    for file in list_xrr_files:

        nxs_name = file+'.nxs'

        # Extract info from nexus file
        nxs_path = path_to_nxs_dir+nxs_name
        if is_print_info:
            print(_BLUE+" - Open Nexus Data File :"+ _RESET)
            print('\t%s'%nxs_path)
        try:
            nexus = PN.PyNexusFile(nxs_path, fast=True)
        except:
            print(_RED+'\t Could not open Nexus file.'+_RESET)
            raise Exception('Could not open Nexus file.')

        stamps_0d, data_0d = nexus.extractData("0D")

        nbpts = int(nexus.get_nbpts())
        if is_print_info:
            print("\t. Number of data points: %s"%nbpts)

        nexus.close()

        sensor_list = [stamps_0d[i][0].lower() if stamps_0d[i][1] is None\
                       else stamps_0d[i][1].lower() for i in range(len(stamps_0d))]

        if 'm4pitch' in sensor_list:
            m4pitch_arg = sensor_list.index('m4pitch')
        else:
            print(_RED+'\t Sensor %s is not in the sensor list'%('m4pitch')+_RESET)
            raise Exception('Required sensor not found.')

        if 'integration_time' in sensor_list:
            integration_time_arg = sensor_list.index('integration_time')
        else:
            print(_RED+'\t Sensor %s is not in the sensor list'%('integration_time')+_RESET)
            raise Exception('Required sensor not found.')

        # Extract the voltage and gain of the ionization chamber
        if 'ionchamber' in sensor_list:
            ionchamber_arg = sensor_list.index('ionchamber')
        else:
            print(_RED+'\t Sensor %s is not in the sensor list'%('ionchamber')+_RESET)
            raise Exception('Required sensor not found.')
        if 'commandfemtoionchamber' in sensor_list:
            gain_arg = sensor_list.index('commandfemtoionchamber')
        else:
            print(_RED+'\t Sensor %s is not in the sensor list'%('commandfemtoionchamber')+_RESET)
            raise Exception('Required sensor not found.')

        m4pitch = np.append(m4pitch, np.mean(data_0d[m4pitch_arg]))
        integration_time = np.mean(data_0d[integration_time_arg])
        gain = data_0d[gain_arg].astype(float)
        volt_refl = np.append(volt_refl, np.nanmean(data_0d[ionchamber_arg])*10**(-gain[0]))

        # Extract time
        if file == list_xrr_files[0]:
            time_init = None
            if 'sensorstimestamps' in sensor_list:
                column_epoch = sensor_list.index('sensorstimestamps')
                # Find positions of non Nan values
                args = ~np.isnan(data_0d[column_epoch])
                epoch_time =  data_0d[column_epoch][args]
                time_init = epoch_time[0]
        if file == list_xrr_files[-1]:
            time_end = None
            if 'sensorstimestamps' in sensor_list:
                column_epoch = sensor_list.index('sensorstimestamps')
                # Find positions of non Nan values
                args = ~np.isnan(data_0d[column_epoch])
                epoch_time =  data_0d[column_epoch][args]
                time_end = epoch_time[-1]

        # Extract the sum of all images in the time scan
        _, image, _, _, _, _, _, _, _ = area_detector.extract_area_detector_scan(
                        nxs_name, path_to_nxs_dir, selected_sensors=None,
                        is_print_stamps=False, is_print_info=False)

        # Replace the intensity of the dead zones with a value of 0
        image = np.where(image<0., 0., image)

        # Apply the full-scan ROI
        image_full_scan_roi = image[roi_y0:roi_y0+roi_size_y,
                                    roi_x0:roi_x0+roi_size_x]

        if is_track_beam:

            ###########################
            # Find the reflected beam
            ###########################

            # Integrate along the horizontal axis and find the maximum
            integrated_x = image_full_scan_roi.sum(axis=1)
            pos_max_y = np.argmax(integrated_x)+roi_y0

            # Prediction of the pos_y based on linear interpolation of previous ones
            if len(pos_y_beam)>5:
                a, b = np.polyfit(np.arange(len(pos_y_beam)), pos_y_beam, 1)
                pos_pred_y = int(a*np.arange(len(pos_y_beam)+1)[-1]+b)

                # If there is a shift of more than 2 pixels with the prediction, take the prediction
                if np.abs(pos_pred_y-pos_max_y)>2:
                    pos_y0 = pos_pred_y
                else:
                    pos_y0 = pos_max_y

                # Do a rocking curve and find the max
                summation_roi0 = image[pos_y0-dy//2 : pos_y0+dy//2+1,
                                       roi_x0 : roi_x0+dx]

                # Move to the max of the rocking curve
                pos_max_rc = np.argmax(summation_roi0.sum(axis=1))
                pos_y_rc = pos_y0-dy//2+pos_max_rc

                # If there is a shift of more than 2 pixels with the prediction, take the prediction
                if np.abs(pos_pred_y-pos_y_rc)>2:
                    pos_y = pos_pred_y
                else:
                    pos_y = pos_y_rc

            else:
                pos_y = pos_max_y

        else:
            # If we do not track the beam, we take the center of the full-scan ROI
            # and integrate over it
            pos_y = int(roi_y0+roi_size_y//2)
            dy = roi_size_y

        pos_y_beam = np.append(pos_y_beam, int(pos_y))

        # Summation ROI
        summation_roi = image[pos_y-dy//2 : pos_y+dy//2+1,
                              roi_x0 : roi_x0+dx]

        # Up & down background ROIs
        bckg_roi_up = image[pos_y-dy-dy//2 : pos_y-dy+dy//2+1,
                            roi_x0 : roi_x0+dx]
        bckg_roi_down = image[pos_y+dy-dy//2 : pos_y+dy+dy//2+1,
                              roi_x0 : roi_x0+dx]

        # Left & right background ROIs
        bckg_roi_left = image[pos_y-dy//2 : pos_y+dy//2+1,
                              roi_x0-dx : roi_x0]
        bckg_roi_right = image[pos_y-dy//2 : pos_y+dy//2+1,
                              roi_x0+dx : roi_x0+2*dx]

        integration_time_arg = sensor_list.index('integration_time')
        integration_time = np.mean(data_0d[integration_time_arg])

        int_raw_refl = np.append(int_raw_refl, summation_roi.sum()/integration_time/nbpts)
        int_bckg_up = np.append(int_bckg_up, bckg_roi_up.sum()/integration_time/nbpts)
        int_bckg_down = np.append(int_bckg_down, bckg_roi_down.sum()/integration_time/nbpts)
        int_bckg_left = np.append(int_bckg_left, bckg_roi_left.sum()/integration_time/nbpts)
        int_bckg_right = np.append(int_bckg_right, bckg_roi_right.sum()/integration_time/nbpts)

        if any([is_bckg_up, is_bckg_down, is_bckg_left, is_bckg_right]):
            # Take the average of the chosen backgrounds ROIs
            if is_print_info:
                print('Background taken: '+is_bckg_up*'up '+is_bckg_down*'down '\
                      +is_bckg_left*'left '+is_bckg_right*'right ')

            int_bckg_refl = (is_bckg_up*int_bckg_up+is_bckg_down*int_bckg_down\
                           +is_bckg_left*int_bckg_left+is_bckg_right*int_bckg_right)\
                          /(is_bckg_up*1.+is_bckg_down*1.+is_bckg_left*1.+is_bckg_right*1.)

            err_int_sub_refl = np.sqrt(int_raw_refl)+\
                             (is_bckg_up*np.sqrt(int_bckg_up)+is_bckg_down*np.sqrt(int_bckg_down)\
                              +is_bckg_left*np.sqrt(int_bckg_left)+is_bckg_right*np.sqrt(int_bckg_right))\
                            /(is_bckg_up*1.+is_bckg_down*1.+is_bckg_left*1.+is_bckg_right*1.)
        else:
            if is_print_info:
                print('No subtraction done.')
            int_bckg_refl = 0.*int_bckg_up
            err_int_sub_refl = np.sqrt(int_raw_refl)

        int_sub_refl = int_raw_refl - int_bckg_refl
        


    time_str = ''
    if (time_init is not None and time_end is not None):
        time_str = 'Starting time: %s\nEnding time: %s'%(
                   datetime.datetime.fromtimestamp(time_init),
                   datetime.datetime.fromtimestamp(time_end))

    # Normalize by V and the direct to get the reflectivity
    refl = int_sub_refl/volt_refl/direct_value
    err_refl = err_int_sub_refl/volt_refl/direct_value
    bckg_refl_up = int_bckg_up/volt_refl/direct_value
    bckg_refl_down = int_bckg_down/volt_refl/direct_value
    bckg_refl_left = int_bckg_left/volt_refl/direct_value
    bckg_refl_right = int_bckg_right/volt_refl/direct_value
    bckg_refl = int_bckg_refl/volt_refl/direct_value

    ######################################
    # Convert m4pitch to theta and qz
    ######################################
    theta = np.abs(2*(m4pitch-m4pitch0)*np.pi/180.) #Incident angle in rad
    qz = 4*np.pi/wavelength*np.sin(theta) #qz in nm-1

    return m4pitch, theta, qz, pos_y_beam, bckg_refl_up, bckg_refl_down,\
           bckg_refl_left, bckg_refl_right, bckg_refl, err_refl, refl,\
           time_str

def plot_xrr_m4pitch(
    m4pitch, bckg_refl, err_refl, refl,
    list_xrr_files, is_first_plot, time_str):
    """
    Plot XRR as a function of m4pitch.

    Parameters
    ----------
    m4pitch : array
        List of m4pitch (deg).
    bckg_refl : array
        List of averaged chosen backgrounds.
    err_refl : array
        List of error bars of the XRR.
    refl : array
        List of values of the normalized XRR.
    list_xrr_files : list of str
        List of XRR file names without the '.nxs', e.g. ['SIRIUS_2021_06_18_0568'].
    is_first_plot : bool
        True if this is the first plot of the series (to print title and text).
    time_str : str
        Starting/ending dates of the whole XRR scan.

    Returns
    -------
    fig_m4pitch : matplotlib figure
        The figure to be saved in pdf.

    """
    fig_m4pitch = plt.figure(figsize=(12,5))
    ax = fig_m4pitch.add_subplot(111)

    if  len(list_xrr_files)>1:
        title = list_xrr_files[0]+'-'+list_xrr_files[-1].split('_')[-1]
    else:
        title = list_xrr_files[0]

    if is_first_plot:
        ax.set_title(title, fontsize='x-large')
        if time_str != '':
            print(time_str)

    plt.yscale('log')
    plt.errorbar(m4pitch, refl, err_refl, fmt = 'x-k', label = 'XRR')
    #plt.plot(m4pitch, bckg_refl_up,'g--', label = 'Background up')
    #plt.plot(m4pitch, bckg_refl_down,'m--', label = 'Background down')
    plt.plot(m4pitch, bckg_refl,'r-', label = 'Background')
    plt.plot(m4pitch, bckg_refl+refl,'b-', label = 'XRR before subtraction')
    plt.legend()
    ax.set_xlabel('m4pitch', fontsize=16)
    ax.set_ylabel('R', fontsize=16)

    fig_m4pitch.subplots_adjust(top=0.9)

    ax.tick_params(labelsize=16)
    ax.yaxis.offsetText.set_fontsize(16)

    plt.show()
    return fig_m4pitch

def plot_xrr_twotheta(
    theta, bckg_refl, err_refl, refl,
    list_xrr_files, is_first_plot, time_str):
    """
    Plot XRR as a function of 2*theta.

    Parameters
    ----------
    theta : array
        List of theta (rad).
    bckg_refl : array
        List of averaged chosen backgrounds.
    err_refl : array
        List of error bars of the XRR.
    refl : array
        List of values of the normalized XRR.
    list_xrr_files : list of str
        List of XRR file names without the '.nxs', e.g. ['SIRIUS_2021_06_18_0568'].
    is_first_plot : bool
        True if this is the first plot of the series (to print title and text).
    time_str : str
        Starting/ending dates of the whole XRR scan.

    Returns
    -------
    fig_twotheta : matplotlib figure
        The figure to be saved in pdf.

    """
    fig_twotheta = plt.figure(figsize=(12,5))
    ax = fig_twotheta.add_subplot(111)

    if  len(list_xrr_files)>1:
        title = list_xrr_files[0]+'-'+list_xrr_files[-1].split('_')[-1]
    else:
        title = list_xrr_files[0]

    if is_first_plot:
        ax.set_title(title, fontsize='x-large')
        if time_str != '':
            print(time_str)

    twotheta = 2*theta*180./np.pi # in deg

    plt.yscale('log')
    plt.errorbar(twotheta, refl, err_refl, fmt = 'x-k', label = 'XRR')
    #plt.plot(twotheta, bckg_refl_up,'g--', label = 'Background up')
    #plt.plot(twotheta, bckg_refl_down,'m--', label = 'Background down')
    plt.plot(twotheta, bckg_refl,'r-', label = 'Background')
    plt.plot(twotheta, bckg_refl+refl,'b-', label = 'XRR before subtraction')
    plt.legend()
    ax.set_xlabel('2*theta (deg)', fontsize=16)
    ax.set_ylabel('R', fontsize=16)

    fig_twotheta.subplots_adjust(top=0.9)

    ax.tick_params(labelsize=16)
    ax.yaxis.offsetText.set_fontsize(16)

    plt.show()
    return fig_twotheta

def plot_xrr_qz(
    qz, bckg_refl, err_refl, refl,
    list_xrr_files, is_first_plot, time_str):
    """
    Plot XRR as a function of qz.

    Parameters
    ----------
    qz : array
        List of qz (nm-1).
    bckg_refl : array
        List of averaged chosen backgrounds.
    err_refl : array
        List of error bars of the XRR.
    refl : array
        List of values of the normalized XRR.
    list_xrr_files : list of str
        List of XRR file names without the '.nxs', e.g. ['SIRIUS_2021_06_18_0568'].
    is_first_plot : bool
        True if this is the first plot of the series (to print title and text).
    time_str : str
        Starting/ending dates of the whole XRR scan.

    Returns
    -------
    fig_qz : matplotlib figure
        The figure to be saved in pdf.

    """
    fig_qz, axs = plt.subplots(2, 1, sharex=True, figsize=(12,9))
    fig_qz.subplots_adjust(top=0.94)

    if  len(list_xrr_files)>1:
        title = list_xrr_files[0]+'-'+list_xrr_files[-1].split('_')[-1]
    else:
        title = list_xrr_files[0]

    if is_first_plot:
        fig_qz.suptitle(title, fontsize=16)
        if time_str != '':
            print(time_str)

    fig_qz.subplots_adjust(hspace=0)

    axs[0].errorbar(qz, refl, err_refl, fmt = 'x-k', label = 'XRR')
    #axs[0].plot(qz, bckg_refl_up,'g--', label = 'Background up')
    #axs[0].plot(qz, bckg_refl_down,'m--', label = 'Background down')
    axs[0].plot(qz, bckg_refl,'r-', label = 'Background')
    axs[0].plot(qz, bckg_refl+refl,'b-', label = 'XRR before subtraction')
    axs[0].set_ylabel('R', fontsize=16)
    axs[0].tick_params(labelsize=16)
    axs[0].yaxis.offsetText.set_fontsize(16)
    axs[0].set_yscale('log')
    axs[0].legend()

    refl_qz_4 = refl[refl>0]*qz[refl>0]**(4)
    err_refl_qz_4 = err_refl[refl>0]*qz[refl>0]**(4)

    axs[1].errorbar(qz[refl>0], refl_qz_4, err_refl_qz_4, fmt = 'x-k', label = 'XRR')
    axs[1].set_ylabel('R*qz^4', fontsize=16)
    axs[1].tick_params(labelsize=16)
    axs[1].yaxis.offsetText.set_fontsize(16)
    axs[1].set_yscale('log')
    axs[1].set_xlabel('qz (nm-1)', fontsize=16)

    axs[1].set_ylim(np.min(refl_qz_4[np.argmax(refl_qz_4):])*0.5)

    plt.show()
    return fig_qz

def plot_xrr_pos_y_beam(
    m4pitch, pos_y_beam,
    list_xrr_files, is_first_plot, time_str):
    """
    Plot the vertical position of the reflected beam
    as a function of m4pitch.

    Parameters
    ----------
    m4pitch : array
        List of m4pitch (deg).
    pos_y_beam : array
        List of vertical positions of the reflected beam (pix).
    list_xrr_files : list of str
        List of XRR file names without the '.nxs', e.g. ['SIRIUS_2021_06_18_0568'].
    is_first_plot : bool
        True if this is the first plot of the series (to print title and text).
    time_str : str
        Starting/ending dates of the whole XRR scan.

    Returns
    -------
    fig_pos_y_beam : matplotlib figure
        The figure to be saved in pdf.

    """
    fig_pos_y_beam = plt.figure(figsize=(12,5))
    ax = fig_pos_y_beam.add_subplot(111)
    plt.plot(m4pitch, pos_y_beam,'kx-')

    if  len(list_xrr_files)>1:
        title = list_xrr_files[0]+'-'+list_xrr_files[-1].split('_')[-1]
    else:
        title = list_xrr_files[0]

    if is_first_plot:
        fig_pos_y_beam.suptitle(title, fontsize=16)
        if time_str != '':
            print(time_str)

    ax.set_xlabel('m4pitch', fontsize=16)
    ax.set_ylabel('Pixel', fontsize=16)
    fig_pos_y_beam.subplots_adjust(top=0.9)
    ax.tick_params(labelsize=16)
    ax.yaxis.offsetText.set_fontsize(16)

    plt.show()
    return fig_pos_y_beam


def save_xrr_liquid_scan(
    m4pitch, theta, qz, pos_y_beam,
    bckg_refl_up, bckg_refl_down, bckg_refl_left, bckg_refl_right,
    bckg_refl, err_refl, refl, list_xrr_files,
    fig_m4pitch, fig_twotheta, fig_qz, fig_pos_y_beam,
    path_to_nxs_dir, path_to_save_dir, is_print_info):
    """
    Save data of a XRR scan on a liquid.

    To save the .dat of each individual file,
    we redo the extraction with PyNexus directly within this function.

    XXX_XRR.dat: different parameters relevant for XRR for each point of m4pitch taken.
    XXX.dat: the value of each sensor at each point of each scan in the list of XRR files.
    XXX_XRR_m4pitch.pdf, XXX_XRR_qz.pdf,
    XXX_XRR_twotheta.pdf, XXX_XRR_pos_y_beam.pdf: the figures in pdf.

    Parameters
    ----------
    m4pitch : array
        List of m4pitch (deg).
    theta : array
        List of theta (rad).
    qz : array
        List of qz (nm-1).
    pos_y_beam : array
        List of vertical positions of the reflected beam (pix).
    bckg_refl_up : array
        List of integrated intensities of the bckg up.
    bckg_refl_down : array
        List of integrated intensities of the bckg down.
    bckg_refl_left : array
        List of integrated intensities of the bckg left.
    bckg_refl_right : array
        List of integrated intensities of the bckg right.
    bckg_refl : array
        List of averaged chosen backgrounds.
    err_refl : array
        List of error bars of the XRR.
    refl : array
        List of values of the normalized XRR.
    list_xrr_files : list of str
        List of XRR file names without the '.nxs', e.g. ['SIRIUS_2021_06_18_0568'].
    fig_m4pitch : matplotlib figure
        The figure to be saved in pdf.
    fig_twotheta : matplotlib figure
        The figure to be saved in pdf.
    fig_qz : matplotlib figure
        The figure to be saved in pdf.
    fig_pos_y_beam : matplotlib figure
        The figure to be saved in pdf.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    path_to_save_dir : str
        Path to the directory where the treated files will be saved.
    is_print_info : bool
        Verbose mode.

    Raises
    ------
    SystemExit('Could not open Nexus file.'))
        If the Nexus file cannot be accessed.

    """
    # Create Save Name
    if  len(list_xrr_files)>1:
        save_name = path_to_save_dir + list_xrr_files[0]+'-'+list_xrr_files[-1].split('_')[-1]
    else:
        save_name = path_to_save_dir + list_xrr_files[0]

    # Figure in pdf
    if fig_m4pitch is not None:
        plt.figure(fig_m4pitch)
        plt.savefig(save_name+'_XRR_m4pitch.pdf', bbox_inches='tight')
        plt.close()
    if fig_qz is not None:
        plt.figure(fig_qz)
        plt.savefig(save_name+'_XRR_qz.pdf', bbox_inches='tight')
        plt.close()
    if fig_pos_y_beam is not None:
        plt.figure(fig_pos_y_beam)
        plt.savefig(save_name+'_pos_y_beam.pdf', bbox_inches='tight')
        plt.close()
    if fig_twotheta is not None:
        plt.figure(fig_twotheta)
        plt.savefig(save_name+'_XRR_twotheta.pdf', bbox_inches='tight')
        plt.close()

    # Save XRR
    print('Save in progress...')
    twotheta = 2*theta*180./np.pi # in deg
    tobesaved = [qz, refl, err_refl, twotheta, m4pitch, pos_y_beam, bckg_refl_up, bckg_refl_down,\
                 bckg_refl_left, bckg_refl_right, bckg_refl]
    header = '#qz(nm-1)\t#refl\t#err_refl\t#twotheta(deg)\t#m4pitch(deg)\t#pos_y_beam(pix)\t'+\
              '#bckg_refl_up\t#bckg_refl_down\t#bckg_refl_left\t#bckg_refl_right\t#bckg_refl'
    np.savetxt(save_name+'_XRR.dat', np.transpose(tobesaved),
               delimiter = '\t', comments='', fmt = '%.7e',
               header = header)
    
    """
    # Removed, too time-consuming
    # Save the .dat file of each file
    for file in list_xrr_files:
        try:
            nexus = PN.PyNexusFile(path_to_nxs_dir+file+'.nxs', fast=True)
        except:
            print(_RED+'\t Could not open Nexus file.'+_RESET)
            raise Exception('Could not open Nexus file.')

        # Get stamps and data
        stamps_0d, data_0d = nexus.extractData('0D')
        nexus.close()

        # Save 0D data
        header_0d = [stamp[1] if stamp[1] is not None else stamp[0] for stamp in stamps_0d]
        header_0d = '\t'.join(header_0d)

        np.savetxt(path_to_save_dir+file+'.dat', np.transpose(data_0d), header = header_0d,
                   comments = '', fmt='%s', delimiter = '\t')
        if is_print_info:
            print('\t. Dat file saved in:')
            print('\t%s.dat'%(path_to_save_dir+file))
    """
    
    # Save the .dat file of the first file
    file = list_xrr_files[0]
    try:
        nexus = PN.PyNexusFile(path_to_nxs_dir+file+'.nxs', fast=True)
    except:
        print(_RED+'\t Could not open Nexus file.'+_RESET)
        raise Exception('Could not open Nexus file.')

    # Get stamps and data
    stamps_0d, data_0d = nexus.extractData('0D')
    nexus.close()

    # Save 0D data
    header_0d = [stamp[1] if stamp[1] is not None else stamp[0] for stamp in stamps_0d]
    header_0d = '\t'.join(header_0d)

    np.savetxt(path_to_save_dir+file+'.dat', np.transpose(data_0d), header = header_0d,
               comments = '', fmt='%s', delimiter = '\t')
    if is_print_info:
        print('\t. Dat file saved in:')
        print('\t%s.dat'%(path_to_save_dir+file))

    print('Save done!')

    if is_print_info:
        print('\t. XRR saved in:')
        print('\t%s_XRR.dat'%save_name)
        if fig_m4pitch is not None:
            print('\t. Figure m4pitch saved in:')
            print('\t%s_XRR_m4pitch.pdf'%save_name)
        if fig_qz is not None:
            print('\t. Figure qz saved in:')
            print('\t%s_XRR_qz.pdf'%save_name)
        if fig_twotheta is not None:
            print('\t. Figure 2*theta saved in:')
            print('\t%s_XRR_twotheta.pdf'%save_name)
        if fig_pos_y_beam is not None:
            print('\t. Figure position beam saved in:')
            print('\t%s_pos_y_beam.pdf'%save_name)

###############################################################
###############################################################
###############################################################
#######################  XRR SOLID ############################
###############################################################
###############################################################
###############################################################

def process_xrr_solid_scan(
        list_xrr_files, path_to_nxs_dir, direct_scan_name,
        roi_x0, roi_y0, roi_size_x, roi_size_y,
        wavelength, fdirect=1., is_bckg_up=True, is_bckg_down=True,
        is_bckg_left=False, is_bckg_right=False, is_force_direct=True,
        path_to_save_dir='', is_plot_twotheta=False, is_plot_qz=False,
        is_save=False, is_print_info=False):
    """
    Call functions for extracting, plotting, and saving a XRR scan on a solid.

    Parameters
    ----------
    list_xrr_files : list of str
        List of XRR file names without the '.nxs', e.g. ['SIRIUS_2021_06_18_0568'].
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    direct_scan_name : str
        Nexus filename of the direct scan.
    roi_x0 : int
        x0 of the full-scan ROI.
    roi_y0 : int
        y0 of the full-scan ROI.
    roi_size_x : int
        Size x of the full-scan ROI.
    roi_size_y : int
        Size y of the full-scan ROI.
        It has to be an odd number.
    wavelength : float
        Wavelength in nm.
    fdirect : float, optional
        Value of the normalisation to be used if is_force_direct is True
    is_bckg_up : bool, optional
        Take into account the upper ROI for background subtraction.
    is_bckg_down : bool, optional
        Take into account the lower ROI for background subtraction.
    is_bckg_left : bool, optional
        Take into account the left ROI for background subtraction.
    is_bckg_right : bool, optional
        Take into account the right ROI for background subtraction.
    is_force_direct : bool, optional
        Force the normalization to be equal to the value of `fdirect`.
    path_to_save_dir : str, optional
        Path to the directory where the treated files will be saved.
    is_plot_twotheta : bool, optional
        Plot the XRR as a function of 2*theta.
    is_plot_qz : bool, optional
        Plot the XRR as a function of qz.
    is_save : bool, optional
        Save the results.
    is_print_info : bool, optional
        Verbose mode.

    Returns
    -------
    theta : array
        List of theta (rad).
    qz : array
        List of qz (nm-1).
    bckg_refl_up : array
        List of integrated intensities of the bckg up.
    bckg_refl_down : array
        List of integrated intensities of the bckg down.
    bckg_refl_left : array
        List of integrated intensities of the bckg left.
    bckg_refl_right : array
        List of integrated intensities of the bckg right.
    bckg_refl : array
        List of averaged chosen backgrounds.
    err_refl : array
        List of error bars of the XRR.
    refl : array
        List of values of the normalized XRR.

    Raises
    ------
    FileNotFoundError
        If a file or a folder is missing.
    SystemExit('Could not open Nexus file.'))
        If the Nexus file cannot be accessed.
    SystemExit('Required sensor not found.')
        If a required sensor is not found in the sensor list.

    """
    # Check if the direct should be extracted from a file,
    # or is given as parameter
    if is_force_direct:
        print('Direct is forced to the value: direct=%g'%fdirect)
        direct_value = fdirect
    else:
        direct_value = extract_direct_xrr_solid(
                                direct_scan_name, roi_x0, roi_size_x, roi_size_y,
                                is_bckg_up, is_bckg_down, is_bckg_left, is_bckg_right,
                                path_to_nxs_dir, is_print_info)

    theta, qz, bckg_refl_up, bckg_refl_down, \
    bckg_refl_left, bckg_refl_right, bckg_refl, err_refl, refl, \
    time_str = \
    extract_xrr_solid_scan(
        list_xrr_files, path_to_nxs_dir,
        roi_x0, roi_y0, roi_size_x, roi_size_y,
        wavelength, direct_value,
        is_bckg_up, is_bckg_down,
        is_bckg_left, is_bckg_right, is_print_info
        )

    fig_twotheta = None
    fig_qz = None
    is_first_plot = True

    if is_plot_twotheta:
        fig_twotheta = plot_xrr_twotheta(theta, bckg_refl, err_refl, refl,
                                         list_xrr_files, is_first_plot, time_str)
        is_first_plot = False

    if is_plot_qz:
        fig_qz = plot_xrr_qz(qz, bckg_refl, err_refl, refl,
                             list_xrr_files, is_first_plot, time_str)
        is_first_plot = False

    if is_save:
        save_xrr_solid_scan(
            theta, qz,
            bckg_refl_up, bckg_refl_down, bckg_refl_left, bckg_refl_right,
            bckg_refl, err_refl, refl, list_xrr_files,
            fig_twotheta, fig_qz,
            path_to_nxs_dir, path_to_save_dir, is_print_info
            )

    return theta, qz, bckg_refl_up, bckg_refl_down, \
           bckg_refl_left, bckg_refl_right, bckg_refl, err_refl, refl


def extract_direct_xrr_solid(
    direct_scan_name, roi_x0, roi_size_x, roi_size_y,
    is_bckg_up, is_bckg_down, is_bckg_left, is_bckg_right,
    path_to_nxs_dir, is_print_info):
    """
    Extract the value of the direct beam.

    Parameters
    ----------
    direct_scan_name : str
        Nexus filename of the direct scan.
    roi_x0 : int
        x0 of the full-scan ROI.
    roi_size_x : int
        Size x of the full-scan ROI.
    roi_size_y : int
        Size y of the full-scan ROI.
        It has to be an odd number.
    is_bckg_up : bool
        Take into account the upper ROI for background subtraction.
    is_bckg_down : bool
        Take into account the lower ROI for background subtraction.
    is_bckg_left : bool
        Take into account the left ROI for background subtraction.
    is_bckg_right : bool
        Take into account the right ROI for background subtraction.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    is_print_info : bool
        Verbose mode.

    Returns
    -------
    direct_value : float
        Direct after background subtraction and normalization.

    Raises
    ------
    FileNotFoundError
        If a file or a folder is missing.
    SystemExit('Could not open Nexus file.'))
        If the Nexus file cannot be accessed.
    SystemExit('Required sensor not found.')
        If a required sensor is not found in the sensor list.

    """
    ###########################################
    # Extract voltage of the ion chamber
    ###########################################

    # Extract sensors
    nxs_path = path_to_nxs_dir+direct_scan_name
    if is_print_info:
        print(_BLUE+" - Open Nexus Data File :"+ _RESET)
        print('\t%s'%nxs_path)
    try:
        nexus = PN.PyNexusFile(nxs_path, fast=True)
    except:
        print(_RED+'\t Could not open Nexus file.'+_RESET)
        raise Exception('Could not open Nexus file.')

    stamps_0d, data_0d = nexus.extractData("0D")
    sensor_list = [stamps_0d[i][0].lower() if stamps_0d[i][1] is None\
                   else stamps_0d[i][1].lower() for i in range(len(stamps_0d))]

    if 'ionchamber' in sensor_list:
        ionchamber_arg = sensor_list.index('ionchamber')
    else:
        print(_RED+'\t Sensor %s is not in the sensor list'%('ionchamber')+_RESET)
        nexus.close()
        raise Exception('Required sensor not found.')

    if 'commandfemtoionchamber' in sensor_list:
        gain_arg = sensor_list.index('commandfemtoionchamber')
    else:
        print(_RED+'\t Sensor %s is not in the sensor list'%('commandfemtoionchamber')+_RESET)
        nexus.close()
        raise Exception('Required sensor not found.')

    integration_time_arg = sensor_list.index('integration_time')
    integration_time = np.mean(data_0d[integration_time_arg])

    nbpts = int(nexus.get_nbpts())
    if is_print_info:
        print("\t. Number of data points: %s"%nbpts)

    nexus.close()

    # Get the voltage of the ionization chamber
    ionchamber = data_0d[ionchamber_arg]
    gain = data_0d[gain_arg].astype(float)
    volt_direct_list = ionchamber*10**(-gain)
    if sensor_list[0] == 'zs':
        # If the direct is a height scan, take only the five first points
        volt_direct = np.mean(volt_direct_list[0:5])
    else:
        volt_direct = np.mean(volt_direct_list)

    ###########################################
    # Extract direct scan
    ###########################################

    if sensor_list[0] == 'zs':
        # If the direct is a height scan, take only the five first points
        images_direct, _, _, _, _, _, _, _, _ = area_detector.extract_area_detector_scan(
                direct_scan_name, path_to_nxs_dir, selected_sensors=None,
                is_print_stamps=False, is_print_info=False)
        image_direct = np.mean(images_direct[0:5], axis = 0)
    else:
        # Extract the sum of all images in the time scan
        _, image_direct, _, _, _, _, _, _, _ = area_detector.extract_area_detector_scan(
                        direct_scan_name, path_to_nxs_dir,selected_sensors=None,
                        is_print_stamps=False, is_print_info=False)
        image_direct = image_direct/nbpts

    # Contrary to XRR on liquid, here image_direct is already normalized by the number of points

    # Replace the intensity of the dead zones with a value of 0
    image_direct = np.where(image_direct<0., 0., image_direct)

    # Integrate along the horizontal axis and find the maximum
    integrated_x_direct = image_direct.sum(axis=1)
    pos_y_direct = np.argmax(integrated_x_direct)

    # Height of the summation ROI
    # Always use an odd height for the summation_roi!
    dy = roi_size_y
    # Width of the summation ROI
    dx = roi_size_x

    summation_roi_direct = image_direct[pos_y_direct-dy//2 : pos_y_direct+dy//2+1,
                                        roi_x0 : roi_x0+dx]

    bckg_roi_up_direct = image_direct[pos_y_direct-dy-dy//2 : pos_y_direct-dy+dy//2+1,
                                      roi_x0 : roi_x0+dx]

    bckg_roi_down_direct = image_direct[pos_y_direct+dy-dy//2 : pos_y_direct+dy+dy//2+1,
                                        roi_x0 : roi_x0+dx]

    bckg_roi_left_direct = image_direct[pos_y_direct-dy//2 : pos_y_direct+dy//2+1,
                                        roi_x0-dx : roi_x0]

    bckg_roi_right_direct = image_direct[pos_y_direct-dy//2 : pos_y_direct+dy//2+1,
                                         roi_x0+dx : roi_x0+2*dx]


    # Sum the ROI and normalize with the integration time
    # It is already normalized by the number of images
    # int stands for intensity
    int_raw_direct = summation_roi_direct.sum()/integration_time

    int_bckg_up_direct = bckg_roi_up_direct.sum()/integration_time
    int_bckg_down_direct = bckg_roi_down_direct.sum()/integration_time
    int_bckg_left_direct = bckg_roi_left_direct.sum()/integration_time
    int_bckg_right_direct = bckg_roi_right_direct.sum()/integration_time

    if any([is_bckg_up, is_bckg_down, is_bckg_left, is_bckg_right]):
        # Take the average of the chosen backgrounds ROIs
        int_bckg_direct = (is_bckg_up*int_bckg_up_direct+is_bckg_down*int_bckg_down_direct\
                         +is_bckg_left*int_bckg_left_direct+is_bckg_right*int_bckg_right_direct)\
                         /(is_bckg_up*1.+is_bckg_down*1.+is_bckg_left*1.+is_bckg_right*1.)
    else:
        int_bckg_direct = 0.

    int_sub_direct = int_raw_direct - int_bckg_direct

    direct_value = int_sub_direct/volt_direct

    if is_print_info:
        print('Voltage corrected by the gain: V=%g'%volt_direct)
        print('Raw direct: int_raw=%g'%int_raw_direct)
        print('Direct after subtraction of bckg: int_sub=%g'%int_sub_direct)
        print('Direct after normalization: int_direct=(int_raw-int_sub)/V=%g'%direct_value)

    return direct_value



def extract_xrr_solid_scan(
    list_xrr_files, path_to_nxs_dir,
    roi_x0, roi_y0, roi_size_x, roi_size_y,
    wavelength, direct_value,
    is_bckg_up, is_bckg_down, is_bckg_left,
    is_bckg_right, is_print_info):
    """
    Extract the XRR from a series of scans.

    Parameters
    ----------
    list_xrr_files : list of str
        List of XRR file names without the '.nxs', e.g. ['SIRIUS_2021_06_18_0568'].
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    roi_x0 : int
        x0 of the full-scan ROI.
    roi_y0 : int
        y0 of the full-scan ROI.
    roi_size_x : int
        Size x of the full-scan ROI.
    roi_size_y : int
        Size y of the full-scan ROI.
        It has to be an odd number.
    wavelength : float
        Wavelength in nm.
    direct_value : float
        Direct after background subtraction and normalization.
    is_bckg_up : bool
        Take into account the upper ROI for background subtraction.
    is_bckg_down : bool
        Take into account the lower ROI for background subtraction.
    is_bckg_left : bool
        Take into account the left ROI for background subtraction.
    is_bckg_right : bool
        Take into account the right ROI for background subtraction.
    is_print_info : bool
        Verbose mode.

    Returns
    -------
    theta : array
        List of theta (rad).
    qz : array
        List of qz (nm-1).
    bckg_refl_up : array
        List of integrated intensities of the bckg up.
    bckg_refl_down : array
        List of integrated intensities of the bckg down.
    bckg_refl_left : array
        List of integrated intensities of the bckg left.
    bckg_refl_right : array
        List of integrated intensities of the bckg right.
    bckg_refl : array
        List of averaged chosen backgrounds.
    err_refl : array
        List of error bars of the XRR.
    refl : array
        List of values of the normalized XRR.
    time_str : str
        Starting/ending dates of the whole XRR scan.

    Raises
    ------
    FileNotFoundError
        If a file or a folder is missing.
    SystemExit('Could not open Nexus file.'))
        If the Nexus file cannot be accessed.
    SystemExit('Required sensor not found.')
        If a required sensor is not found in the sensor list.

    """
    # Summation ROI
    # Height of the summation ROI
    dy = roi_size_y
    if dy%2 == 0:
        print("\033[1mroi_size_y has to be an odd number!\033[0m")
    # Width of the summation ROI
    dx = roi_size_x

    ######################################
    # Extract the reflected beams
    ######################################

    int_raw_refl = np.array([])
    int_bckg_up = np.array([])
    int_bckg_down = np.array([])
    int_bckg_left = np.array([])
    int_bckg_right = np.array([])
    gamma = np.array([])
    volt_refl = np.array([])

    for file in list_xrr_files:

        nxs_name = file+'.nxs'

        ######################################
        # Extract info from nexus file
        ######################################

        nxs_path = path_to_nxs_dir+nxs_name
        if is_print_info:
            print(_BLUE+" - Open Nexus Data File :"+ _RESET)
            print('\t%s'%nxs_path)
        try:
            nexus = PN.PyNexusFile(nxs_path, fast=True)
        except:
            print(_RED+'\t Could not open Nexus file.'+_RESET)
            raise Exception('Could not open Nexus file.')

        stamps_0d, data_0d = nexus.extractData("0D")

        sensor_list = [stamps_0d[i][0].lower() if stamps_0d[i][1] is None\
                       else stamps_0d[i][1].lower() for i in range(len(stamps_0d))]

        if 'gamma' in sensor_list:
            gamma_arg = sensor_list.index('gamma')
        else:
            print(_RED+'\t Sensor %s is not in the sensor list'%('gamma')+_RESET)
            raise Exception('Required sensor not found.')

        if 'ionchamber' in sensor_list:
            ionchamber_arg = sensor_list.index('ionchamber')
        else:
            print(_RED+'\t Sensor %s is not in the sensor list'%('ionchamber')+_RESET)
            raise Exception('Required sensor not found.')

        if 'commandfemtoionchamber' in sensor_list:
            gain_arg = sensor_list.index('commandfemtoionchamber')
        else:
            print(_RED+'\t Sensor %s is not in the sensor list'%('commandfemtoionchamber')+_RESET)
            raise Exception('Required sensor not found.')

        if 'integration_time' in sensor_list:
            integration_time_arg = sensor_list.index('integration_time')
        else:
            print(_RED+'\t Sensor %s is not in the sensor list'%('integration_time')+_RESET)
            raise Exception('Required sensor not found.')

        gamma = np.append(gamma, data_0d[gamma_arg])
        integration_time = np.mean(data_0d[integration_time_arg])

        # Extract time
        if file == list_xrr_files[0]:
            time_init = None
            if 'sensorstimestamps' in sensor_list:
                column_epoch = sensor_list.index('sensorstimestamps')
                # Find positions of non Nan values
                args = ~np.isnan(data_0d[column_epoch])
                epoch_time =  data_0d[column_epoch][args]
                time_init = epoch_time[0]
        if file == list_xrr_files[-1]:
            time_end = None
            if 'sensorstimestamps' in sensor_list:
                column_epoch = sensor_list.index('sensorstimestamps')
                # Find positions of non Nan values
                args = ~np.isnan(data_0d[column_epoch])
                epoch_time =  data_0d[column_epoch][args]
                time_end = epoch_time[-1]

        ####################################################
        # Extract the voltage of the ionization chamber
        ####################################################
        ionchamber = data_0d[ionchamber_arg]
        gain = data_0d[gain_arg].astype(float)
        volt_refl = np.append(volt_refl, ionchamber*10**(-gain))

        ####################################################
        # Extract individual images (one per theta)
        ####################################################

        # Extract individual images
        images, _, _, _, _, _, _, _, _ = area_detector.extract_area_detector_scan(
                        nxs_name, path_to_nxs_dir, selected_sensors=None,
                        is_print_stamps=False, is_print_info=is_print_info)

        for image in images:

            # Replace the intensity of the dead zones with a value of 0
            image = np.where(image<0., 0., image)

            # Apply the full-scan ROI
            image_roi = image[roi_y0:roi_y0+roi_size_y,
                              roi_x0:roi_x0+roi_size_x]

            # Up & down background ROIs
            bckg_roi_up = image[roi_y0-dy : roi_y0,
                                roi_x0 : roi_x0+dx]
            bckg_roi_down = image[roi_y0+dy : roi_y0+2*roi_size_y,
                                  roi_x0 : roi_x0+dx]

            # Left & right background ROIs
            bckg_roi_left = image[roi_y0 : roi_y0+dy,
                                  roi_x0-dx : roi_x0]
            bckg_roi_right = image[roi_y0 : roi_y0+dy,
                                  roi_x0+dx : roi_x0+2*dx]

            int_raw_refl = np.append(int_raw_refl, image_roi.sum()/integration_time)
            int_bckg_up = np.append(int_bckg_up, bckg_roi_up.sum()/integration_time)
            int_bckg_down = np.append(int_bckg_down, bckg_roi_down.sum()/integration_time)
            int_bckg_left = np.append(int_bckg_left, bckg_roi_left.sum()/integration_time)
            int_bckg_right = np.append(int_bckg_right, bckg_roi_right.sum()/integration_time)

    if any([is_bckg_up, is_bckg_down, is_bckg_left, is_bckg_right]):
        # Take the average of the chosen backgrounds ROIs
        if is_print_info:
            print('Background taken: '+is_bckg_up*'up '+is_bckg_down*'down '\
                  +is_bckg_left*'left '+is_bckg_right*'right ')

        int_bckg_refl = (is_bckg_up*int_bckg_up+is_bckg_down*int_bckg_down\
                       +is_bckg_left*int_bckg_left+is_bckg_right*int_bckg_right)\
                      /(is_bckg_up*1.+is_bckg_down*1.+is_bckg_left*1.+is_bckg_right*1.)

        err_int_sub_refl = np.sqrt(int_raw_refl)+\
                         (is_bckg_up*np.sqrt(int_bckg_up)+is_bckg_down*np.sqrt(int_bckg_down)\
                          +is_bckg_left*np.sqrt(int_bckg_left)+is_bckg_right*np.sqrt(int_bckg_right))\
                        /(is_bckg_up*1.+is_bckg_down*1.+is_bckg_left*1.+is_bckg_right*1.)
    else:
        if is_print_info:
            print('No subtraction done.')
        int_bckg_refl = 0.*int_bckg_up
        err_int_sub_refl = np.sqrt(int_raw_refl)

    int_sub_refl = int_raw_refl - int_bckg_refl

    time_str = ''
    if (time_init is not None and time_end is not None):
        time_str = 'Starting time: %s\nEnding time: %s'%(
                   datetime.datetime.fromtimestamp(time_init),
                   datetime.datetime.fromtimestamp(time_end))

    # Normalize by V and the direct to get the reflectivity
    refl = int_sub_refl/volt_refl/direct_value
    err_refl = err_int_sub_refl/volt_refl/direct_value
    bckg_refl_up = int_bckg_up/volt_refl/direct_value
    bckg_refl_down = int_bckg_down/volt_refl/direct_value
    bckg_refl_left = int_bckg_left/volt_refl/direct_value
    bckg_refl_right = int_bckg_right/volt_refl/direct_value
    bckg_refl = int_bckg_refl/volt_refl/direct_value

    theta = gamma/2.*np.pi/180. #Incident angle in rad
    qz = 4*np.pi/wavelength*np.sin(theta) #qz in nm-1

    return theta, qz, bckg_refl_up, bckg_refl_down,\
           bckg_refl_left, bckg_refl_right, bckg_refl, err_refl, refl,\
           time_str

def save_xrr_solid_scan(
    theta, qz,
    bckg_refl_up, bckg_refl_down, bckg_refl_left, bckg_refl_right,
    bckg_refl, err_refl, refl, list_xrr_files,
    fig_twotheta, fig_qz,
    path_to_nxs_dir, path_to_save_dir, is_print_info):
    """
    Save data of a XRR scan on a solid.

    To save the .dat of each individual file,
    we redo the extraction with PyNexus directly within this function.

    XXX_XRR.dat: different parameters relevant for XRR for each point of theta taken.
    XXX.dat: the value of each sensor at each point of each scan in the list of XRR files.
    XXX_XRR_qz.pdf, XXX_XRR_twotheta.pdf: the figures in pdf.

    Parameters
    ----------
    theta : array
        List of theta (rad).
    qz : array
        List of qz (nm-1).
    bckg_refl_up : array
        List of integrated intensities of the bckg up.
    bckg_refl_down : array
        List of integrated intensities of the bckg down.
    bckg_refl_left : array
        List of integrated intensities of the bckg left.
    bckg_refl_right : array
        List of integrated intensities of the bckg right.
    bckg_refl : array
        List of averaged chosen backgrounds.
    err_refl : array
        List of error bars of the XRR.
    refl : array
        List of values of the normalized XRR.
    list_xrr_files : list of str
        List of XRR file names without the '.nxs', e.g. ['SIRIUS_2021_06_18_0568'].
    fig_twotheta : matplotlib figure
        The figure to be saved in pdf.
    fig_qz : matplotlib figure
        The figure to be saved in pdf.
    path_to_nxs_dir : str
        Path to the nexus files directory, e.g. `user/`.
    path_to_save_dir : str
        Path to the directory where the treated files will be saved.
    is_print_info : bool
        Verbose mode.

    Raises
    ------
    SystemExit('Could not open Nexus file.'))
        If the Nexus file cannot be accessed.

    """
    # Create Save Name
    if  len(list_xrr_files)>1:
        save_name = path_to_save_dir + list_xrr_files[0]+'-'+list_xrr_files[-1].split('_')[-1]
    else:
        save_name = path_to_save_dir + list_xrr_files[0]

    # Figure in pdf
    if fig_qz is not None:
        plt.figure(fig_qz)
        plt.savefig(save_name+'_XRR_qz.pdf', bbox_inches='tight')
        plt.close()
    if fig_twotheta is not None:
        plt.figure(fig_twotheta)
        plt.savefig(save_name+'_XRR_twotheta.pdf', bbox_inches='tight')
        plt.close()

    # Save XRR
    twotheta = 2*theta*180./np.pi # in deg
    tobesaved = [twotheta, qz, bckg_refl_up, bckg_refl_down,\
                 bckg_refl_left, bckg_refl_right, bckg_refl, err_refl, refl]
    header = '#twotheta(deg)\t#qz(nm-1)\t'+\
              '#bckg_refl_up\t#bckg_refl_down\t#bckg_refl_left\t#bckg_refl_right\t#bckg_refl\t#err_refl\t#refl'
    np.savetxt(save_name+'_XRR.dat', np.transpose(tobesaved),
               delimiter = '\t', comments='', fmt = '%.7e',
               header = header)

    print('Save in progress...')
    # Save the .dat file of each file
    for file in list_xrr_files:
        try:
            nexus = PN.PyNexusFile(path_to_nxs_dir+file+'.nxs', fast=True)
        except:
            print(_RED+'\t Could not open Nexus file.'+_RESET)
            raise Exception('Could not open Nexus file.')

        # Get stamps and data
        stamps_0d, data_0d = nexus.extractData('0D')
        nexus.close()

        # Save 0D data
        header_0d = [stamp[1] if stamp[1] is not None else stamp[0] for stamp in stamps_0d]
        header_0d = '\t'.join(header_0d)

        np.savetxt(path_to_save_dir+file+'.dat', np.transpose(data_0d), header = header_0d,
                   comments = '', fmt='%s', delimiter = '\t')
        if is_print_info:
            print('\t. Dat file saved in:')
            print('\t%s.dat'%(path_to_save_dir+file))

                
    print('Save done!')
    if is_print_info:
        print('\t. XRR saved in:')
        print('\t%s_XRR.dat'%save_name)
        if fig_qz is not None:
            print('\t. Figure qz saved in:')
            print('\t%s_XRR_qz.pdf'%save_name)
        if fig_twotheta is not None:
            print('\t. Figure 2*theta saved in:')
            print('\t%s_XRR_twotheta.pdf'%save_name)
