"""
Module for action widgets.
"""
from IPython.display import display
import ipywidgets as iwid
from lib.frontend import notebook, form, jlb_io
from lib.frontend.process_widgets import wgixd, wxrr

def display_widgets_action(expt):
    """
    Set and display the widgets for choosing an action.

    Parameters
    ----------
    expt : object
        Object of the class Experiment.

    """
    ##################################
    # Functions on_click
    ##################################

    def _on_button_process_clicked(button):
        """
        Print title and display the process wigets.
        """
        code = 'jlb.display_process()'
        notebook.create_cell(
            code=code, position='below', celltype='code',
            is_print=False, is_execute=True
            )

        if len(expt.list_scans)==1:
            sel_scan = expt.list_scans[0]
            if sel_scan.command:
                code = '### '+sel_scan.name+': '+sel_scan.command
            else:
                code = '### '+sel_scan.name

            notebook.create_cell(code=code, position='below', celltype='markdown')

        notebook.delete_cell()

    def _on_button_calib_thetaz_clicked(button):
        """
        Insert the cell for calibration of thetaz (for GIXD).
        """
        wgixd.display_widgets_calib_thetaz()

    def _on_button_refresh_clicked(button):
        """
        Re-execute the cell to update list of files.
        """
        notebook.refresh_cell()

    def _on_button_calib_xrr_liquid_clicked(button):
        wxrr.display_widgets_calib_xrr_liquid(expt)

    def _on_button_display_form_clicked(button):
        """
        Display the widgets to fill and insert the form.
        """
        form.display_widgets_form()

    def _on_button_insert_from_log_clicked(button):
        expt.set_list_logs()
        jlb_io.display_widgets_insert_from_log(expt)

    def _on_button_export_clicked(button):
        """
        Save the notebook, export it to pdf, and export the logs
        to a human-readable format.
        """
        # Save the notebook
        print(70*'#')
        print('Save the notebook (in progress) ...\n')
        notebook.save_nb()
        print('Finished!\n')

        # Export to pdf
        print(70*'#')
        print('Export the notebook to pdf (in progress) ...\n')
        jlb_io.export_nb_to_pdf(path_to_nb=expt.paths['notebook'],
                            timeout=100.,
                            verbose=True,
                            output_dir='.')
        print('Finished!\n')


        # Update the list of logs and export the logs
        print(70*'#')
        print('Export the logs to a human-readable format (in progress) ...\n')
        expt.set_list_logs()
        jlb_io.export_logs_to_rlogs(path_to_logs_dir=expt.paths['dir_logs'],
                                list_logs=expt.list_logs,
                                verbose=True,
                                output_dir=expt.paths['dir_save']+'readable_logs/')
        print('Finished!\n')

    def _on_button_insert_text_clicked(button):
        """
        Format the text and insert it in a markdown cell.
        """
        jlb_io.display_widgets_insert_text()

    def _on_button_insert_script_clicked(button):
        """
        Insert a script in the notebook with the corresponding scan numbers.
        """
        expt.set_list_scripts()
        jlb_io.display_widgets_insert_script(expt.paths['dir_scripts'], expt.list_scripts)

    def _on_button_insert_image_clicked(button):
        """
        Insert an image in the notebook.
        """
        expt.set_list_images()
        jlb_io.display_widgets_insert_image(expt.paths['dir_images'], expt.list_images)

    def _on_button_save_params_clicked(button):
        jlb_io.display_widgets_save_params_in_json(expt)

    def _on_button_load_params_clicked(button):
        expt.set_list_params_files()
        jlb_io.display_widgets_load_params_from_json(expt)



    ##################################
    # Create the widgets
    ##################################

    # Dictionnary for widgets
    dw = {}

    # Interactive selection of scans to be processed
    dw['interactive_select_scans'] = iwid.interactive(
        expt.set_scans,
        list_nxs=iwid.SelectMultiple(
            options=expt.list_nxs,
            rows=10,
            description='Next scan(s):',
            layout={'width': '400px'}
            ),
        )

    # Open the pannel for processing the selected scans
    button = iwid.Button(description='Process scan(s)',
                         style={'button_color':'lightgreen'})
    button.on_click(_on_button_process_clicked)
    dw['button_process'] = button

    # Calibration of thetaz for GIXD
    button = iwid.Button(description='Calib. theta z')
    button.on_click(_on_button_calib_thetaz_clicked)
    dw['button_calib_thetaz'] = button

    # Re-execute the cell to refresh the list of files
    button = iwid.Button(description='Refresh',
                         style={'button_color':'lightgreen'})
    button.on_click(_on_button_refresh_clicked)
    dw['button_refresh'] = button

    # Calibration of XRR on liquid
    button = iwid.Button(description='Calib. XRR (liquid)')
    button.on_click(_on_button_calib_xrr_liquid_clicked)
    dw['button_calib_xrr_liquid'] = button

    # Display the widget for filling the form
    button = iwid.Button(description='Fill form')
    button.on_click(_on_button_display_form_clicked)
    dw['button_display_form'] = button

    # Insert commands/positions from log
    button = iwid.Button(description='Insert commands/positions',
                         style={'button_color':'lightgreen'},
                         layout=iwid.Layout(width='200px'))
    button.on_click(_on_button_insert_from_log_clicked)
    dw['button_insert_from_log'] = button

    # Export
    button = iwid.Button(description='Export')
    button.on_click(_on_button_export_clicked)
    dw['button_export'] = button

    # Insert a markdown cell with text
    button = iwid.Button(description='Insert text',
                         style={'button_color':'lightgreen'})
    button.on_click(_on_button_insert_text_clicked)
    dw['button_insert_text'] = button

    # Insert a script
    button = iwid.Button(description='Insert script',
                         style={'button_color':'lightgreen'})
    button.on_click(_on_button_insert_script_clicked)
    dw['button_insert_script'] = button

    # Insert an image
    button = iwid.Button(description='Insert image',
                         style={'button_color':'lightgreen'})
    button.on_click(_on_button_insert_image_clicked)
    dw['button_insert_image'] = button

    # Save the notebook state
    button = iwid.Button(description='Save params',
                         layout=iwid.Layout(width='100px'))
    button.on_click(_on_button_save_params_clicked)
    dw['button_save_params'] = button

    # Load the notebook state
    button = iwid.Button(description='Load params',
                         layout=iwid.Layout(width='100px'))
    button.on_click(_on_button_load_params_clicked)
    dw['button_load_params'] = button

    ##################################
    # Organize and display the widgets
    ##################################

    lvl_0 = iwid.HBox([
        dw['button_process'],
        dw['button_refresh'],
        ])

    lvl_1 = dw['interactive_select_scans']

    lvl_2 = iwid.HBox([
        dw['button_display_form'],
        dw['button_calib_thetaz'],
        dw['button_calib_xrr_liquid'],
        dw['button_save_params'],
        dw['button_load_params'],
        dw['button_export'],
        ])

    lvl_3 = iwid.HBox([
        dw['button_insert_text'],
        dw['button_insert_script'],
        #dw['button_insert_image'], # removed in v3.0.2
        dw['button_insert_from_log'],
        ])

    # Display the widgets
    display(iwid.VBox([
        lvl_0,
        lvl_1,
        lvl_2,
        lvl_3,
        ])
            )


