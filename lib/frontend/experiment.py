"""
Module defining the class Experiment.
"""
import json
import os
from lib.frontend import scan as sc

_RED='\x1b[31;01m'
_RESET='\x1b[0m'

class Experiment:
    """
    Class defining an Experiment.

    Attributes
    ----------
    paths : dict
        Dictionary of paths to files and folders.
    params : dict
        Dictionary of parameters.
    default : dict
        Dictionary of default values.
    is_paths_ok : bool
        True if all the paths are correct.
    list_nxs : list of str
        List of nexus filenames in the nxs folder.
    list_logs : list of str
        List of log filenames in the logs folder.
    list_scripts : list of str
        List of script filenames in the scripts folder.
    list_scans : list of objects Scan
        List of scans selected in the interactive widget.
    list_params_files : list of str
        List of json files in the params folder.
    default_log : str
        List of commands from a log file.

    """
    def __init__(self):
        """
        Constructor.
        """
        self.paths = {}
        self.params = {}
        self.default = {}
        self.is_paths_ok = False
        self.list_nxs = []
        self.list_logs = []
        self.list_scripts = []
        self.list_scans = []
        self.list_images = []
        self.list_params_files = []
        self.default_log = ''

    def check_and_set_paths(self, paths):
        """
        Check if the paths exist and set the attribute `paths`.

        Parameters
        ----------
        paths : dict
            Dictionary of paths to files and folders.

        """
        def _check_path(path, message_ok, message_not_ok, is_mandatory):
            """
            Check if `path` exists.

            Parameters
            ----------
            path : str
                Path to file or folder.
            message_ok : str
                Message to print if ok.
            message_not_ok : str
                Message to print if not ok.
            is_mandatory : bool
                True if the file or folder has to exist to continue.

            """
            if os.path.exists(path):
                print(message_ok)
            else:
                print(message_not_ok)
                if is_mandatory:
                    self.is_paths_ok = False

        # Initialize with True
        self.is_paths_ok = True

        path = paths['dir_save']
        _check_path(path=path,
                    message_ok='Data reduction will be saved in the folder:\n\t%s'%path,
                    message_not_ok=_RED+'\nCareful, the following folder does not exist:\n\t%s\n'%path+_RESET,
                    is_mandatory=True)

        path = paths['dir_nxs']
        _check_path(path=path,
                    message_ok='The nexus files should be in the folder:\n\t%s'%path,
                    message_not_ok=_RED+'\nCareful, the following folder does not exist:\n\t%s\n'%path+_RESET,
                    is_mandatory=True)

        path = paths['dir_logs']
        _check_path(path=path,
                    message_ok='The logs should be in the folder:\n\t%s'%path,
                    message_not_ok=_RED+'\nCareful, the following folder does not exist:\n\t%s\n'%path+_RESET,
                    is_mandatory=True)

        path = paths['dir_params']
        _check_path(path=path,
                    message_ok='The params should be in the folder:\n\t%s'%path,
                    message_not_ok=_RED+'\nThe path to the params folder is not correct.\n'+_RESET,
                    is_mandatory=True)

        path = paths['dir_scripts']
        _check_path(path=path,
                    message_ok='The scripts should be in the folder:\n\t%s'%path,
                    message_not_ok=_RED+'\nCareful, the following folder does not exist:\n\t%s\n'%path+_RESET,
                    is_mandatory=False)

        """
        # Removed in v3.0.2
        path = paths['dir_images']
        _check_path(path=path,
                    message_ok='The images should be in the folder:\n\t%s'%path,
                    message_not_ok=_RED+'\nCareful, the following folder does not exist:\n\t%s\n'%path+_RESET,
                    is_mandatory=False)
        """

        path = paths['notebook']
        _check_path(path=path,
                    message_ok='Notebook name:\n\t%s'%path,
                    message_not_ok=_RED+'\nAssign the correct notebook name to the variable `notebook`.\n'+_RESET,
                    is_mandatory=True)

        # If all the mandatory paths are ok, set the attribute `self.paths`
        if self.is_paths_ok:
            self.paths = paths

    def load_params_from_json(self, path_to_params):
        """
        Import a json file of params and set the attribute `params`.

        Parameters
        ----------
        path_to_params : str
            Path to the json file.

        """
        with open(path_to_params, 'r') as read_file:
            self.params = json.load(read_file)

    def save_params_in_json(self, path_to_params):
        """
        Save the attribute `params` in a json file.

        Parameters
        ----------
        path_to_params : str
            Path to the json file.

        """
        with open(path_to_params, 'w') as output_file:
            json.dump(self.params, output_file)

    def set_list_params_files(self):
        """
        Refresh the list of params files and set the attribute `list_params_files`.
        """
        self.list_params_files = [file for file in sorted(os.listdir(self.paths['dir_params'])) if 'json' in file][::-1]
        if self.list_params_files == []:
            print(_RED+'There is no json file in the folder:\n\t%s'%self.paths['dir_params']+_RESET)

    def set_list_nxs(self):
        """
        Refresh the list of nxs files and set the attribute `list_nxs`.
        """
        self.list_nxs = [file for file in sorted(os.listdir(self.paths['dir_nxs'])) if 'nxs' in file][::-1]
        if self.list_nxs == []:
            print(_RED+'There is no nexus file in the folder:\n\t%s'%self.paths['dir_nxs']+_RESET)
            # Put a dull name to avoid exception if there is no file yet
            self.list_nxs = ['SIRIUS_NoFileFound_00_00_00.nxs']

    def set_list_logs(self):
        """
        Refresh the list of log files and set the attribute `list_logs`.
        """
        self.list_logs = [file for file in sorted(os.listdir(self.paths['dir_logs'])) if 'log' in file][::-1]
        if self.list_logs == []:
            print(_RED+'There is no log file in the folder:\n\t%s'%self.paths['dir_logs']+_RESET)

    def set_list_scripts(self):
        """
        Refresh the list of script files and set the attribute `list_scripts`.
        """
        self.list_scripts = [file for file in sorted(os.listdir(self.paths['dir_scripts'])) if '.ipy' in file][::-1]
        if self.list_scripts == []:
            print(_RED+'There is no script in the folder:\n\t%s'%self.paths['dir_scripts']+_RESET)

    def set_list_images(self):
        """
        Refresh the list of files in the images folder and set the attribute `list_images`.
        """
        self.list_images = list(sorted(os.listdir(self.paths['dir_images'])))[::-1]
        if self.list_images == []:
            print(_RED+'There is no files in the folder:\n\t%s'%self.paths['dir_images']+_RESET)

    def set_scans(self, list_nxs):
        """
        Create a list of object Scans.

        Parameters
        ----------
        list_nxs : list of str
            List of nexus filenames in the nxs folder.

        """
        self.list_scans = []

        for nxs_name in list_nxs:

            # Create a scan
            scan = sc.Scan()

            # Set identifiers (path, name ...)
            scan.set_identifiers(path_to_nxs_dir = self.paths['dir_nxs'],
                                 nxs_name = nxs_name)

            # Set the corresponding command from the logs
            scan.set_command(path_to_logs_dir = self.paths['dir_logs'],
                             list_logs = self.list_logs)

            # Print the command found in logs, if any
            if scan.command:
                print('%s: %s'%(scan.nxs_name, scan.command))
            else:
                print('%s: command not found in logs.'%scan.nxs_name)

            self.list_scans.append(scan)

    def get_default_param_value(self, key, alt):
        """
        Return the value associated to the key if it already exists,
        else return the alternative value.

        Parameters
        ----------
        key : str
            Key in the dictionnary `params`.
        alt
            Alternative value to return if the key is not present in `params`.

        Returns
        -------
        value
            Value associated to the key or alternative value if not present in `params`.

        """
        if key in self.params:
            value = self.params[key]
        else:
            value = alt

        return value

    def update_param_from_widget(self, dict_widgets, list_keys):
        """
        Update the default value of the keys in `params` with the value of the
        corresponding widget.

        Parameters
        ----------
        dict_widgets : dict
            Dictionnary with the widgets.
        list_keys : list of str
            List of keys in `params` to update.

        """
        for key in list_keys:
            self.params[key] = dict_widgets[key].value
