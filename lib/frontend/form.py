"""
Module for inserting a form (description of the experiment) in the notebook.
"""
import ipywidgets as iwid
from IPython.display import display
from lib.frontend import notebook

def display_widgets_form():
    """
    Set and display the widgets for filling and inserting a form in
    the notebook.
    """

    ##################################
    # Functions on_click
    ##################################

    def _on_button_insert_form_clicked(button):
        """
        Insert the markdown cells (from the filled form) in the notebook.
        """
        txt = []
        txt.append('$\\LARGE \\textbf{SIRIUS Beamline}:\\textbf{Experiment %s}$'%dw['number'].value)

        ########################################
        # Cut the title in several parts
        # Avoid having a line larger than the page

        # Max number of characters allowed by line
        max_length = 50

        title_split = dw['title'].value.split(' ')
        title_blocks = []

        j=0
        for k in range(0,len(title_split)):
            title_part = ''
            for i in range(j,len(title_split)):
                if len(title_part)<max_length:
                    title_part += title_split[i]+' '
                    j=j+1
                else:
                    break
            title_blocks.append(title_part)

        for title_block in title_blocks:
            if title_block != '':
                txt.append('$\\Large \\color{red}{\\textbf{%s}}$'%(' '.join(title_block.split(' '))))

        ########################################

        txt.append('* %s %s'%(dw['type'].description,dw['type'].value)+'\n'
                    +'* %s %s'%(dw['safety'].description,dw['safety'].value)+'\n'
                    +'* %s %s'%(dw['date'].description,dw['date'].value))

        txt.append('* %s %s'%(dw['proposer'].description,dw['proposer'].value)+'\n'
                    +'* %s %s'%(dw['contact'].description,dw['contact'].value)+'\n'
                    +'* %s %s'%(dw['users'].description,dw['users'].value))

        txt.append('* Machine:'+'\n'
                    +'\t * %s %s'%(dw['current'].description,dw['current'].value)+'\n'
                    +'\t * %s %s'%(dw['mode'].description,dw['mode'].value))

        txt.append('* Optics:'+'\n'
                    +'\t * %s %s'%(dw['dcm'].description,dw['dcm'].value)+'\n'
                    +'\t * %s %s'%(dw['mgm'].description,dw['mgm'].value)+'\n'
                    +'\t * %s %s'%(dw['m1'].description,dw['m1'].value)+'\n'
                    +'\t * %s %s'%(dw['m2'].description,dw['m2'].value)+'\n'
                    +'\t * %s %s'%(dw['m3'].description,dw['m3'].value)+'\n'
                    +'\t * %s %s'%(dw['m4'].description,dw['m4'].value))

        txt.append('* Beam:'+'\n'
                    +'\t * %s %s'%(dw['energy_type'].description,dw['energy_type'].value)+'\n'
                    +'\t * %s %s'%(dw['energy'].description,dw['energy'].value)+'\n'
                    +'\t * %s %s'%(dw['wavelength'].description,dw['wavelength'].value)+'\n'
                    +'\t * %s %s'%(dw['harmonic'].description,dw['harmonic'].value)+'\n'
                    +'\t * %s %s'%(dw['polarisation'].description,dw['polarisation'].value)+'\n'
                    +'\t * %s %s'%(dw['phase'].description,dw['phase'].value)+'\n'
                    +'\t * %s %s'%(dw['horizontal_foc'].description,dw['horizontal_foc'].value)+'\n'
                    +'\t * %s %s'%(dw['vertical_foc'].description,dw['vertical_foc'].value)+'\n'
                    +'\t * %s %s'%(dw['horizontal_size'].description,dw['horizontal_size'].value)+'\n'
                    +'\t * %s %s'%(dw['vertical_size'].description,dw['vertical_size'].value))

        txt.append('* Monitors and XBPM:'+'\n'
                    +'\t * %s %s'%(dw['mon1'].description,dw['mon1'].value)+'\n'
                    +'\t * %s %s'%(dw['mon2'].description,dw['mon2'].value)+'\n'
                    +'\t * %s %s'%(dw['mon3'].description,dw['mon3'].value)+'\n'
                    +'\t * %s %s'%(dw['mon4'].description,dw['mon4'].value)+'\n'
                    +'\t * %s %s'%(dw['detectors'].description,dw['detectors'].value))

        txt.append('* %s %s'%(dw['remarks'].description,dw['remarks'].value))

        # Cells need to be created backwards
        txt.reverse()

        for elem in txt:
            notebook.create_cell(code=elem, position='below', celltype='markdown')

        notebook.create_cell(code='# Experimental setup', position ='below', celltype='markdown')

        # Put back the action widgets
        notebook.create_cell_action_widgets(is_delete_current_cell=True)

    ##################################
    # Create the widgets
    ##################################

    style = {'description_width': 'initial'}

    # Dictionnary for widgets
    dw = {}

    ############## TEXT WIDGETS #################

    dw['title'] = iwid.Text(
        placeholder='Title',
        description='Experiment title:',
        layout=iwid.Layout(width='800px'),
        style=style
        )

    dw['number'] = iwid.Text(
        placeholder='Number',
        description='Experiment number:',
        layout=iwid.Layout(width='300px'),
        style=style
        )

    dw['type'] =iwid.Text(
        placeholder='Proposal, Commissioning, ...',
        description='Type:',
        layout=iwid.Layout(width='300px'),
        style=style
        )

    dw['date'] = iwid.Text(
        placeholder='DD/MM/YYYY - DD/MM/YYYY',
        description='Date:',
        layout=iwid.Layout(width='300px'),
        style=style
        )

    dw['proposer'] = iwid.Text(
        placeholder='Name',
        description='Main proposer:',
        layout=iwid.Layout(width='300px'),
        style=style
        )

    dw['contact'] = iwid.Text(
        placeholder='Name',
        description='Local contact:',
        layout=iwid.Layout(width='300px'),
        style=style
        )

    dw['users'] = iwid.Text(
        placeholder='Name',
        description='Users (on site):',
        layout=iwid.Layout(width='800px'),
        style=style
        )

    dw['current'] = iwid.Text(
        placeholder='450 mA, 500 mA, ...',
        description='Current:',
        layout=iwid.Layout(width='300px'),
        style=style
        )

    dw['mode'] = iwid.Text(
        placeholder='Hybrid, Top-up, ...',
        description='Mode:',
        layout=iwid.Layout(width='300px'),
        style=style
        )

    dw['energy'] = iwid.Text(
        placeholder='Value(s)',
        description='Energy (keV):',
        layout=iwid.Layout(width='300px'),
        style=style
        )

    dw['wavelength'] = iwid.Text(
        placeholder='Value(s)',
        description='Wavelength (nm):',
        layout=iwid.Layout(width='300px'),
        style=style
        )

    dw['harmonic'] = iwid.Text(
        placeholder='Value(s)',
        description='Harmonic:',
        layout=iwid.Layout(width='200px'),
        style=style
        )

    dw['polarisation'] = iwid.Text(
        placeholder='Value(s)',
        value='LH',
        description='Polarisation:',
        layout=iwid.Layout(width='200px'),
        style=style
        )

    dw['phase'] = iwid.Text(
        placeholder='Value(s)',
        value='0',
        description='Phase (deg):',
        layout=iwid.Layout(width='200px'),
        style=style
        )

    dw['horizontal_size'] = iwid.Text(
        placeholder='Value(s)',
        description='Horizontal beamsize (mm):',
        layout=iwid.Layout(width='300px'),
        style=style
        )

    dw['vertical_size'] = iwid.Text(
        placeholder='Value(s)',
        description='Vertical beamsize (mm):',
        layout=iwid.Layout(width='300px'),
        style=style
        )

    dw['mon1'] = iwid.Text(
        placeholder='Empty or type of mon',
        description='mon1:',
        layout=iwid.Layout(width='200px'),
        style=style
        )

    dw['mon2'] = iwid.Text(
        placeholder='Empty or type of mon',
        description='mon2:',
        layout=iwid.Layout(width='200px'),
        style=style
        )

    dw['mon3'] = iwid.Text(
        placeholder='Empty or type of mon',
        description='mon3:',
        layout=iwid.Layout(width='200px'),
        style=style
        )

    dw['mon4'] = iwid.Text(
        placeholder='Empty or type of mon',
        description='mon4:',
        layout=iwid.Layout(width='200px'),
        style=style
        )

    ############## TEXT AREA WIDGETS #################

    dw['detectors'] = iwid.Textarea(
        placeholder='Type of detectors',
        description='Detectors:',
        layout=iwid.Layout(width='800px'),
        style=style
        )

    dw['remarks'] = iwid.Textarea(
        placeholder='Remarks',
        description='Remarks:',
        layout=iwid.Layout(width='800px'),
        style=style
        )

    ############## DROPDOWN WIDGETS #################

    dw['safety'] = iwid.Dropdown(
        options=['Green', 'Yellow', 'Red'],
        value='Yellow',
        description='Safety:',
        layout=iwid.Layout(width='150px'),
        style=style
        )

    dw['dcm']  = iwid.Dropdown(
        options=['Si111', 'InSb', 'Not Used'],
        value='Si111',
        description='DCM:',
        layout=iwid.Layout(width='150px'),
        style=style
        )

    dw['mgm']  = iwid.Dropdown(
        options=['In use', 'Not used'],
        value='Not used',
        description='MGM:',
        layout=iwid.Layout(width='150px'),
        style=style
        )

    dw['energy_type'] = iwid.Dropdown(
        options=['Fixed', 'Variable'],
        value='Fixed',
        description='Fixed/Variable energy:',
        layout=iwid.Layout(width='300px'),
        style=style
        )

    dw['m1'] = iwid.Dropdown(
          options=['M1-A Pt Track', 'M1-A B4C Track', 'M1-B (B4C)', 'No M1'],
          value='M1-A Pt Track',
          description='M1:',
          layout=iwid.Layout(width='200px'),
          style=style
          )

    dw['m2'] = iwid.Dropdown(
          options=['M2 Pt Track', 'M2 B4C Track', 'No M2'],
          value='M2 Pt Track',
          description='M2:',
          layout=iwid.Layout(width='200px'),
          style=style
          )

    dw['m3'] = iwid.Dropdown(
          options=['M3 Pt Track', 'M3 B4C Track', 'No M3'],
          value='No M3',
          description='M3:',
          layout=iwid.Layout(width='200px'),
          style=style
          )

    dw['m4'] = iwid.Dropdown(
          options=['M4 Pt Track', 'M4 Si Track', 'No M4'],
          value='M4 Pt Track',
          description='M4:',
          layout=iwid.Layout(width='200px'),
          style=style
          )

    ############## CHECKBOX WIDGETS #################

    dw['horizontal_foc'] = iwid.Checkbox(
        value=True,
        description='Horizontal focalisation:',
        layout=iwid.Layout(width='800px'),
        style=style
        )

    dw['vertical_foc'] = iwid.Checkbox(
        value=True,
        description='Vertical focalisation:',
        layout=iwid.Layout(width='800px'),
        style=style
        )

    ############## BUTTON WIDGETS #################

    # Insert the form
    button = iwid.Button(description='Insert form')
    button.on_click(_on_button_insert_form_clicked)
    dw['button_insert_form'] = button


    ##################################
    # Organize and display the widgets
    ##################################

    lvl_0 = iwid.VBox([
        dw['title'],
        iwid.HBox([dw['date'], dw['number']]),
        iwid.HBox([dw['type'], dw['safety']]),
        ])

    lvl_1 = iwid.VBox([
        iwid.HBox([dw['proposer'], dw['contact']]),
        dw['users'],
        ])

    lvl_2 = iwid.VBox([
        iwid.HBox([dw['current'], dw['mode']]),
        ])

    lvl_3 = iwid.VBox([
        iwid.HBox([dw['dcm'], dw['mgm']]),
        iwid.HBox([dw['m1'], dw['m2'], dw['m3'], dw['m4']]),
        ])

    lvl_4 = iwid.VBox([
        iwid.HBox([dw['energy_type'], dw['energy'], dw['wavelength']]),
        iwid.HBox([dw['harmonic'], dw['polarisation'], dw['phase']]),
        iwid.HBox([dw['horizontal_foc'], dw['vertical_foc']]),
        iwid.HBox([dw['horizontal_size'], dw['vertical_size']]),
        ])

    lvl_5 = iwid.VBox([
        iwid.HBox([dw['mon1'], dw['mon2'], dw['mon3'], dw['mon4']]),
        dw['detectors'],
        ])

    lvl_6 = iwid.VBox([
        dw['remarks'],
        ])

    # Display the widgets
    display(lvl_0)
    print('-'*100)
    display(lvl_1)
    print('-'*100)
    print('\033[1m'+'Machine:')
    display(lvl_2)
    print('\033[1m'+'Optics:')
    display(lvl_3)
    print('\033[1m'+'Beam:')
    display(lvl_4)
    print('\033[1m'+'Monitors and XBPM:')
    display(lvl_5)
    print('\033[1m'+'Remarks:')
    display(lvl_6)

    display(dw['button_insert_form'])

