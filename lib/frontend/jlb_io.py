"""
Module dealing with the various imports and exports (pdf, logs, state ...)
and insertions in the notebook.
"""
import os
import time
import subprocess
import math
from datetime import datetime
import numpy as np
import ipywidgets as iwid
from IPython.display import display
from lib.frontend import notebook

_RED='\x1b[31;01m'
_RESET='\x1b[0m'

################################################################
################################################################
##################### INSERT TEXT ##############################
################################################################
################################################################

def display_widgets_insert_text():
    """
    Set and display the widgets for inserting text.
    """

    ##################################
    # Functions on_click
    ##################################

    def _on_button_insert_text_clicked(button):
        """
        Format the text and insert it in a markdown cell.
        """
        # Convert the \n to double space \n (specificity of markdown)
        text_to_print = dw['content'].value.replace('\n', '  \n')

        # Add the proper characters depending on the text level
        if dw['level'].value == 'Title':
            text_to_print = '# '+text_to_print
        if dw['level'].value == 'SubTitle':
            text_to_print = '## '+text_to_print
        if dw['level'].value == 'SubSubTitle':
            text_to_print = '### '+text_to_print

        if 'Remark' in dw['level'].value:

            # Max number of characters allowed by line
            max_length = 50

            text_split = text_to_print.split(' ')
            text_blocks = []
            text_to_print = []

            j=0
            for k in range(0,len(text_split)):
                text_part = ''
                for i in range(j,len(text_split)):
                    if len(text_part)<max_length:
                        text_part += text_split[i]+' '
                        j=j+1
                    else:
                        break
                text_blocks.append(text_part)

            # Remove the \n
            temp = []
            for elem in text_blocks:
                temp.append(elem.split('\n'))
            text_blocks = [item for sublist in temp for item in sublist]

            for text_block in text_blocks:
                if text_block != '':
                    if dw['level'].value == 'Big Remark':
                        text_to_print.append('$\\Large \\color{red}{\\textbf{%s}}$'%(' '.join(text_block.split(' '))))
                    if dw['level'].value == 'Remark':
                        text_to_print.append('$\\large \\color{red}{\\textbf{%s}}$'%(' '.join(text_block.split(' '))))

            # Cells need to be created backwards
            text_to_print.reverse()

            for elem in text_to_print:
                notebook.create_cell(code=elem, position='below', celltype='markdown')

        else:
            # Create a markdown cell
            notebook.create_cell(code=text_to_print, position ='at_bottom', celltype='markdown')

        # Put back the action widgets
        notebook.create_cell_action_widgets(is_delete_current_cell=True)

    ##################################
    # Create the widgets
    ##################################

    style = {'description_width': 'initial'}

    # Dictionnary for widgets
    dw = {}

    # Title, subtitle ...
    dw['level'] = iwid.Dropdown(
                options=['Title', 'SubTitle', 'SubSubTitle', 'Simple text', 'Big Remark', 'Remark'],
                value='Simple text',
                description='Format:',
                layout=iwid.Layout(width='200px'),
                style=style
                )

    # Text content
    dw['content'] = iwid.Textarea(
                placeholder='Type here...',
                description='Text: ',
                layout=iwid.Layout(width='800px', height='80px'),
                style=style
                )

    # Insert the text
    button = iwid.Button(description='Insert', style={'button_color':'lightgreen'})
    button.on_click(_on_button_insert_text_clicked)
    dw['button_insert_text'] = button

    ##################################
    # Organize and display the widgets
    ##################################

    lvl_0 = iwid.VBox([
        dw['content'],
        iwid.HBox([dw['level'], dw['button_insert_text']]),
        ])

    display(lvl_0)






################################################################
################################################################
##################### INSERT SCRIPT ############################
################################################################
################################################################

def display_widgets_insert_script(path_to_scripts_dir, list_scripts):
    """
    Set and display the widgets for inserting a script.

    Parameters
    ----------
    path_to_scripts_dir : str
        Path to the directory with the scripts.
    list_scripts : list of str
        List of script filenames in the scripts folder.

    """
    if len(list_scripts)>0:

        ##################################
        # Functions on_click
        ##################################

        def _on_button_insert_script_clicked(button):
            """
            Insert the script in the notebook with the scan numbers.
            """
            with open(path_to_scripts_dir+dw['script_name'].value) as text_file:

                # Import original script
                script_init = text_file.read()

                # Split the different lines
                script_split = script_init.split('\n')

                # Replace the empty strings with \n
                script_split = ['\n'  if elem=='' else elem for elem in script_split]

                # Check if the widget contains a value for the first scan
                # If yes, number the scans
                if np.char.isnumeric(dw['index_first'].value):

                    # List of names which triggers an increment of the scan number
                    list_trig = ['cont_regh', 'ascan', 'dscan', 'tscan']

                    # Index of the first scan
                    count = int(dw['index_first'].value)

                    # Token to detect the loops
                    is_loop = False

                    for i in range(len(script_split)):

                        if is_loop:

                            # Fist run on the loop to collect info
                            loop_length = 0
                            nb_scan_in_loop = 0
                            is_first_run = True

                            for k in range(i,len(script_split)):

                                if is_first_run:

                                    if any(match in script_split[k] for match in ['    ','\t','\n']):

                                        # Do not count commented lines
                                        if '#' not in script_split[k].split('%')[0]:
                                            loop_length +=1

                                        # Count a scan in the loop for each element of the trig list
                                        if any(elem in script_split[k] for elem in list_trig):
                                            # Do not count commented lines
                                            if '#' not in script_split[k].split('%')[0]:
                                                nb_scan_in_loop+=1

                                    else:
                                        is_first_run = False

                            # Attribute the scan numbers
                            line_length = len(script_split[k])
                            for j in range(repet_nb-1):
                                for k in range(i,i+loop_length):
                                    if any(elem in script_split[k] for elem in list_trig):
                                        if '#' not in script_split[k].split('%')[0]:
                                            script_split[k] = script_split[k]+' #'+str(count)
                                            line_length += len(' #'+str(count))
                                            if line_length>60:
                                                line_length = 0
                                                script_split[k] = script_split[k]+'\n'
                                            count+=1

                            is_loop = False

                        # Detect a loop
                        if 'in range' in script_split[i]:
                            repet_nb = int(''.join([str(s) for s in script_split[i] if s.isdigit()]))
                            is_loop = True

                        # Regular scan not in a loop
                        if any(elem in script_split[i] for elem in list_trig):
                            if '#' not in script_split[i].split('%')[0]:
                                script_split[i] = script_split[i]+' #'+str(count)
                                count+=1

                # Put back the empty strings by replacing \n with ''
                script_split = [''  if elem=='\n' else elem for elem in script_split]

                # Re-create the script with added scan numbers
                script_modif ='\n'.join(script_split)

            # Print the script
            code = '```python\n'+script_modif+'```'

            # Create the markdown cells
            notebook.create_cell(code='### '+ dw['script_name'].value, position ='above', celltype='markdown')
            notebook.create_cell(code=code, position ='above', celltype='markdown')

            # Put back the action widgets
            notebook.create_cell_action_widgets(is_delete_current_cell=True)


        ##################################
        # Create the widgets
        ##################################

        style = {'description_width': 'initial'}

        # Dictionnary for widgets
        dw = {}

        # Select script
        dw['script_name'] = iwid.Dropdown(
                    options=list_scripts,
                    value=list_scripts[-1],
                    layout=iwid.Layout(width='300px'),
                    style=style)

        # Index first scan
        dw['index_first'] = iwid.Text(
                    description='Index first scan',
                    value='',
                    layout=iwid.Layout(width='200px'),
                    style=style)

        # Insert the script
        button = iwid.Button(description='Insert', style={'button_color':'lightgreen'})
        button.on_click(_on_button_insert_script_clicked)
        dw['button_insert_script'] = button

        ##################################
        # Organize and display the widgets
        ##################################

        lvl_0 = iwid.HBox([dw['script_name'], dw['index_first'], dw['button_insert_script']])

        display(lvl_0)





################################################################
################################################################
################# INSERT COMMANDS/POSITIONS ####################
##################### FROM A LOG FILE ##########################
################################################################
################################################################

def display_widgets_insert_from_log(expt):
    """
    Set and display the widgets for inserting commands or positions
    from a log file.

    Parameters
    ----------
    expt : object
        Object of the class Experiment.

    """
    if len(expt.list_logs)>0:

        ##################################
        # Preparation
        ##################################

        # If no log was selected before,
        # take the most recent one as default
        if 'log' not in expt.default:
            expt.default['log'] = expt.list_logs[-1]

        ##################################
        # Functions on_click
        ##################################

        def _on_button_positions_clicked(button):
            """
            Prepare the list of positions and display the widgets for selection
            of positions to be inserted in the notebook.
            """
            ##################################
            # Preparation
            ##################################

            # Update the default log with the selected one
            expt.default['log'] = dw['log'].value

            # Extract positions from the log
            list_all_positions = extract_positions_from_log(
                expt.paths['dir_logs']+expt.default['log']
                )

            ##################################
            # Functions on_click
            ##################################

            def _on_button_insert_selected_positions_clicked(button):
                """
                Insert the positions in the notebook.
                """
                for position in dw['positions'].value:
                    # Extract the date of the wm (used as an identifier to be found in the log)
                    date_wm = position.split('; ')[0]

                    # Find the right lines in the log
                    list_sensors = []
                    is_date_found = False
                    count = 0
                    with open(expt.paths['dir_logs']+expt.default['log']) as file:
                        for line in file:
                            if date_wm in line:
                                is_date_found = True
                            if (is_date_found and '-------' in line):
                                count += 1
                            if (count == 2 and '-------' not in line):
                                # append name, value, unit
                                list_sensors.append(line.split()[2:5])

                                # if unit is not present
                                if '[' in line.split()[4]:
                                    list_sensors[-1][-1] = ' '

                                # if unit is 'No unit'
                                if 'No' in line.split()[4]:
                                    list_sensors[-1][-1] = ' '

                    file.close()

                    # Display as a markdown table
                    max_elem_per_line = 6
                    nb_elem = len(list_sensors)
                    if nb_elem >= max_elem_per_line:
                        nb_line = math.ceil(nb_elem/max_elem_per_line)
                        nb_per_line = math.ceil(nb_elem/nb_line)
                    else:
                        nb_per_line = max_elem_per_line

                    list_sensors_str = []
                    for i in range(0,nb_elem,nb_per_line):
                        list_sensors_cut = list_sensors[i:i+nb_per_line]

                        tbw_str = ''
                        for sensor in list_sensors_cut:
                            tbw_str += '|'+str(sensor[0])
                        tbw_str += '|'+' \n'

                        for sensor in list_sensors_cut:
                            tbw_str += '|'+':-:'
                        tbw_str += '|'+' \n'

                        for sensor in list_sensors_cut:
                            tbw_str += '|'+str(sensor[1])
                        tbw_str += '|'+' \n'

                        for sensor in list_sensors_cut:
                            tbw_str += '|'+str(sensor[2])
                        tbw_str += '|'

                        list_sensors_str.append(tbw_str)

                    for sensor_str in list_sensors_str[::-1]:
                        # Create markdown cells with the tables
                        notebook.create_cell(code=sensor_str, position ='below', celltype='markdown')

                    # Put title
                    notebook.create_cell(code='### '+position.split('; ')[1],
                                         position ='below', celltype='markdown')

                # Put back the action widgets
                notebook.create_cell_action_widgets(is_delete_current_cell=True)


            ##################################
            # Create the widgets
            ##################################

            if list_all_positions == []:
                print(_RED+'No positions in the log file: '+expt.default['log']+_RESET)
                print(_RED+'Choose another log file.'+_RESET)

            else:

                # Select the commands
                dw['positions'] = iwid.SelectMultiple(
                    options=list_all_positions,
                    rows=30,
                    layout=iwid.Layout(width='800px'),
                    value=[str(list_all_positions[-1])],
                    style=style)

                # Insert the selected commands
                button = iwid.Button(description='Insert', style={'button_color':'lightgreen'})
                button.on_click(_on_button_insert_selected_positions_clicked)
                dw['button_insert_selected_positions'] = button

                ##################################
                # Organize and display the widgets
                ##################################
                display(iwid.VBox([
                    dw['positions'],
                    dw['button_insert_selected_positions']
                    ])
                )


        def _on_button_commands_clicked(button):
            """
            Prepare the list of commands and display the widgets for selection
            of commands to be inserted in the notebook.
            """
            ##################################
            # Preparation
            ##################################

            # Update the default log with the selected one
            expt.default['log'] = dw['log'].value

            # Extract commands from the log
            arr_all_commands = extract_commands_from_log(
                expt.paths['dir_logs']+expt.default['log']
                )

            # Convert numpy.ndarray into a simple list
            # (ipywidget does not like numpy array as options)
            list_all_commands = [str(elem) for elem in arr_all_commands]

            ##################################
            # Functions on_click
            ##################################

            def _on_button_insert_selected_commands_clicked(button):
                """
                Insert the commands in the notebook.
                """
                # Reformat the commands to avoid long lines cut in the pdf
                list_sel_commands = []

                for command_full in dw['commands'].value:

                    command_split = command_full.split(' ')
                    str_to_print = ''
                    current_line = ''

                    for elem in command_split:
                        current_line += elem
                        nb_char_line = len(current_line)
                        if nb_char_line > 80:
                            str_to_print += current_line + '\n'
                            current_line = ''
                        else:
                            # Necessary, to avoid adding a space after \n which are already present in the command
                            if current_line[-1:]!='\n':
                                current_line += ' '

                    str_to_print += current_line
                    list_sel_commands.append(str_to_print)

                code = '```python\n'+''.join(list_sel_commands)+'```'

                # Create the markdown cells
                notebook.create_cell(code=code, position ='above', celltype='markdown')

                # Put back the action widgets
                notebook.create_cell_action_widgets(is_delete_current_cell=True)

            ##################################
            # Create the widgets
            ##################################

            # Select the commands
            dw['commands'] = iwid.SelectMultiple(
                options=list_all_commands,
                rows=30,
                layout=iwid.Layout(width='800px'),
                value=[str(list_all_commands[-1])],
                style=style)

            # Insert the selected commands
            button = iwid.Button(description='Insert', style={'button_color':'lightgreen'})
            button.on_click(_on_button_insert_selected_commands_clicked)
            dw['button_insert_selected_commands'] = button

            ##################################
            # Organize and display the widgets
            ##################################
            display(iwid.VBox([
                dw['commands'],
                dw['button_insert_selected_commands']
                ])
            )

        ##################################
        # Create the widgets
        ##################################

        style = {'description_width': 'initial'}

        # Dictionnary for widgets
        dw = {}

        # Select log
        dw['log'] = iwid.Dropdown(
            options=expt.list_logs,
            value=expt.default['log'],
            layout=iwid.Layout(width='400px'),
            style=style)

        # Display widgets for selection of commands
        button = iwid.Button(description='Insert commands', style={'button_color':'lightgreen'})
        button.on_click(_on_button_commands_clicked)
        dw['button_commands'] = button

        # Displat widgets for selection of positions
        button = iwid.Button(description='Insert positions', style={'button_color':'lightgreen'})
        button.on_click(_on_button_positions_clicked)
        dw['button_positions'] = button

        ##################################
        # Organize and display the widgets
        ##################################

        lvl_0 = dw['log']

        lvl_1 = iwid.HBox([dw['button_commands'], dw['button_positions']])

        display(iwid.VBox([
            lvl_0, lvl_1
            ])
        )







################################################################
################################################################
##################### INSERT IMAGE #############################
################################################################
################################################################

def display_widgets_insert_image(path_to_images_dir, list_images):
    """
    Set and display the widgets for inserting an image.

    Parameters
    ----------
    path_to_images_dir : str
        Path to the directory with the images.
    list_images : list of str
        List of images filenames in the images folder.

    """
    if len(list_images)>0:

        ##################################
        # Functions on_click
        ##################################

        def _on_button_insert_image_clicked(button):
            """
            Insert the image in the notebook.
            """
            path_to_img = path_to_images_dir + dw['image_name'].value

            # Create the markdown cells
            notebook.create_cell(code='![]('+ path_to_img+')', position ='above', celltype='markdown')

            # Put back the action widgets
            notebook.create_cell_action_widgets(is_delete_current_cell=True)

        ##################################
        # Create the widgets
        ##################################

        style = {'description_width': 'initial'}

        # Dictionnary for widgets
        dw = {}

        # Select script
        dw['image_name'] = iwid.Dropdown(
                    options=list_images,
                    value=list_images[-1],
                    layout=iwid.Layout(width='300px'),
                    style=style)

        # Insert the script
        button = iwid.Button(description='Insert', style={'button_color':'lightgreen'})
        button.on_click(_on_button_insert_image_clicked)
        dw['button_insert'] = button

        ##################################
        # Organize and display the widgets
        ##################################

        lvl_0 = iwid.HBox([dw['image_name'], dw['button_insert']])

        display(lvl_0)





################################################################
################################################################
##################### EXPORT TO PDF ############################
################################################################
################################################################

def export_nb_to_pdf(path_to_nb, timeout=100., verbose=True,
                     output_dir='.'):
    """
    Export the notebook to pdf.

    Parameters
    ----------
    path_to_nb : str
        Path to the notebook. PDF is saved in the same folder.
    timeout : float, optional
        Time in s before timeout.
    verbose : bool, optional
        Verbose mode.
    output_dir : str, optional
        Output directory.

    """
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    time0 = time.time()
    token = 1
    while token>0:
        if (time.time()-time0) > timeout:
            export_done = False
            break
        else:
            time.sleep(3)
            command = 'jupyter nbconvert '
            command+= path_to_nb
            command+= ' --output-dir %s'%output_dir
            command+= ' --to pdf '
            command+= ' --TagRemovePreprocessor.remove_cell_tags \'notPrint\' ' # Remove the widgets from the PDF
            command+= ' --no-input ' # Remove the code cells
            #command+= '--template latex_template.tplx' # Custom template (not working with nbconvert >= 6.0)
            command+= ' --PDFExporter.latex_command="[\'xelatex\', \'{filename}\']"> nbconvert.log 2>&1' # Write the log in a file
            token = subprocess.call(command, shell=True)
            if token==0:
                export_done = True

    if verbose:
        if export_done:
            print('Notebook exported to %s/%s.pdf\n'%(output_dir,path_to_nb.split('.')[0]))
        else:
            print('There was something wrong with the export to pdf.')
            print('Try this:')
            print('1) Check that the pdf is not already open in a pdf viewer.')
            print('2) Check that the value of ```notebook``` in the first cell (top of the notebook)')
            print('is set to the current name of the notebook.')
            print('3) Look at the log file nbconvert.log to find the error.')
            print('4) Re-execute the first cell.')
            print('5) Try to export the pdf again in the last cell (bottom of the Notebook).\n')





################################################################
################################################################
##################### EXTRACT FROM LOGS ########################
################################################################
################################################################

def export_logs_to_rlogs(path_to_logs_dir, list_logs, verbose=True,
                         output_dir='readable_logs/'):
    """
    Export all the logs to a human-readable version with date and commands only.

    Parameters
    ----------
    path_to_logs_dir : str
        Path to the logs files directory, e.g. `user/logs/`.
    list_logs : list of str
        List of log filenames in the logs folder.
    verbose : bool, optional
        Verbose mode.
    output_dir : str, optional
        Output directory.

    """
    if len(list_logs)>0:

        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        # Convert all the logs
        for file in list_logs:
            arr_all_commands = extract_commands_from_log(path_to_logs_dir+file)

            if len(np.shape(arr_all_commands))>0:
                with open(output_dir+'r_'+file[1:],'w') as wfile:
                    for line in arr_all_commands:
                        wfile.write(line)

        if verbose:
            print('Logs converted and saved in %s\n'%(output_dir))

def extract_commands_from_log(path_to_log_file):
    """
    Extract all commands from a log file.

    Parameters
    ----------
    path_to_log_file : str
        Path to the log file.

    Returns
    -------
    arr_all_commands : array of str
        Extracted commands.

    """
    # Define the elements to be removed from the original log file
    remove_elem_0 = ['#', ']', '\n']
    remove_elem = [
                  'DevError', 'desc =', 'origin =', 'reason =', 'severity =',
                  'connection', 'DEBUG', '.e-', '.e+',
                   ]

    # List of possible dates
    date_list = ['# Mon', '# Tue', '# Wed', '# Thu', '# Fri', '# Sat', '# Sun']
    date = ''

    with open(path_to_log_file, 'r', encoding='utf8', errors='ignore') as file:
        log_lines = file.readlines()

    arr_all_commands = np.empty([], dtype ='<U1000')

    for log_line in log_lines:
        if any(date in log_line for date in date_list):
            date = log_line.replace('\n',' ')[2:]
        elif 'Scan File Name' in log_line:
            scan_number = log_line.split('_')[-1].replace('\n','')
            while scan_number[0]=='0':
                # Delete trailing zero
                scan_number = scan_number[1:]
            arr_all_commands[-1] = arr_all_commands[-1][:-1] +  ' #' + scan_number +'\n'
        elif '*** ABORT ***' in log_line:
            arr_all_commands = np.append(arr_all_commands, 'ABORTED :'+arr_all_commands[-1])
        elif 'Scan aborted' in log_line:
            arr_all_commands = np.append(arr_all_commands, 'SCAN ABORTED: '+arr_all_commands[-1])

        elif log_line[0] in remove_elem_0:
            pass
        elif any([elem in log_line for elem in remove_elem]):
            pass
        else:
            arr_all_commands = np.append(arr_all_commands, date+log_line)

    # Remove first empty line
    if len(np.shape(arr_all_commands))>0:
        arr_all_commands = arr_all_commands[1:]

    return arr_all_commands



def extract_positions_from_log(path_to_log_file):
    """
    Extract all positions from a log file.

    Parameters
    ----------
    path_to_log_file : str
        Path to the log file.

    Returns
    -------
    list_positions : array of str
        Extracted positions.

    """
    list_positions = []

    with open(path_to_log_file, encoding='utf8', errors='ignore') as file:
        for line in file:
            if '# ' in line:
                temp = line
            if ('wm ' in line and 'pwm' not in line and 'ERROR' not in line):
                list_positions.append(temp.replace('\n','')+'; '+line.replace('\n',''))
    file.close()

    return list_positions


def extract_absorbers_from_log(path_to_logs_dir, list_logs, scan_name):
    """
    Extract from a log file the absorbers used for a specific scan.

    Parameters
    ----------
    path_to_logs_dir : str
        Path to the logs files directory, e.g. `user/logs/`.
    list_logs : list of str
        List of log filenames in the logs folder.
    scan_name : str
        Name of the scan, e.g. 'SIRIUS_2020_03_12_0756'.

    Returns
    -------
    absorbers : str
        Absorbers used for the scan.

    """
    scan_found = False
    absorbers = 'No absorbers found in the log'
    temp = ''

    # Look for the scan in all the logs file present in the folder
    for log_file in list_logs:
        with open(path_to_logs_dir+log_file, encoding='utf8', errors='ignore') as f:
            for line in f:
                if "Absorbers (wheel) are present" in line:
                    temp = line.split(': ')[-1]+' (wheel)' 
                if "No absorbers (wheel)" in line:
                    temp = 'No absorbers (wheel)'
                if "Fast absorbers are present" in line:
                    temp += '; '+line.split(': ')[-1]+' (fast absorbers)'
                if "No fast absorbers" in line:
                    temp += '; No fast absorbers'
                if scan_name in line:
                    # Remove the line jump
                    absorbers = temp.replace('\n','')
                    scan_found = True
            f.close()

        if scan_found:
            break

    return absorbers

################################################################
################################################################
##################### SAVE/LOAD PARAMS #########################
################################################################
################################################################

def display_widgets_load_params_from_json(expt):
    """
    Set and display the widgets for loading params from
    a json file.

    Parameters
    ----------
    expt : object
        Object of the class Experiment.

    """
    if len(expt.list_params_files)>0:

        ##################################
        # Preparation
        ##################################

        if len(expt.list_params_files)>1:
            default_file = expt.list_params_files[1]
        else:
            default_file = expt.list_params_files[0]

        ##################################
        # Functions on_click
        ##################################

        def _on_button_load_json_clicked(button):
            """
            Extract the parameters from the selected json file.
            """
            expt.load_params_from_json(expt.paths['dir_params']+dw['json_name'].value)
            print('Params imported from %s.'%(expt.paths['dir_params']+dw['json_name'].value))

        ##################################
        # Create the widgets
        ##################################

        style = {'description_width': 'initial'}

        # Dictionnary for widgets
        dw = {}

        # Choose the json file
        dw['json_name'] = iwid.Dropdown(
            options=expt.list_params_files,
            value=default_file,
            layout=iwid.Layout(width='300px'),
            style=style)

        button = iwid.Button(description='Load')
        button.on_click(_on_button_load_json_clicked)
        dw['button_load_json'] = button

        ##################################
        # Organize and display the widgets
        ##################################

        display(iwid.HBox([
            dw['json_name'],
            dw['button_load_json']
            ])
        )


def display_widgets_save_params_in_json(expt):
    """
    Set and display the widgets for saving params in
    a json file.

    Parameters
    ----------
    expt : object
        Object of the class Experiment.

    """
    ##################################
    # Preparation
    ##################################

    date = datetime.now().strftime("%Y%m%d-%Hh%Mm%Ss")
    filename = 'params_%s.json'%date

    ##################################
    # Functions on_click
    ##################################

    def _on_button_save_json_clicked(button):
        """
        Save the parameters in the selected json file.
        """
        expt.save_params_in_json(expt.paths['dir_params']+dw['json_name'].value)
        print('Params saved in %s.'%(expt.paths['dir_params']+dw['json_name'].value))

    ##################################
    # Create the widgets
    ##################################

    style = {'description_width': 'initial'}

    # Dictionnary for widgets
    dw = {}

    # Choose the json file
    dw['json_name'] = iwid.Text(
        description='File name:',
        value=filename,
        layout=iwid.Layout(width='500px'),
        style=style)

    button = iwid.Button(description='Save')
    button.on_click(_on_button_save_json_clicked)
    dw['button_save_json'] = button

    ##################################
    # Organize and display the widgets
    ##################################

    display(iwid.HBox([
        dw['json_name'],
        dw['button_save_json']
        ])
    )

################################################################
################################################################
################ LOAD LIST DEAD PIXELS #########################
################################################################
################################################################

def extract_dead_pixels(file_dead_pixels):
    """
    Extract the dead pixels file.

    Parameters
    ----------
    file_dead_pixels : str
        Path to the dead pixels file, e.g. 'lib/params/dead_pixels/dead_pixels_pilatus.dat'

    Returns
    -------
    arr_dead_pixels : array
        (y,x) positions of the dead pixels.

    """
    # Get positions of the dead pixels
    if os.path.isfile(file_dead_pixels):
        arr_dead_pixels = np.genfromtxt(file_dead_pixels, dtype = 'uint16', delimiter = ', ')
    else:
        print('Careful: the file '+file_dead_pixels+' was not found. Taking no dead pixels.')
        arr_dead_pixels = []

    return arr_dead_pixels
