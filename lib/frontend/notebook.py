"""
Module dealing with actions on the notebook itself (create cell, save nb, ...).
The code requires use of Javascript to modify the structure of the notebook
and is therefore not usable in Jupyter Lab, which forbids Javascript.
"""
import time
import base64
from IPython.display import Javascript, display
from ipywidgets import Widget

def save_nb():
    """
    Save the current state of the notebook (including the widgets).
    """
    #display(HTML('<script>Jupyter.menubar.actions._actions["widgets:save-with-widgets"].handler()</script>') )
    display(Javascript('IPython.notebook.save_checkpoint();'))

def delete_cell():
    """
    Delete the cell in which this function was called in the Jupyter Notebook.
    """
    display(Javascript(
        """
        var index = IPython.notebook.get_selected_cells_indices();
        IPython.notebook.delete_cell(index);
        """
    ))

def create_cell(code, position='below', celltype='markdown',
                is_print=True, is_execute=True):
    """
    Create a cell in the Jupyter Notebook.

    Parameters
    ----------
    code : str
        Code to display in the cell
    position : str, optional
        Where to put the cell: 'below' or 'at_bottom'
    celltype : str, optional
        Type of cell: 'code' or 'markdown'
    is_print : bool, optional
        Print the cell in the pdf report
    is_execute : bool, optional
        Execute the cell after its creation

    """
    encoded_code = (base64.b64encode(code.encode('latin1'))).decode('utf8')

    # Create a unique id for the cell based on epoch time
    time.sleep(0.1) # Delay to ensure unique id
    display_id = int(time.time()*1e9)

    js_code = """var cell = IPython.notebook.insert_cell_{0}("{1}");
              cell.set_text(atob("{2}"));
              """

    if not is_print:
        js_code += """cell.metadata.tags = ['notPrint']
                   """

    if is_execute:
        js_code += """cell.execute();
                   """

    display(Javascript(js_code.format(position, celltype, encoded_code)),display_id=display_id)

    # Necessary hack to avoid self-execution of cells at notebook re-opening
    display(Javascript(""" """), display_id=display_id, update=True)

def refresh_cell():
    """
    Refresh the current cell.
    """
    # Create a unique id for the cell based on epoch time
    display_id = int(time.time()*1e9)

    display(Javascript("""IPython.notebook.execute_selected_cells();"""),display_id=display_id)

    # Necessary hack to avoid self-execution of cells at notebook re-opening
    display(Javascript(""" """), display_id=display_id, update=True)

def create_cell_action_widgets(is_delete_current_cell):
    """
    Delete the current cell if asked, and create a cell to call the action widgets.

    Parameters
    ----------
    is_delete_current_cell : bool
        Delete the current cell if True.

    """
    create_cell(
        code='jlb.display_action()', position='at_bottom',
        celltype='code', is_print=False, is_execute=True
        )

    # Close all the active widgets to save memory
    Widget.close_all()

    if is_delete_current_cell:
        delete_cell()
