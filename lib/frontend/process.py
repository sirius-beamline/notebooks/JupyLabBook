"""
Module for process widgets.
"""
import ipywidgets as iwid
from IPython.display import display

from lib.frontend import notebook, jlb_io
from lib.frontend.process_widgets import wgixd, wgixs, wxrf, warea_detector
from lib.frontend.process_widgets import wisotherm, wdata_1d, wxrr

_RED='\x1b[31;01m'
_RESET='\x1b[0m'


def display_widgets_process(expt):
    """
    Set and display the widgets for choosing a process on a selected scan.

    Parameters
    ----------
    expt : object
        Object of the class Experiment.

    """
    ##################################
    # Functions on_click
    ##################################

    def _on_button_insert_text_clicked(button):
        """
        Format the text and insert it in a markdown cell.
        """
        jlb_io.display_widgets_insert_text()

    def _on_button_next_action_clicked(button):
        """
        Bring back the action widgets.
        """
        notebook.create_cell_action_widgets(is_delete_current_cell=True)

    def _on_button_extract_vineyard_clicked(button):
        """
        Extract the vineyard.
        """
        wgixd.display_widgets_vineyard(expt)

    def _on_button_gaussian_fit_clicked(button):
        """
        Fit current plot with a gaussian.
        """
        wdata_1d.display_widgets_1d_fit(expt, 'gaussian')

    def _on_button_erf_fit_clicked(button):
        """
        Fit current plot with an error function.
        """
        wdata_1d.display_widgets_1d_fit(expt, 'erf')

    def _on_button_energy_calib_clicked(button):
        wdata_1d.display_widgets_energy_calib(expt)

    def _on_button_process_data_1d_clicked(button):
        """
        Add current 1d plot to the notebook.
        """
        wdata_1d.display_widgets_data_1d(expt)

    def _on_button_process_gixd_clicked(button):
        """
        Display widgets for GIXD.
        """
        wgixd.display_widgets_gixd(expt)

    def _on_button_process_xrf_clicked(button):
        """
        Display widgets for XRF.
        """
        wxrf.display_widgets_xrf(expt)

    def _on_button_process_isotherm_clicked(button):
        """
        Display widgets for isotherm.
        """
        wisotherm.display_widgets_isotherm(expt)

    def _on_button_process_gixs_clicked(button):
        """
        Display widgets for GIXS.
        """
        wgixs.display_widgets_gixs(expt)

    def _on_button_process_xrr_liquid_clicked(button):
        wxrr.display_widgets_xrr_liquid(expt)

    def _on_button_process_xrr_solid_clicked(button):
        wxrr.display_widgets_xrr_solid(expt)

    def _on_button_process_area_detector_clicked(button):
        """
        Display widgets for area detector.
        """
        warea_detector.display_widgets_area_detector(expt)

    ##################################
    # Create the widgets
    ##################################

    # Dictionnary for widgets
    dw = {}

    # Fit with a gaussian
    button = iwid.Button(description='Gaussian fit')
    button.on_click(_on_button_gaussian_fit_clicked)
    dw['button_gaussian_fit'] = button

    # Fit with a erf
    button = iwid.Button(description='Erf fit')
    button.on_click(_on_button_erf_fit_clicked)
    dw['button_erf_fit'] = button

     # Extract Vineyard
    button = iwid.Button(description='Extract Vineyard')
    button.on_click(_on_button_extract_vineyard_clicked)
    dw['button_extract_vineyard'] = button

     # Add 1D plot
    button = iwid.Button(description='Add plot to report',
                         style={"button_color":"lightgreen"})
    button.on_click(_on_button_process_data_1d_clicked)
    dw['button_process_data_1d'] = button

     # Add GIXD plot
    button = iwid.Button(description='Plot GIXD',
                         style={"button_color":"lightgreen"})
    button.on_click(_on_button_process_gixd_clicked)
    dw['button_process_gixd'] = button

     # Add XRF plot
    button = iwid.Button(description='Plot XRF',
                         style={"button_color":"lightgreen"})
    button.on_click(_on_button_process_xrf_clicked)
    dw['button_process_xrf'] = button

     # Add isotherm plot
    button = iwid.Button(description='Plot isotherm',
                         style={"button_color":"lightgreen"})
    button.on_click(_on_button_process_isotherm_clicked)
    dw['button_process_isotherm'] = button

     # Add detector plot
    button = iwid.Button(description='Plot 2D detector',
                         style={"button_color":"lightgreen"})
    button.on_click(_on_button_process_area_detector_clicked)
    dw['button_process_area_detector'] = button

     # Add GIXS plot
    button = iwid.Button(description='Plot GIXS',
                         style={"button_color":"lightgreen"})
    button.on_click(_on_button_process_gixs_clicked)
    dw['button_process_gixs'] = button

     # Add XRR liquid plot
    button = iwid.Button(description='Plot XRR (liquid)',
                         style={"button_color":"lightgreen"})
    button.on_click(_on_button_process_xrr_liquid_clicked)
    dw['button_process_xrr_liquid'] = button

     # Add XRR solid plot
    button = iwid.Button(description='Plot XRR (solid)',
                         style={"button_color":"lightgreen"})
    button.on_click(_on_button_process_xrr_solid_clicked)
    dw['button_process_xrr_solid'] = button

     # Add calib energy
    button = iwid.Button(description='Energy calib.')
    button.on_click(_on_button_energy_calib_clicked)
    dw['button_energy_calib'] = button

     # Next action
    button = iwid.Button(description='Next action',
                         style={"button_color":"lightgreen"})
    button.on_click(_on_button_next_action_clicked)
    dw['button_next_action'] = button

    # Insert a markdown cell with text
    button = iwid.Button(description='Insert text',
                         style={'button_color':'lightgreen'})
    button.on_click(_on_button_insert_text_clicked)
    dw['button_insert_text'] = button

    ##################################
    # Organize and display the widgets
    ##################################

    lvl_0 = iwid.HBox([
        dw['button_insert_text'],
        dw['button_next_action'],
        ])

    lvl_1 = iwid.HBox([
        dw['button_extract_vineyard'],
        dw['button_gaussian_fit'],
        dw['button_erf_fit'],
        dw['button_energy_calib'],
        ])

    lvl_2 = iwid.HBox([
        dw['button_process_data_1d'],
        dw['button_process_gixd'],
        dw['button_process_xrf'],
        dw['button_process_isotherm'],
        ])

    lvl_3 = iwid.HBox([
        dw['button_process_gixs'],
        dw['button_process_xrr_liquid'],
        dw['button_process_xrr_solid'],
        dw['button_process_area_detector'],
        ])

    # Display the widgets
    display(lvl_0)

    if len(expt.list_scans)==1:
        # Set up an interactive 1D plot
        wdata_1d.display_interactive_1d_plot(expt.list_scans[0])
    else:
        print('Selected scans:')
        for scan in expt.list_scans:
            if scan.command:
                print('%s: %s'%(scan.nxs_name,scan.command))
            else:
                print(scan.nxs_name)

    display(iwid.VBox([
        lvl_1,
        lvl_2,
        lvl_3,
        ])
            )
