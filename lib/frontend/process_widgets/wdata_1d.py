"""
Module for data 1D widgets.
"""
import os
import numpy as np
import ipywidgets as iwid
import matplotlib.pyplot as plt
from IPython.display import display
from lib.frontend import notebook, jlb_io
from lib.backend import PyNexus as PN
from lib.backend import data_1d

_RED='\x1b[31;01m'
_RESET='\x1b[0m'

def display_interactive_1d_plot(scan):
    """
    Extract the 0D sensors from the nxs file and display an interactive 1D plot.

    Parameters
    ----------
    scan : object
        Object of the class Scan.

    Raises
    ------
    SystemExit('Could not open Nexus file.'))
        If the Nexus file cannot be accessed.
    """
    try:
        nexus = PN.PyNexusFile(scan.path_to_nxs, fast=True)
    except:
        print(_RED+'\t Could not open Nexus file.'+_RESET)
        raise Exception('Could not open Nexus file.')

    # Extract 0D data
    stamps_0d, data_0d = nexus.extractData('0D')
    nexus.close()

    sensor_list = [stamps_0d[i][0] if stamps_0d[i][1] is None else stamps_0d[i][1] for i in range(len(stamps_0d))]
    sensor_list_y2 = sensor_list + ['None']

    def interactive_1d_plot(x_label, y_label, is_logx, is_logy, y2_label, is_logy2):
        """
        Set the interactive plot.
        """
        x_arg = sensor_list.index(x_label)
        y_arg = sensor_list.index(y_label)

        fig = plt.figure()
        ax1 = fig.add_subplot(111)

        # Axe 1

        ax1.plot(data_0d[x_arg], data_0d[y_arg], 'o-')
        ax1.set_xlabel(x_label, fontsize=16)
        ax1.set_ylabel(y_label, fontsize=16)

        # Avoid strange offsets on the number range
        ax1.get_yaxis().get_major_formatter().set_useOffset(False)
        ax1.get_xaxis().get_major_formatter().set_useOffset(False)

        if is_logy:
            ax1.set_yscale('log')

        # Axe 2

        if y2_label != 'None':
            ax2 = ax1.twinx()

            y2_arg = sensor_list.index(y2_label)

            ax2.plot(data_0d[x_arg], data_0d[y2_arg], 'rx-')
            ax2.set_ylabel(y2_label, fontsize=16)

            # Avoid strange offsets on the number range
            ax2.get_yaxis().get_major_formatter().set_useOffset(False)
            ax2.get_xaxis().get_major_formatter().set_useOffset(False)

            if is_logy2:
                ax2.set_yscale('log')

        if is_logx:
            ax1.set_xscale('log')

        plt.show()

        scan.x_label = x_label
        scan.y_label = y_label
        scan.y2_label = y2_label
        scan.is_logx = is_logx
        scan.is_logy = is_logy
        scan.is_logy2 = is_logy2

    style = {'description_width': 'initial'}
    layout = iwid.Layout(width='300px', height='30px')

    wid_x_label = iwid.Dropdown(options=sensor_list, style=style, layout=layout,
                                description='x axis:')
    wid_y_label = iwid.Dropdown(options=sensor_list, style=style, layout=layout,
                                description='y1 axis:')
    wid_y2_label = iwid.Dropdown(options=sensor_list_y2, style=style, layout=layout,
                                description='y2 axis:', value = 'None')
    wid_is_logx = iwid.Checkbox(value=False, description='Log x', indent=False)
    wid_is_logy = iwid.Checkbox(value=False, description='Log y1', indent=False)
    wid_is_logy2 = iwid.Checkbox(value=False, description='Log y2', indent=False)

    ui = iwid.VBox([iwid.VBox([
                    iwid.HBox([wid_x_label, wid_is_logx]),
                    iwid.HBox([wid_y_label, wid_is_logy]),
                    iwid.HBox([wid_y2_label, wid_is_logy2])
                                    ])
                      ])

    out = iwid.interactive_output(interactive_1d_plot,
        {'x_label': wid_x_label, 'y_label': wid_y_label, 'is_logx': wid_is_logx, 'is_logy': wid_is_logy,
        'y2_label': wid_y2_label, 'is_logy2': wid_is_logy2}
        )

    display(ui, out)

def display_widgets_1d_fit(expt, function_name):
    """
    Set and display the widgets for fitting 1d data with a predefined function.
    So far, no widget is required and this function directly creates the cell.
    This function leaves the possibility to add future intermediate widgets.

    Parameters
    ----------
    expt : object
        Object of the class Experiment.
    function_name : str
        Predefined function to be used for the fit, e.g. 'gaussian' or 'erf'.

    """
    if len(expt.list_scans)>1:
        print(_RED+'\t Cannot process several files at once.'+_RESET)

    else:
        scan = expt.list_scans[0]

        # Extract absorbers
        absorbers = jlb_io.extract_absorbers_from_log(
            path_to_logs_dir=expt.paths['dir_logs'],
            list_logs=expt.list_logs,
            scan_name=scan.name)

        # Create a cell with the code to call the backend function
        code = (
        "x, y, y_fit, lmfit_result = "
        "data_1d.process_%s_fit(\n"
        "nxs_name='%s', "
        "path_to_nxs_dir=%s,\n"
        "x_label='%s', "
        "y_label='%s',\n"
        "params_init=%s, "
        "absorbers='%s',\n"
        "path_to_save_dir=%s, "
        "is_print_stamps=%s, "
        "is_plot=%s, "
        "is_save=%s, "
        "is_print_info=%s"
        ")"%(
            function_name,
            scan.nxs_name,
            'paths[\'dir_nxs\']',
            scan.x_label,
            scan.y_label,
            None,
            absorbers,
            'paths[\'dir_save\']',
            False,
            True,
            True,
            False
            )
        )

        notebook.create_cell(
            code=code, position='below', celltype='code',
            is_print=True, is_execute=True
            )

        # Put back the action widgets
        notebook.create_cell_action_widgets(is_delete_current_cell=True)


def display_widgets_data_1d(expt):
    """
    Set and display the widgets for processing 1d data.
    So far, no widget is required and this function directly creates the cell.
    This function leaves the possibility to add future intermediate widgets.

    Parameters
    ----------
    expt : object
        Object of the class Experiment.

    """
    if len(expt.list_scans)>1:
        print(_RED+'\t Cannot process several files at once.'+_RESET)

    else:
        scan = expt.list_scans[0]

        # Extract absorbers
        absorbers = jlb_io.extract_absorbers_from_log(
            path_to_logs_dir=expt.paths['dir_logs'],
            list_logs=expt.list_logs,
            scan_name=scan.name)

        if scan.y2_label != 'None':
            # Create a cell with the code to call the backend function
            # with two y axis
            code = (
            "x, y1, y2 = "
            "data_1d_twinx.process_data_1d(\n"
            "nxs_name='%s', "
            "path_to_nxs_dir=%s,\n"
            "x_label='%s', "
            "y_label='%s', "
            "y2_label='%s', "
            "is_logx=%s, "
            "is_logy=%s, "
            "is_logy2=%s, "
            "absorbers='%s',\n"
            "path_to_save_dir=%s, "
            "is_print_stamps=%s, "
            "is_plot=%s, "
            "is_save=%s, "
            "is_print_info=%s"
            ")"%(
                scan.nxs_name,
                'paths[\'dir_nxs\']',
                scan.x_label,
                scan.y_label,
                scan.y2_label,
                scan.is_logx,
                scan.is_logy,
                scan.is_logy2,
                absorbers,
                'paths[\'dir_save\']',
                False,
                True,
                True,
                False
                )
            )

        else:
            # Create a cell with the code to call the backend function
            # with one y axis
            code = (
            "x, y = "
            "data_1d.process_data_1d(\n"
            "nxs_name='%s', "
            "path_to_nxs_dir=%s,\n"
            "x_label='%s', "
            "y_label='%s', "
            "is_logx=%s, "
            "is_logy=%s, "
            "absorbers='%s',\n"
            "path_to_save_dir=%s, "
            "is_print_stamps=%s, "
            "is_plot=%s, "
            "is_save=%s, "
            "is_print_info=%s"
            ")"%(
                scan.nxs_name,
                'paths[\'dir_nxs\']',
                scan.x_label,
                scan.y_label,
                scan.is_logx,
                scan.is_logy,
                absorbers,
                'paths[\'dir_save\']',
                False,
                True,
                True,
                False
                )
            )



        notebook.create_cell(
            code=code, position='below', celltype='code',
            is_print=True, is_execute=True
            )

        # Put back the action widgets
        notebook.create_cell_action_widgets(is_delete_current_cell=True)


def display_widgets_energy_calib(expt):
    """
    Set and display the widgets for energy calibration with standards.

    Parameters
    ----------
    expt : object
        Object of the class Experiment.

    Raises
    ------
    SystemExit('Could not open Nexus file.'))
        If the Nexus file cannot be accessed.

    """
    ##################################
    # Functions on_click
    ##################################

    def _on_button_preview_plot_clicked(button):
        """
        Preview the plot.
        """
        # Update the params with the widget values
        expt.update_param_from_widget(dw,[
            'xas_signal_label', 'xas_norm_label', 'xas_standard_name',
            'xas_energy_min', 'xas_energy_max', 'xas_energy_shift',
            'energy_current', 'bragg_current'
            ]
        )

        data_1d.process_energy_calib(
            nxs_name=scan.nxs_name,
            path_to_nxs_dir=expt.paths['dir_nxs'],
            xas_energy_min=expt.params['xas_energy_min'],
            xas_energy_max=expt.params['xas_energy_max'],
            xas_energy_shift=expt.params['xas_energy_shift'],
            xas_signal_label = expt.params['xas_signal_label'],
            xas_norm_label = expt.params['xas_norm_label'],
            xas_standard_name=expt.params['xas_standard_name'],
            energy_current=expt.params['energy_current'],
            bragg_current=expt.params['bragg_current'],
            path_to_xas_dir=path_to_xas_dir,
            path_to_save_dir=expt.paths['dir_save'],
            is_save=False,
            )

    def _on_button_insert_plot_clicked(button):
        """
        Process and plot the energy calibration.
        """
        # Update the params with the widget values
        expt.update_param_from_widget(dw,[
            'xas_signal_label', 'xas_norm_label', 'xas_standard_name',
            'xas_energy_min', 'xas_energy_max', 'xas_energy_shift',
            'energy_current', 'bragg_current'
            ]
        )

        # Create a cell with the code to call the backend function
        code = (
        "data_1d.process_energy_calib(\n"
        "nxs_name='%s', "
        "path_to_nxs_dir=%s,\n"
        "xas_energy_min=%s, "
        "xas_energy_max=%s, "
        "xas_energy_shift=%s,\n"
        "xas_signal_label='%s', "
        "xas_norm_label='%s', "
        "xas_standard_name='%s',\n"
        "energy_current=%s, "
        "bragg_current=%s,\n"
        "path_to_xas_dir='%s', "
        "path_to_save_dir=%s, "
        "is_save=%s "
        ")"%(
            scan.nxs_name,
            'paths[\'dir_nxs\']',
            expt.params['xas_energy_min'],
            expt.params['xas_energy_max'],
            expt.params['xas_energy_shift'],
            expt.params['xas_signal_label'],
            expt.params['xas_norm_label'],
            expt.params['xas_standard_name'],
            expt.params['energy_current'],
            expt.params['bragg_current'],
            path_to_xas_dir,
            'paths[\'dir_save\']',
            expt.params['is_save']
            )
        )

        notebook.create_cell(
            code=code, position='below', celltype='code',
            is_print=True, is_execute=True
            )

        # Put back the action widgets
        notebook.create_cell_action_widgets(is_delete_current_cell=True)


    if len(expt.list_scans)>1:
        print(_RED+'\t Cannot process several files at once.'+_RESET)

    else:

        ##########################
        # Prepare the widgets
        ##########################

        scan = expt.list_scans[0]

        try:
            nexus = PN.PyNexusFile(scan.path_to_nxs, fast=True)
        except:
            print(_RED+'\t Could not open Nexus file.'+_RESET)
            raise Exception('Could not open Nexus file.')

        # Extract 0D data
        stamps_0d, data_0d = nexus.extractData('0D')
        nexus.close()

        sensor_list = [stamps_0d[i][0] if stamps_0d[i][1] is None else stamps_0d[i][1] for i in range(len(stamps_0d))]

        if 'energydcm' not in sensor_list:
            print('Sensor energydcm not in the sensor list. Please select a scan in energy.')

        else:
            energydcm_arg = sensor_list.index('energydcm')

            if 'mon4' in sensor_list:
                signal_init = 'mon4'
            else:
                signal_init = sensor_list[0]

            if 'mon2' in sensor_list:
                norm_init = 'mon2'
            else:
                norm_init = 'No norm.'

            # List of available XAS standards
            path_to_xas_dir = expt.paths['dir_params']+'xas_standards/'
            xas_standards_list = [elem for elem in os.listdir(path_to_xas_dir) if 'XAS' in elem]

            ##################################
            # Create the widgets
            ##################################

            style = {'description_width': 'initial'}

            # Dictionnary for widgets
            dw = {}

            # Select the signal
            dw['xas_signal_label'] = iwid.Dropdown(
                options=sensor_list,
                value=signal_init,
                style=style,
                layout=iwid.Layout(width='200px'),
                description='Signal:')

            # Select the normalization
            dw['xas_norm_label'] = iwid.Dropdown(
                options=sensor_list+['No norm.'],
                value=norm_init,
                style=style,
                layout=iwid.Layout(width='250px'),
                description='Normalization:')

            # Select the XAS standard
            dw['xas_standard_name'] = iwid.Dropdown(
                options=xas_standards_list,
                value=xas_standards_list[0],
                style=style,
                layout=iwid.Layout(width='350px'),
                description='Standard:')

            # Energy min
            dw['xas_energy_min'] = iwid.FloatText(
                value=np.round(np.nanmin(data_0d[energydcm_arg]),4),
                style=style,
                layout=iwid.Layout(width='200px'),
                description='E min. (keV)')

            # Energy max
            dw['xas_energy_max'] = iwid.FloatText(
                value=np.round(np.nanmax(data_0d[energydcm_arg]),4),
                style=style,
                layout=iwid.Layout(width='200px'),
                description='E max. (keV)')

            # Shift
            dw['xas_energy_shift'] = iwid.FloatText(
                value=0.,
                style=style,
                layout=iwid.Layout(width='200px'),
                description='Shift (keV)')

            # Current energy
            dw['energy_current'] = iwid.FloatText(
                value=0.,
                style=style,
                layout=iwid.Layout(width='200px'),
                description='Current energy (keV)')

            # Current bragg
            dw['bragg_current'] = iwid.FloatText(
                value=0.,
                style=style,
                layout=iwid.Layout(width='300px'),
                description='Current Bragg (deg)')

            # Preview
            button = iwid.Button(description='Preview plot',
                                 style={"button_color":"lightgreen"})
            button.on_click(_on_button_preview_plot_clicked)
            dw['button_preview_plot'] = button

            # Proceed
            button = iwid.Button(description='Insert plot',
                                 style={"button_color":"lightgreen"})
            button.on_click(_on_button_insert_plot_clicked)
            dw['button_insert_plot'] = button

            ##################################
            # Organize and display the widgets
            ##################################

            lvl_0 = iwid.HBox([
                dw['xas_signal_label'], dw['xas_norm_label'], dw['xas_standard_name']
                ])

            lvl_1 = iwid.HBox([
                dw['xas_energy_min'], dw['xas_energy_max'], dw['xas_energy_shift']
                ])

            lvl_2 = iwid.HBox([
                dw['energy_current'], dw['bragg_current']
                ])

            # Display the widgets
            display(iwid.VBox([
                lvl_0,
                lvl_1,
                lvl_2,
                iwid.HBox([dw['button_preview_plot'], dw['button_insert_plot']])
                ])
                    )

