"""
Module for GIXD widgets.
"""
import ipywidgets as iwid
from IPython.display import display
from lib.frontend import notebook, jlb_io

def display_widgets_gixd(expt):
    """
    Set and display the widgets for processing GIXD.

    Parameters
    ----------
    expt : object
        Object of the class Experiment.

    """
    ##################################
    # Functions on_click
    ##################################

    def _on_button_insert_plot_clicked(button):
        """
        Process and plot the GIXD.
        """
        # Update the params with the widget values
        expt.update_param_from_widget(dw,[
            'is_plot', 'is_save', 'is_gixd_logx', 'is_gixd_logy', 'is_gixd_logz',
            'is_compute_qz', 'is_force_gamma', 'is_print_stamps', 'is_print_info',
            'is_print_abs', 'channel0', 'thetaz_factor', 'wavelength', 'thetac',
            'fgamma', 'bin_size', 'nb_levels', 'moy_to_create', 'map_gixd', 'is_raster',
            'x0_soller', 'size_x_soller', 'y0_soller', 'size_y_soller', 'selected_sensors_str',
            ]
        )

        for scan in expt.list_scans:

            # Extract absorbers
            if expt.params['is_print_abs']:
                absorbers = jlb_io.extract_absorbers_from_log(
                    path_to_logs_dir=expt.paths['dir_logs'],
                    list_logs=expt.list_logs,
                    scan_name=scan.name)
            else:
                absorbers = ''

            # Convert moy_to_create into a list
            list_moy_to_create =\
            [int(expt.params['moy_to_create'].split(',')[i]) for i in range(len(expt.params['moy_to_create'].split(',')))]

            # Create a cell with the code to call the backend function
            code = (
            "x, y, integ_rod, integ_rod_top, integ_rod_bottom, integ_rod_first_quarter,\\\n"
            "mat, mat_binned, channels_binned,\\\n"
            "mean_pi, mean_area, mean_gamma =\\\n"
            "gixd.process_gixd_scan(\n"
            "nxs_name='%s', "
            "path_to_nxs_dir=%s,\n"
            "channel0=%s, "
            "thetaz_factor=%s, "
            "wavelength=%s, "
            "thetac=%s,\n"
            "bin_size=%s, "
            "is_compute_qz=%s, "
            "is_force_gamma=%s, "
            "fgamma=%s,\n"
            "selected_sensors=%s,\n"
            "absorbers='%s', "
            "is_gixd_logx=%s, "
            "is_gixd_logy=%s, "
            "is_gixd_logz=%s,\n"
            "nb_levels=%s, "
            "map_gixd='%s', "
            "path_to_save_dir=%s, "
            "list_moy_to_create=%s,\n"
            "roi_soller=[%s, %s, %s, %s], "
            "is_print_stamps=%s, "
            "is_plot=%s,\n"
            "is_save=%s, "
            "is_print_info=%s, "
            "is_raster=%s"
            ")"%(
                scan.nxs_name,
                'paths[\'dir_nxs\']',
                expt.params['channel0'],
                expt.params['thetaz_factor'],
                expt.params['wavelength'],
                expt.params['thetac'],
                expt.params['bin_size'],
                expt.params['is_compute_qz'],
                expt.params['is_force_gamma'],
                expt.params['fgamma'],
                expt.params['selected_sensors_str'].split(';'),
                absorbers,
                expt.params['is_gixd_logx'],
                expt.params['is_gixd_logy'],
                expt.params['is_gixd_logz'],
                expt.params['nb_levels'],
                expt.params['map_gixd'],
                'paths[\'dir_save\']',
                list_moy_to_create,
                expt.params['x0_soller'],
                expt.params['y0_soller'],
                expt.params['size_x_soller'],
                expt.params['size_y_soller'],
                expt.params['is_print_stamps'],
                expt.params['is_plot'],
                expt.params['is_save'],
                expt.params['is_print_info'],
                expt.params['is_raster']
                )
            )

            notebook.create_cell(
                code=code, position='below', celltype='code',
                is_print=True, is_execute=True
                )

            # Add a title for each scan
            if len(expt.list_scans)>1:
                if scan.command:
                    code = '### '+scan.name+': '+scan.command
                else:
                    code = '### '+scan.name
                notebook.create_cell(code=code, position='below', celltype='markdown')

        # Put back the action widgets
        notebook.create_cell_action_widgets(is_delete_current_cell=True)

    ##################################
    # Create the widgets
    ##################################

    style = {'description_width': 'initial'}

    # Dictionnary for widgets
    dw = {}

    # Add the plot to the notebook
    dw['is_plot'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_plot', alt=True),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Plot')

    # Save the results
    dw['is_save'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_save', alt=True),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Save')

    # Log x
    dw['is_gixd_logx'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_gixd_logx', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Log x')

    # Log y
    dw['is_gixd_logy'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_gixd_logy', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Log y')

    # Log z
    dw['is_gixd_logz'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_gixd_logz', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Log z')

    # Compute qz
    dw['is_compute_qz'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_compute_qz', alt=True),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Compute qz')

    # Force gamma
    dw['is_force_gamma'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_force_gamma', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Force gamma (deg):')

    # Print data stamps
    dw['is_print_stamps'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_print_stamps', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Print sensors')

    # Print scan info
    dw['is_print_info'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_print_info', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Print scan info')

    # Print absorbers
    dw['is_print_abs'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_print_abs', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Print absorbers')

    # Rasterize the plot
    dw['is_raster'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_raster', alt=True),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Rasterize')

    # Channel 0
    dw['channel0'] = iwid.FloatText(
        value=expt.get_default_param_value('channel0', alt=640.),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Vineyard (channel)')

    # Thetaz factor from calibration
    dw['thetaz_factor'] = iwid.FloatText(
        value=expt.get_default_param_value('thetaz_factor', alt=0.000243),
        style=style,
        layout=iwid.Layout(width='300px'),
        description='thetaz_factor (rad/channel)')

    # Wavelength
    dw['wavelength'] = iwid.FloatText(
        value=expt.get_default_param_value('wavelength', alt=0.155),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Wavelength (nm)')

    # Critical theta
    dw['thetac'] = iwid.FloatText(
        value=expt.get_default_param_value('thetac', alt=0.0028),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Critical theta (rad)')

    # Value of gamma if gamma is forced
    dw['fgamma'] = iwid.FloatText(
        value=expt.get_default_param_value('fgamma', alt=0.),
        style=style,
        layout=iwid.Layout(width='80px'))

    # Bin size
    dw['bin_size'] = iwid.IntText(
        value=expt.get_default_param_value('bin_size', alt=10),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Bin size')

    # Nb levels on color scale
    dw['nb_levels'] = iwid.IntText(
        value=expt.get_default_param_value('nb_levels', alt=50),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Nb levels')

    # Moy files to create
    dw['moy_to_create'] = iwid.Text(
        value=expt.get_default_param_value('moy_to_create', alt='10, 20, 40'),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Moy to create')

    # Color map
    dw['map_gixd'] = iwid.Dropdown(
        value=expt.get_default_param_value('map_gixd', alt='jet'),
        style=style,
        options=['viridis', 'jet', 'Greys', 'cividis', 'hot'],
        rows=5,
        description='Map')

    # x0 Soller
    dw['x0_soller'] = iwid.IntText(
        value=expt.get_default_param_value('x0_soller', alt=510),
        style=style,
        layout=iwid.Layout(width='250px'),
        description='ROI Soller: x0')

    # y0 Soller
    dw['y0_soller'] = iwid.IntText(
        value=expt.get_default_param_value('y0_soller', alt=350),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='y0')

    # size x Soller
    dw['size_x_soller'] = iwid.IntText(
        value=expt.get_default_param_value('size_x_soller', alt=130),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='size x')

    # size y Soller
    dw['size_y_soller'] = iwid.IntText(
        value=expt.get_default_param_value('size_y_soller', alt=692),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='size y')

    # Selected sensors to plot
    dw['selected_sensors_str'] = iwid.Text(
        value=expt.get_default_param_value('selected_sensors_str', alt=''),
        style=style,
        layout=iwid.Layout(width='800px'),
        description='Sensors to add (sep. with \";\")')

    # Proceed
    button = iwid.Button(description='Insert plot',
                         style={"button_color":"lightgreen"})
    button.on_click(_on_button_insert_plot_clicked)
    dw['button_insert_plot'] = button

    ##################################
    # Organize and display the widgets
    ##################################

    lvl_0 = iwid.HBox([
        dw['is_plot'], dw['is_save'], dw['is_print_stamps'], dw['is_print_info'], dw['is_print_abs'],
        dw['is_gixd_logx'], dw['is_gixd_logy'], dw['is_gixd_logz'], dw['map_gixd'],
        ])

    lvl_1 = iwid.HBox([
        dw['x0_soller'], dw['y0_soller'], dw['size_x_soller'], dw['size_y_soller']
        ])

    lvl_2 = iwid.HBox([
        dw['bin_size'], dw['nb_levels'], dw['moy_to_create'], dw['channel0'], dw['is_compute_qz'], dw['is_raster']
        ])

    lvl_3 = iwid.HBox([
        dw['wavelength'], dw['thetac'], dw['thetaz_factor'], dw['is_force_gamma'], dw['fgamma']
        ])

    lvl_4 = iwid.HBox([
        dw['selected_sensors_str']
        ])

    # Display the widgets
    display(iwid.VBox([
        lvl_0,
        lvl_1,
        lvl_2,
        lvl_3,
        lvl_4,
        dw['button_insert_plot']
        ])
            )

def display_widgets_vineyard(expt):
    """
    Set and display the widgets for extracting the Vineyard peak.
    So far, no widget is required and this function directly creates the cell.
    This function leaves the possibility to add future intermediate widgets.

    Parameters
    ----------
    expt : object
        Object of the class Experiment.

    """
    for scan in expt.list_scans:
        # Create a cell with the code to call the backend function
        code = (
        "jlb.expt.params['channel0'] = "
        "gixd.process_vineyard(\n"
        "nxs_name='%s', "
        "path_to_nxs_dir=%s,\n"
        "bin_size=%s, "
        "is_force_gamma=%s, "
        "fgamma=%s,\n"
        "is_gixd_logx=%s, "
        "is_gixd_logy=%s, "
        "is_gixd_logz=%s,\n"
        "nb_levels=%s, "
        "map_gixd='%s', "
        "roi_soller=%s,\n"
        "is_print_stamps=%s, "
        "is_plot=%s, "
        "is_print_info=%s, "
        "is_raster=%s"
        ")"%(
            scan.nxs_name,
            'paths[\'dir_nxs\']',
            10,
            False,
            0.,
            False,
            False,
            False,
            50,
            'jet',
            [510, 350, 130, 692],
            True,
            True,
            True,
            False
            )
        )

        notebook.create_cell(
            code=code, position='below', celltype='code',
            is_print=True, is_execute=True
            )

        # Add a title for each scan
        if len(expt.list_scans)>1:
            if scan.command:
                code = '### '+scan.name+': '+scan.command
            else:
                code = '### '+scan.name
            notebook.create_cell(code=code, position='below', celltype='markdown')

    # Put back the action widgets
    notebook.create_cell_action_widgets(is_delete_current_cell=True)

def display_widgets_calib_thetaz():
    """
    Set and display the widgets for calibration of theta z for GIXD.
    So far, no widget is required and this function directly creates the cell.
    This function leaves the possibility to add future intermediate widgets.

    """
    code = \
    'gamma_channel = np.array(['  +'\n'\
    '# gamma  channel'            +'\n'\
    '[0,      970],'              +'\n'\
    '[-1,     899],'              +'\n'\
    '[-2,     827],'              +'\n'\
    '[-3,     755],'              +'\n'\
    '[-4,     683],'              +'\n'\
    '[-5,     611],'              +'\n'\
    '[-6,     539],'              +'\n'\
    '])'                          +'\n'\
    'jlb.expt.params[\'thetaz_factor\'] = gixd.calib_thetaz(gamma_channel)'

    notebook.create_cell(
        code=code, position='below', celltype='code',
        is_print=True, is_execute=False
        )

    notebook.create_cell(code='## Calibration thetaz', position='below', celltype='markdown')

    # Put back the action widgets
    notebook.create_cell_action_widgets(is_delete_current_cell=True)
