"""
Module for GIXS widgets.
"""
import ipywidgets as iwid
from IPython.display import display
from lib.frontend import notebook, jlb_io

def display_widgets_gixs(expt):
    """
    Set and display the widgets for processing GIXS.

    Parameters
    ----------
    expt : object
        Object of the class Experiment.

    """
    ##################################
    # Functions on_click
    ##################################

    def _on_button_insert_plot_clicked(button):
        """
        Process and plot the GIXS.
        """
        # Update the params with the widget values
        expt.update_param_from_widget(dw,[
            'is_plot', 'is_save_sum', 'is_save_each', 'is_gixs_logz',
            'is_force_gamma', 'is_force_delta', 'is_force_thetai',
            'is_print_stamps', 'is_print_info', 'is_print_abs',
            'distance_detec', 'pixel_poni_x', 'pixel_poni_y',
            'fgamma', 'fdelta', 'fthetai', 'pixel_size', 'wavelength',
            'x_min', 'size_x', 'y_min', 'size_y', 'clim_min', 'clim_max', 'map_gixs',
            'selected_sensors_str'
            ]
        )

        for scan in expt.list_scans:

            # Extract absorbers
            if expt.params['is_print_abs']:
                absorbers = jlb_io.extract_absorbers_from_log(
                    path_to_logs_dir=expt.paths['dir_logs'],
                    list_logs=expt.list_logs,
                    scan_name=scan.name)
            else:
                absorbers = ''

            # Create a cell with the code to call the backend function
            code = (
            "images_sum, integ_qxy, integ_qz, qxy_list, qz_list =\\\n"
            "gixs.process_gixs_scan(\n"
            "nxs_name='%s', "
            "path_to_nxs_dir=%s,\n"
            "wavelength=%s, "
            "distance_detec=%s, "
            "pixel_poni_x=%s, "
            "pixel_poni_y=%s, "
            "pixel_size=%s,\n"
            "is_force_gamma=%s, "
            "is_force_delta=%s, "
            "is_force_thetai=%s, "
            "fgamma=%s, "
            "fdelta=%s, "
            "fthetai=%s,\n"
            "roi_2d=[%s, %s, %s, %s], "
            "clim=(%s, %s),\n"
            "selected_sensors=%s,\n"
            "absorbers='%s', "
            "is_gixs_logz=%s, "
            "map_gixs='%s', "
            "path_to_save_dir=%s,\n"
            "is_print_stamps=%s, "
            "is_plot=%s, "
            "is_save_sum=%s, "
            "is_save_each=%s, "
            "is_print_info=%s"
            ")"%(
                scan.nxs_name,
                'paths[\'dir_nxs\']',
                expt.params['wavelength'],
                expt.params['distance_detec'],
                expt.params['pixel_poni_x'],
                expt.params['pixel_poni_y'],
                expt.params['pixel_size'],
                expt.params['is_force_gamma'],
                expt.params['is_force_delta'],
                expt.params['is_force_thetai'],
                expt.params['fgamma'],
                expt.params['fdelta'],
                expt.params['fthetai'],
                expt.params['x_min'],
                expt.params['y_min'],
                expt.params['size_x'],
                expt.params['size_y'],
                expt.params['clim_min'],
                expt.params['clim_max'],
                expt.params['selected_sensors_str'].split(';'),
                absorbers,
                expt.params['is_gixs_logz'],
                expt.params['map_gixs'],
                'paths[\'dir_save\']',
                expt.params['is_print_stamps'],
                expt.params['is_plot'],
                expt.params['is_save_sum'],
                expt.params['is_save_each'],
                expt.params['is_print_info']
                )
            )

            notebook.create_cell(
                code=code, position='below', celltype='code',
                is_print=True, is_execute=True
                )

            # Add a title for each scan
            if len(expt.list_scans)>1:
                if scan.command:
                    code = '### '+scan.name+': '+scan.command
                else:
                    code = '### '+scan.name
                notebook.create_cell(code=code, position='below', celltype='markdown')

        # Put back the action widgets
        notebook.create_cell_action_widgets(is_delete_current_cell=True)

    ##################################
    # Create the widgets
    ##################################

    style = {'description_width': 'initial'}

    # Dictionnary for widgets
    dw = {}

    # Add the plot to the notebook
    dw['is_plot'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_plot', alt=True),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Plot')

    # Save the sum of all images
    dw['is_save_sum'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_save_sum', alt=True),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Save sum')

    # Save each individual images
    dw['is_save_each'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_save_each', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Save each')

    # Log x
    dw['is_gixs_logz'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_gixs_logz', alt=True),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Log z')

    # Force gamma
    dw['is_force_gamma'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_force_gamma', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Force gamma (deg):')

    # Force delta
    dw['is_force_delta'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_force_delta', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Force delta (deg):')

    # Force thetai
    dw['is_force_thetai'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_force_thetai', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Force thetai (deg):')

    # Print data stamps
    dw['is_print_stamps'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_print_stamps', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Print sensors')

    # Print scan info
    dw['is_print_info'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_print_info', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Print scan info')

    # Print absorbers
    dw['is_print_abs'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_print_abs', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Print absorbers')

    # Distance sample to detector
    dw['distance_detec'] = iwid.FloatText(
        value=expt.get_default_param_value('distance_detec', alt=1000.),
        style=style,
        layout=iwid.Layout(width='250px'),
        description='Sample-detector distance (mm)')

    # PONI x
    dw['pixel_poni_x'] = iwid.FloatText(
        value=expt.get_default_param_value('pixel_poni_x', alt=0.),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='PONI x (pix)')

    # PONI y
    dw['pixel_poni_y'] = iwid.FloatText(
        value=expt.get_default_param_value('pixel_poni_y', alt=0.),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='PONI y (pix)')

    # Wavelength
    dw['wavelength'] = iwid.FloatText(
        value=expt.get_default_param_value('wavelength', alt=0.155),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Wavelength (nm)')

    # x min
    dw['x_min'] = iwid.IntText(
        value=expt.get_default_param_value('x_min', alt=0),
        style=style,
        layout=iwid.Layout(width='250px'),
        description='ROI: x0 (pix)')

    # y min
    dw['y_min'] = iwid.IntText(
        value=expt.get_default_param_value('y_min', alt=0),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='y0 (pix)')

    # size x
    dw['size_x'] = iwid.IntText(
        value=expt.get_default_param_value('size_x', alt=980),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='size x (pix)')

    # size y
    dw['size_y'] = iwid.IntText(
        value=expt.get_default_param_value('size_y', alt=1042),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='size y (pix)')
    
    # clim min
    dw['clim_min'] = iwid.IntText(
        value=expt.get_default_param_value('clim_min', alt=-1),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Color scale: min.')
    
    # clim max
    dw['clim_max'] = iwid.IntText(
        value=expt.get_default_param_value('clim_max', alt=-1),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='max.')

    # Value of gamma if gamma is forced
    dw['fgamma'] = iwid.FloatText(
        value=expt.get_default_param_value('fgamma', alt=0.),
        style=style,
        layout=iwid.Layout(width='80px'))

    # Value of delta if delta is forced
    dw['fdelta'] = iwid.FloatText(
        value=expt.get_default_param_value('fdelta', alt=0.),
        style=style,
        layout=iwid.Layout(width='80px'))

    # Value of thetai if thetai is forced
    dw['fthetai'] = iwid.FloatText(
        value=expt.get_default_param_value('fthetai', alt=0.),
        style=style,
        layout=iwid.Layout(width='80px'))

    # Pixel size
    dw['pixel_size'] = iwid.FloatText(
        value=expt.get_default_param_value('pixel_size', alt=0.172),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Pixel size (mm)')

    # Color map
    dw['map_gixs'] = iwid.Dropdown(
        value=expt.get_default_param_value('map_gixs', alt='viridis'),
        style=style,
        options=['viridis', 'jet', 'Greys', 'cividis', 'hot'],
        rows=5,
        description='Map')

    # Selected sensors to plot
    dw['selected_sensors_str'] = iwid.Text(
        value=expt.get_default_param_value('selected_sensors_str', alt=''),
        style=style,
        layout=iwid.Layout(width='800px'),
        description='Sensors to add (sep. with \";\")')

    # Proceed
    button = iwid.Button(description='Insert plot',
                         style={"button_color":"lightgreen"})
    button.on_click(_on_button_insert_plot_clicked)
    dw['button_insert_plot'] = button

    ##################################
    # Organize and display the widgets
    ##################################

    lvl_0 = iwid.HBox([
        dw['is_plot'], dw['is_save_sum'], dw['is_save_each'], dw['is_print_stamps'], dw['is_print_info'], dw['is_print_abs'],
        dw['is_gixs_logz'], dw['map_gixs']
        ])

    lvl_1 = iwid.HBox([
        dw['wavelength'], dw['distance_detec'], dw['pixel_poni_x'], dw['pixel_poni_y'], dw['pixel_size']
        ])

    lvl_2 = iwid.HBox([
        dw['is_force_gamma'], dw['fgamma'], dw['is_force_delta'], dw['fdelta'], dw['is_force_thetai'], dw['fthetai']
        ])

    lvl_3 = iwid.HBox([
        dw['x_min'], dw['y_min'], dw['size_x'], dw['size_y']
        ])

    lvl_4 = iwid.HBox([
        dw['clim_min'], dw['clim_max'], dw['selected_sensors_str']
        ])


    # Display the widgets
    print('Set any ROI value to -1 to have the whole detector.')
    print('Set color min or max to -1 to have the default color scale.')
    display(iwid.VBox([
        lvl_0,
        lvl_1,
        lvl_2,
        lvl_3,
        lvl_4,
        dw['button_insert_plot']
        ])
            )




