"""
Module for isotherm widgets.
"""
import ipywidgets as iwid
from IPython.display import display
from lib.frontend import notebook
from lib.backend import isotherm

def display_widgets_isotherm(expt):
    """
    Set and display the widgets for processing isotherms.

    Parameters
    ----------
    expt : object
        Object of the class Experiment.

    """
    ##################################
    # Functions on_click
    ##################################

    def _on_button_preview_plot_clicked(button):
        """
        Preview the isotherm plot.
        """
        # Update the params with the widget values
        expt.update_param_from_widget(dw,[
            'is_plot', 'is_save', 'is_print_stamps', 'is_print_info',
            'first_point_plot', 'last_point_plot'
            ]
        )

        for scan in expt.list_scans:
            _, _, _ = isotherm.process_isotherm_scan(
                nxs_name=scan.nxs_name,
                path_to_nxs_dir=expt.paths['dir_nxs'],
                first_point_plot=expt.params['first_point_plot'],
                last_point_plot=expt.params['last_point_plot'],
                path_to_save_dir=expt.paths['dir_save'],
                is_print_stamps=False,
                is_plot=True,
                is_save=False,
                is_print_info=False
                )

    def _on_button_insert_plot_clicked(button):
        """
        Process and plot the isotherm.
        """
        # Update the params with the widget values
        expt.update_param_from_widget(dw,[
            'is_plot', 'is_save', 'is_print_stamps', 'is_print_info',
            'first_point_plot', 'last_point_plot'
            ]
        )

        for scan in expt.list_scans:

            # Create a cell with the code to call the backend function
            code = (
            "area, pressure, time =\\\n"
            "isotherm.process_isotherm_scan(\n"
            "nxs_name='%s', "
            "path_to_nxs_dir=%s,\n"
            "first_point_plot=%s, "
            "last_point_plot=%s, "
            "path_to_save_dir=%s,\n"
            "is_print_stamps=%s, "
            "is_plot=%s, "
            "is_save=%s, "
            "is_print_info=%s"
            ")"%(
                scan.nxs_name,
                'paths[\'dir_nxs\']',
                expt.params['first_point_plot'],
                expt.params['last_point_plot'],
                'paths[\'dir_save\']',
                expt.params['is_print_stamps'],
                expt.params['is_plot'],
                expt.params['is_save'],
                expt.params['is_print_info']
                )
            )

            notebook.create_cell(
                code=code, position='below', celltype='code',
                is_print=True, is_execute=True
                )

            # Add a title for each scan
            if len(expt.list_scans)>1:
                if scan.command:
                    code = '### '+scan.name+': '+scan.command
                else:
                    code = '### '+scan.name
                notebook.create_cell(code=code, position='below', celltype='markdown')

        # Put back the action widgets
        notebook.create_cell_action_widgets(is_delete_current_cell=True)

    ##################################
    # Create the widgets
    ##################################

    style = {'description_width': 'initial'}

    # Dictionnary for widgets
    dw = {}

    # Add the plot to the notebook
    dw['is_plot'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_plot', alt=True),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Plot')

    # Save
    dw['is_save'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_save', alt=True),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Save')

    # Print data stamps
    dw['is_print_stamps'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_print_stamps', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Print sensors')

    # Print scan info
    dw['is_print_info'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_print_info', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Print scan info')

    # First point
    dw['first_point_plot'] = iwid.IntText(
        value=0,
        style=style,
        layout=iwid.Layout(width='200px'),
        description='First point')

    # Last point
    dw['last_point_plot'] = iwid.IntText(
        value=-1,
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Last point')

    # Preview
    button = iwid.Button(description='Preview plot',
                         style={"button_color":"lightgreen"})
    button.on_click(_on_button_preview_plot_clicked)
    dw['button_preview_plot'] = button

    # Proceed
    button = iwid.Button(description='Insert plot',
                         style={"button_color":"lightgreen"})
    button.on_click(_on_button_insert_plot_clicked)
    dw['button_insert_plot'] = button

    ##################################
    # Organize and display the widgets
    ##################################

    lvl_0 = iwid.HBox([
        dw['is_plot'], dw['is_save'], dw['is_print_stamps'], dw['is_print_info']
        ])

    lvl_1 = iwid.HBox([
        dw['first_point_plot'], dw['last_point_plot']
        ])

    # Display the widgets
    display(iwid.VBox([
        lvl_0,
        lvl_1,
        iwid.HBox([dw['button_preview_plot'],dw['button_insert_plot']])
        ])
            )
