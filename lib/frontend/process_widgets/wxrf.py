"""
Module for XRF widgets.
"""
import ipysheet
import ipywidgets as iwid
import numpy as np
from IPython.display import display
from lib.frontend import notebook, jlb_io
from lib.backend import xrf

_RED='\x1b[31;01m'
_RESET='\x1b[0m'

def display_widgets_xrf(expt):
    """
    Set and display the widgets for processing XRF.

    Parameters
    ----------
    expt : object
        Object of the class Experiment.

    """
    ##################################
    # Functions on_click
    ##################################

    def _on_button_identify_peaks_clicked(button):
        """
        Display the widgets for identification of peaks.
        """
        expt.update_param_from_widget(dw,[
            'is_save', 'is_plot_spectrogram', 'is_plot_first_last',
            'is_plot_sum', 'is_xrf_log', 'is_print_stamps', 'is_print_info', 'is_use_ev',
            'is_print_abs', 'sdd_elements', 'channel_xrf_first', 'channel_xrf_last',
            'sdd_gain', 'sdd_ev0', 'selected_sensors_str'
            ]
        )
        display_widgets_identify_peaks(expt, dw)

    def _on_button_insert_plot_clicked(button):
        """
        Process and plot the XRF.
        """
        if not expt.params['is_identify_peaks']:
            expt.params['arr_peaks'] = [(None,None)]

        expt.update_param_from_widget(dw,[
            'is_save', 'is_plot_spectrogram', 'is_plot_first_last',
            'is_plot_sum', 'is_xrf_log', 'is_print_stamps', 'is_print_info', 'is_use_ev',
            'is_print_abs', 'sdd_elements', 'channel_xrf_first', 'channel_xrf_last',
            'sdd_gain', 'sdd_ev0', 'selected_sensors_str'
            ]
        )

        for scan in expt.list_scans:

            # Extract absorbers
            if expt.params['is_print_abs']:
                absorbers = jlb_io.extract_absorbers_from_log(
                    path_to_logs_dir=expt.paths['dir_logs'],
                    list_logs=expt.list_logs,
                    scan_name=scan.name)
            else:
                absorbers = ''

            # Convert sdd_elements into a list
            list_sdd_elems = [int(expt.params['sdd_elements'].split(',')[i]) \
                              for i in range(len(expt.params['sdd_elements'].split(',')))]

            # Necessary to check first that the sdd elements
            # are present in the nexus file to avoid crash of the notebook
            is_icr_found, is_ocr_found, is_spectrum_found = \
            xrf.check_sdd_elems(
                    nxs_name=scan.nxs_name,
                    path_to_nxs_dir=expt.paths['dir_nxs'],
                    list_sdd_elems=list_sdd_elems
                    )


            if (is_icr_found and is_ocr_found and is_spectrum_found):
                # Break the list of peaks into smaller pieces to avoid long lines
                str_split = str(expt.params['arr_peaks']).split('),')
                # Add a line return every three peaks
                str_completed = [' \\\n'+str_split[i] if i%5==4 else str_split[i] for i in range(len(str_split))]
                str_rejoin = '),'.join(str_completed)

                # Create a cell with the code to call the backend function
                code = (
                "channels, energies, spectrums =\\\n"
                "xrf.process_xrf_scan(\n"
                "nxs_name='%s', "
                "path_to_nxs_dir=%s,\n"
                "list_sdd_elems=%s, "
                "channel_xrf_first=%s, "
                "channel_xrf_last=%s, "
                "sdd_gain=%s, "
                "sdd_ev0=%s,\n"
                "arr_peaks=%s,\n"
                "selected_sensors=%s,\n"
                "is_use_ev=%s, "
                "is_xrf_log=%s, "
                "absorbers='%s', "
                "path_to_save_dir=%s,\n"
                "is_print_stamps=%s, "
                "is_plot_spectrogram=%s, "
                "is_plot_sum=%s, "
                "is_plot_first_last=%s,\n"
                "is_save=%s, "
                "is_print_info=%s"
                ")"%(
                    scan.nxs_name,
                    'paths[\'dir_nxs\']',
                    list_sdd_elems,
                    expt.params['channel_xrf_first'],
                    expt.params['channel_xrf_last'],
                    expt.params['sdd_gain'],
                    expt.params['sdd_ev0'],
                    str_rejoin,
                    expt.params['selected_sensors_str'].split(';'),
                    expt.params['is_use_ev'],
                    expt.params['is_xrf_log'],
                    absorbers,
                    'paths[\'dir_save\']',
                    expt.params['is_print_stamps'],
                    expt.params['is_plot_spectrogram'],
                    expt.params['is_plot_sum'],
                    expt.params['is_plot_first_last'],
                    expt.params['is_save'],
                    expt.params['is_print_info']
                    )
                )

            else:
                code = 'print(\"'
                code += _RED
                if not is_icr_found:
                    code += 'ICR not found in data.\\n'
                if not is_ocr_found:
                    code += 'OCR not found in data.\\n'
                if not is_spectrum_found:
                    code += 'Spectrum not found in data.\\n'
                code += 'Put 4 in the box \'SDD elements\' for the single-element detector.\\n'
                code += 'Put 0, 1, 2, 3 in the box \'SDD elements\' for the four-elements detector.'
                code += _RESET
                code += '\")'

            notebook.create_cell(
                code=code, position='below', celltype='code',
                is_print=True, is_execute=True
                )

            # Add a title for each scan
            if len(expt.list_scans)>1:
                if scan.command:
                    code = '### '+scan.name+': '+scan.command
                else:
                    code = '### '+scan.name
                notebook.create_cell(code=code, position='below', celltype='markdown')


        # Put back the action widgets
        notebook.create_cell_action_widgets(is_delete_current_cell=True)


    ##################################
    # Create the widgets
    ##################################

    style = {'description_width': 'initial'}

    # In case the user plots without identifying peaks first
    expt.params['is_identify_peaks'] = False

    # Dictionnary for widgets
    dw = {}

    # Save the results
    dw['is_save'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_save', alt=True),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Save')

    # Print data stamps
    dw['is_print_stamps'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_print_stamps', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Print sensors')

    # Print scan info
    dw['is_print_info'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_print_info', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Print scan info')

    # Print absorbers
    dw['is_print_abs'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_print_abs', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Print absorbers')

    # Convert channels to eVs
    dw['is_use_ev'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_use_ev', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Use eV')

    # Plot spectrogram
    dw['is_plot_spectrogram'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_plot_spectrogram', alt=True),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Plot spectrogram')

    # Plot first and last spectrums
    dw['is_plot_first_last'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_plot_first_last', alt=True),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Plot first&last spectrums')

    # Plot sum of spectrums
    dw['is_plot_sum'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_plot_sum', alt=True),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Plot sum of spectrums')

    # Log z
    dw['is_xrf_log'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_xrf_log', alt=True),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Log z')

    # Elements of the sdd to use
    dw['sdd_elements'] = iwid.Text(
        value=expt.get_default_param_value('sdd_elements', alt='4'),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Elements')

    # First channel of the range
    dw['channel_xrf_first'] = iwid.IntText(
        value=expt.get_default_param_value('channel_xrf_first', alt=0),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='First channel')

    # Last channel of the range
    dw['channel_xrf_last'] = iwid.IntText(
        value=expt.get_default_param_value('channel_xrf_last', alt=2047),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Last channel')

    # Gain sdd
    dw['sdd_gain'] = iwid.FloatText(
        value=expt.get_default_param_value('sdd_gain', alt=10.),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Gain')

    # ev0 sdd
    dw['sdd_ev0'] = iwid.FloatText(
        value=expt.get_default_param_value('sdd_ev0', alt=0.),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='eV0')

    # Selected sensors to plot
    dw['selected_sensors_str'] = iwid.Text(
        value=expt.get_default_param_value('selected_sensors_str', alt=''),
        style=style,
        layout=iwid.Layout(width='800px'),
        description='Sensors to add (sep. with \";\")')

    # Identify peaks
    button = iwid.Button(description='Identify peaks',
                         style={"button_color":"lightgreen"})
    button.on_click(_on_button_identify_peaks_clicked)
    dw['button_identify_peaks'] = button

    # Proceed
    button = iwid.Button(description='Plot',
                         style={"button_color":"lightgreen"})
    button.on_click(_on_button_insert_plot_clicked)
    dw['button_insert_plot'] = button

    ##################################
    # Organize and display the widgets
    ##################################

    lvl_0 = iwid.HBox([
        dw['is_print_stamps'], dw['is_print_info'], dw['is_print_abs'], dw['is_save']
        ])

    lvl_1 = iwid.HBox([
        dw['is_plot_spectrogram'], dw['is_plot_first_last'], dw['is_plot_sum']
        ])

    lvl_2 = iwid.HBox([
        dw['is_xrf_log'], dw['sdd_elements'], dw['channel_xrf_first'], dw['channel_xrf_last']
        ])

    lvl_3 = iwid.HBox([
        dw['is_use_ev'], dw['sdd_gain'], dw['sdd_ev0']
        ])

    lvl_4 = iwid.HBox([
        dw['selected_sensors_str']
        ])

    lvl_5 = iwid.HBox([
        dw['button_identify_peaks'], dw['button_insert_plot']
        ])


    # Display the widgets
    display(iwid.VBox([
        lvl_0,
        lvl_1,
        lvl_2,
        lvl_3,
        lvl_4,
        lvl_5,
        ])
            )


def display_widgets_identify_peaks(expt, dw):
    """
    Prepare and display the widgets for identification of peaks.

    Parameters
    ----------
    expt : object
        Object of the class Experiment.
    dw : dict
        Dictionnary of widgets.

    """
    ##################################
    # Functions on_click
    ##################################

    def _on_button_validate_peaks_clicked(button):
        """
        Validate the sheet and plot a preview of the XRF with the peaks.
        """

        expt.update_param_from_widget(dw,[
            'is_save', 'is_plot_spectrogram', 'is_plot_first_last',
            'is_plot_sum', 'is_xrf_log', 'is_print_stamps', 'is_print_info', 'is_use_ev',
            'is_print_abs', 'sdd_elements', 'channel_xrf_first', 'channel_xrf_last',
            'sdd_gain', 'sdd_ev0'
            ]
        )

        expt.params['is_identify_peaks'] = True

        # Get values from the sheet
        expt.params['arr_peaks_filled'] = ipysheet.numpy_loader.to_array(ipysheet.easy.current()).tolist()

        # Remove the empty lines and make array of tuples (name, eV)
        expt.params['arr_peaks'] = [(elem[0],elem[1],elem[2]) for elem in expt.params['arr_peaks_filled']\
                                    if elem[0] is not None]

        # Send only the lines with y in the third column
        expt.params['arr_peaks'] = [elem[0:2] for elem in expt.params['arr_peaks'] if elem[2]=='y']

        # Convert sdd_elements into a list
        list_sdd_elems = [int(expt.params['sdd_elements'].split(',')[i]) \
                          for i in range(len(expt.params['sdd_elements'].split(',')))]

        for scan in expt.list_scans:

            # Necessary to check first that the sdd elements
            # are present in the nexus file to avoid crash of the notebook
            is_icr_found, is_ocr_found, is_spectrum_found = \
            xrf.check_sdd_elems(
                    nxs_name=scan.nxs_name,
                    path_to_nxs_dir=expt.paths['dir_nxs'],
                    list_sdd_elems=list_sdd_elems
                    )

            if (is_icr_found and is_ocr_found and is_spectrum_found):
                print('Peaks on scan %s'%scan.nxs_name)
                # Create a cell with the code to call the backend function
                _, _, _ = xrf.process_xrf_scan(
                    nxs_name=scan.nxs_name,
                    path_to_nxs_dir=expt.paths['dir_nxs'],
                    list_sdd_elems=list_sdd_elems,
                    channel_xrf_first = expt.params['channel_xrf_first'],
                    channel_xrf_last = expt.params['channel_xrf_last'],
                    sdd_gain=expt.params['sdd_gain'],
                    sdd_ev0=expt.params['sdd_ev0'],
                    arr_peaks=expt.params['arr_peaks'],
                    selected_sensors = None,
                    is_use_ev=expt.params['is_use_ev'],
                    is_xrf_log=expt.params['is_xrf_log'],
                    absorbers='',
                    path_to_save_dir=expt.paths['dir_save'],
                    is_print_stamps=False,
                    is_plot_spectrogram=False,
                    is_plot_sum=True,
                    is_plot_first_last=False,
                    is_save=False,
                    is_print_info=False
                    )

            else:
                if not is_icr_found:
                    print(_RED+"ICR not found in data. Check if the box \'SDD elements\' is right."+_RESET)
                if not is_ocr_found:
                    print(_RED+"OCR not found in data. Check if the box \'SDD elements\' is right."+_RESET)
                if not is_spectrum_found:
                    print(_RED+"Spectrum not found in data. Check if the box \'SDD elements\' is right."+_RESET)
                print(_RED+"Try to put 4 in the box \'SDD elements\' for the single-element detector."+_RESET)
                print(_RED+"Try to put 0, 1, 2, 3 in the box \'SDD elements\' for the four-elements detector."+_RESET)


    ##################################
    # Create the widgets
    ##################################

    # Fill the sheet with previous values or None entries
    if 'arr_peaks_filled' in expt.params:
        to_fill = np.array(expt.params['arr_peaks_filled'])
    else:
        to_fill = np.array([[None,None,None] for i in range(20)])

    # Determine the number of rows to dynamically add some empty rows
    nb_empty_rows = len([elem for elem in to_fill if (elem[0] is None or elem[0]=='')])
    if nb_empty_rows<15:
        to_fill = np.append([elem for elem in to_fill if (elem[0] is not None and elem[0]!='')],
                             np.array([[None,None,None] for i in range(15)]), axis = 0)

    # Create the sheet
    sheet = ipysheet.easy.sheet(columns=3, rows=len(to_fill) ,column_headers = ['Name','Position','Use?(y/n)'])

    # ipysheet does not work correctly with no entries
    # It is necessary to fill first the cells with something
    for i in range(3):
        ipysheet.easy.column(i,  to_fill[:,i])

    ##################################
    # Create the widgets
    ##################################

    # In case the user plots without identifying peaks first
    expt.params['is_identify_peaks'] = False

    # Validate peaks
    button_validate_peaks = \
        iwid.Button(description='Validate peaks',
                    style={"button_color":"lightgreen"})
    button_validate_peaks.on_click(_on_button_validate_peaks_clicked)

    ##################################
    # Organize and display the widgets
    ##################################

    # Display the widgets
    display(button_validate_peaks)
    display(sheet)
