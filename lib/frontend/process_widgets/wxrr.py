"""
Module for XRR widgets (on solids and liquids).
"""
import os
import ipywidgets as iwid
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib import colors
from IPython.display import display
from lib.frontend import notebook
from lib.backend import area_detector
from lib.backend import PyNexus as PN

_RED='\x1b[31;01m'
_BLUE='\x1b[34;01m'
_RESET='\x1b[0m'


def display_widgets_calib_xrr_liquid(expt):
    """
    Set and display the widgets for calibration of XRR on liquid.

    Parameters
    ----------
    expt : object
        Object of the class Experiment.

    """

    ##################################
    # Functions on_click
    ##################################

    def _on_button_insert_calib_clicked(button):
        """
        Insert the prepared code cell in the notebook.
        """
        # Initialize local variables
        distance = dw['distance_detec'].value
        m4pitch = dw['m4pitch_init'].value
        c10tablepitch = dw['c10tablepitch_init'].value
        zs = dw['zs_init'].value
        gamma = dw['gamma_init'].value

        # Estimate
        d_m4pitch = (-1.-m4pitch)/4.
        d_c10tablepitch = 2.15*d_m4pitch
        d_zs = 76.3*d_m4pitch
        d_gamma = -2.*d_m4pitch + d_zs*180./(distance*np.pi)

        code = '# Create a large (vertical) ROI taking the reflected beam ROI1, but not the direct beam'+'\n'
        code += '# This ROI1 should have a large height for alignment of zs at high incident angles'+'\n'
        code += '# Create a small ROI centered on the reflected beam ROI2'+'\n'
        code += '# For each line of the table:'+'\n'
        code += '# 1) Move m4pitch and zs to the suggested values'+'\n'
        code += '# 2) Move c10tablepitch and gamma to the suggested value'+'\n'
        code += '# 3) Adjust the fast attenuators'+'\n'
        code += '# 4) Align gamma so that the reflection is in the center of ROI2'+'\n'
        code += '# 5) Align c10tablepitch and zs around the suggested values using ROI1'+'\n'
        code += '# 6) Adjust gamma in ROI2'+'\n'
        code += '# 7) Replace the values in the table'+'\n'
        code += '# '+'\n'
        code += '# When done, execute the cell'+'\n'
        code += '# Use the value of each fit in the script XRR_prepare.ipy'+'\n'
        code += '# '+'\n'
        code += 'calib_xrr_data = np.array(['+'\n'
        code += '# m4pitch  c10tablepitch gamma zs '+'\n'

        for i in range(5):
            code += '[%g, %g, %g, %g],'%(m4pitch, c10tablepitch, gamma, zs)+'\n'
            m4pitch += d_m4pitch
            c10tablepitch += d_c10tablepitch
            zs += d_zs
            gamma += d_gamma

        code += '])\n'
        code += 'jlb.expt.params[\'distance_detec\']= %g\n'%distance

        code += 'xrr.plot_calib_xrr_liquid(calib_xrr_data, distance_detec=jlb.expt.params[\'distance_detec\'])'

        notebook.create_cell(
            code=code, position='below', celltype='code',
            is_print=True, is_execute=False
            )

        notebook.create_cell(code='## Calibration XRR (liquid)', position ='below', celltype='markdown')

        # Put back the action widgets
        notebook.create_cell_action_widgets(is_delete_current_cell=True)


    ##################################
    # Create the widgets
    ##################################

    style = {'description_width': 'initial'}

    # Dictionnary for widgets
    dw = {}

    ############### FLOATTEXT WIDGETS #############

    # We initialize the widgets with a fixed value
    # unless this value has been already changed

    # Sample-to-detector distance in mm
    dw['distance_detec'] = iwid.FloatText(
        value=expt.get_default_param_value('distance_detec', alt=720.),
        description='Sample-to-detector distance (mm):',
        layout=iwid.Layout(width='300px'),
        style=style
        )

    # m4pitch initial (at the beginning of calibration)
    dw['m4pitch_init'] = iwid.FloatText(
        value=expt.get_default_param_value('m4pitch_init', alt=-0.059),
        description='m4pitch (deg):',
        layout=iwid.Layout(width='200px'),
        style=style
        )

    # c10tablepitch initital
    dw['c10tablepitch_init'] = iwid.FloatText(
        value=expt.get_default_param_value('c10tablepitch_init', alt=-0.1175),
        description='c10tablepitch (deg):',
        layout=iwid.Layout(width='250px'),
        style=style
        )

    # zs initital
    dw['zs_init'] = iwid.FloatText(
        value=expt.get_default_param_value('zs_init', alt=33.7),
        description='zs (mm):',
        layout=iwid.Layout(width='150px'),
        style=style
        )

    # gamma initital
    dw['gamma_init'] = iwid.FloatText(
        value=expt.get_default_param_value('gamma_init', alt=5.9),
        description='gamma (deg):',
        layout=iwid.Layout(width='200px'),
        style=style
        )

    ############## BUTTON WIDGETS #################

    # Insert the form
    button = iwid.Button(description='Start calib.')
    button.on_click(_on_button_insert_calib_clicked)
    dw['button_insert_calib'] = button

    # Organize the widgets
    lvl_0 = iwid.VBox([
        iwid.HBox([dw['m4pitch_init'], dw['c10tablepitch_init'], dw['zs_init'], dw['gamma_init']]),
        dw['distance_detec'],
        ])

    ##################################
    # Organize and display the widgets
    ##################################

    print(30*'-')
    print('Instructions:')
    print('1) Go to the first m4pitch point (ideally on the plateau of total reflection)')
    print('2) Align c10tablepitch and zs')
    print('3) Tune gamma so that the reflected beam is in the desired zone of the detector')
    print('4) Create a large (vertical) ROI taking the reflected beam ROI1, but not the direct beam')
    print('5) Create a small ROI centered on the reflected beam ROI2')
    print('6) Report the values and the distance detector-trough')
    print(30*'-')
    display(lvl_0)
    display(dw['button_insert_calib'])


def preview_indiv_scan_xrr(nxs_name, expt):
    """
    Extract and display the ROIs, for interactive preview.

    Parameters
    ----------
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    expt : object
        Object of the class Experiment.

    """
    # Extract the sum of all images in the time scan
    _, images_sum, _, _, _, _, _, _, _ = area_detector.extract_area_detector_scan(
                nxs_name, expt.paths['dir_nxs'], selected_sensors=None,
                is_print_stamps=False, is_print_info=False)

    # Replace the intensity of the dead zones with a value of 0
    image = np.where(images_sum<0., 0., images_sum)

    # Apply the full-scan ROI
    image_roi = image[expt.params['roi_y0']:expt.params['roi_y0']+expt.params['roi_size_y'],
                      expt.params['roi_x0']:expt.params['roi_x0']+expt.params['roi_size_x']]

    fig = plt.figure(figsize=(15,15))

    # Divide the grid in 2x2
    outer = gridspec.GridSpec(2, 2, wspace=0.2)

    #Divide the left row in 1x1
    inner = gridspec.GridSpecFromSubplotSpec(1, 1,
                    subplot_spec=outer[0], wspace=0.1, hspace=0.1)

    ############################################
    ax0 = fig.add_subplot(inner[0])
    ax0.invert_yaxis()
    ax0.pcolormesh(image_roi, cmap = 'jet', shading = 'auto', rasterized=True)
    ax0.set(xlabel = 'horizontal pixel (x)', ylabel ='vertical pixel (y)')
    str_to_display = str([expt.params['roi_x0'], expt.params['roi_y0'],\
                          expt.params['roi_size_x'], expt.params['roi_size_y']])
    if expt.params['type_xrr'] == 'liquid':
        plt.title(nxs_name+'\n'+'Full-scan ROI: %s, lin scale'%str_to_display)
    if expt.params['type_xrr'] == 'solid':
        plt.title(nxs_name+'\n'+'Summation ROI: %s, lin scale'%str_to_display)

    fig.subplots_adjust(top=0.95)

    # Divide the right row in 1x1
    inner = gridspec.GridSpecFromSubplotSpec(1, 1,
                    subplot_spec=outer[1], wspace=0.1, hspace=0.1)

    ############################################

    ax1 = fig.add_subplot(inner[0])
    ax1.invert_yaxis()
    ax1.pcolormesh(image_roi, cmap = 'jet', norm = colors.LogNorm(), shading = 'auto', rasterized=True)
    ax1.set(xlabel = 'horizontal pixel (x)', ylabel ='vertical pixel (y)')
    plt.title('log scale')
    fig.subplots_adjust(top=0.95)

    plt.show()

def preview_direct_xrr(expt):
    """
    Extract and display the ROI of the direct beam.

    Parameters
    ----------
    expt : object
        Object of the class Experiment.

    """
    ####################################################
    # Extract images
    ####################################################

    if expt.params['type_xrr'] == 'solid':

        # Extract sensors to determine if the direct is
        # obtained from a height scan or a time scan
        nxs_path = expt.paths['dir_nxs']+expt.params['direct_scan_name']
        if expt.params['is_print_info']:
            print(_BLUE+" - Open Nexus Data File :"+ _RESET)
            print('\t%s'%nxs_path)
        try:
            nexus = PN.PyNexusFile(nxs_path, fast=True)
        except:
            print(_RED+'\t Could not open Nexus file.'+_RESET)
            raise Exception('Could not open Nexus file.')

        stamps_0d, _ = nexus.extractData("0D")
        sensor_list = [stamps_0d[i][0] if stamps_0d[i][1] is None else stamps_0d[i][1] for i in range(len(stamps_0d))]

        if sensor_list[0] == 'zs':
            # Extract each individual image from the height scan
            images_direct, _, _, _, _, _, _, _ ,_ = area_detector.extract_area_detector_scan(
                            expt.params['direct_scan_name'], expt.paths['dir_nxs'],
                            selected_sensors=None,
                            is_print_stamps=False, is_print_info=False)

            # Take the average over the first five images to get the direct
            image_direct = np.mean(images_direct[0:5], axis = 0)
        else:
            # Extract the sum of all images in the time scan
            _, image_direct, _, _, _, _, _, _, _ = area_detector.extract_area_detector_scan(
                        expt.params['direct_scan_name'], expt.paths['dir_nxs'],
                        selected_sensors=None,
                        is_print_stamps=False, is_print_info=False)

    elif expt.params['type_xrr'] == 'liquid':

        # Extract the sum of all images in the time scan
        _, image_direct, _, _, _, _, _, _, _ = area_detector.extract_area_detector_scan(
                                expt.params['direct_scan_name'], expt.paths['dir_nxs'],
                                selected_sensors=None,
                                is_print_stamps=False, is_print_info=False)

    # Replace the intensity of the dead zones with a value of 0
    image_direct = np.where(image_direct<0., 0., image_direct)

    ####################################################
    # Find the beam and apply ROI
    ####################################################

    # Integrate along the horizontal axis and find the maximum
    integrated_x_direct = image_direct.sum(axis=1)
    pos_y_direct = np.argmax(integrated_x_direct)

    # Height of the summation ROI
    # Always use an odd height for the summation_roi!
    if expt.params['type_xrr'] == 'solid':
        dy = expt.params['roi_size_y']
    if expt.params['type_xrr'] == 'liquid':
        if expt.params['is_track_beam']:
            dy = expt.params['summation_roi_size_y']
        else:
            dy = expt.params['roi_size_y']

    # Width of the summation ROI
    dx = expt.params['roi_size_x']

    summation_roi_direct = image_direct[pos_y_direct-dy//2 : pos_y_direct+dy//2+1,
                                        expt.params['roi_x0'] : expt.params['roi_x0']+dx]

    ####################################################
    # Plot
    ####################################################

    fig = plt.figure(figsize=(15,15))

    # Divide the grid in 2x2
    outer = gridspec.GridSpec(2, 2, wspace=0.2)

    #Divide the left row in 1x1
    inner = gridspec.GridSpecFromSubplotSpec(1, 1,
                    subplot_spec=outer[0], wspace=0.1, hspace=0.1)

    ############################################
    ax0 = fig.add_subplot(inner[0])
    ax0.invert_yaxis()
    ax0.pcolormesh(image_direct, cmap = 'jet', shading = 'auto',
                   norm = colors.LogNorm(), rasterized=True)
    ax0.set(xlabel = 'horizontal pixel (x)', ylabel ='vertical pixel (y)')
    plt.title(expt.params['direct_scan_name']+'\n'+'Direct beam on whole image')
    fig.subplots_adjust(top=0.95)

    # Divide the right row in 2x1
    inner = gridspec.GridSpecFromSubplotSpec(2, 1,
                    subplot_spec=outer[1], hspace=0.5)

    ############################################

    ax1 = fig.add_subplot(inner[0])
    ax1.invert_yaxis()
    ax1.pcolormesh(summation_roi_direct, cmap = 'jet', shading = 'auto', rasterized=True)
    ax1.set_title('Summation ROI (lin scale)', fontsize=12)
    ax1.set(xlabel = 'horizontal pixel (x)', ylabel ='vertical pixel (y)')

    ax2 = fig.add_subplot(inner[1])
    ax2.invert_yaxis()
    ax2.pcolormesh(summation_roi_direct, cmap = 'jet', norm = colors.LogNorm(),
                   shading = 'auto', rasterized=True)
    ax2.set_title('Summation ROI (log scale)', fontsize=12)
    ax2.set(xlabel = 'horizontal pixel (x)', ylabel ='vertical pixel (y)')

    plt.show()

def display_widgets_xrr_liquid(expt):
    """
    Set and display the widgets for processing XRR on a liquid.

    Parameters
    ----------
    expt : object
        Object of the class Experiment.

    """
    ##################################
    # Functions on_click
    ##################################


    def _on_button_preview_full_roi_clicked(button):
        """
        Preview of the full-scan ROI.
        """
        expt.update_param_from_widget(dw,[
            'direct_scan_name', 'is_save', 'is_print_info',
            'is_track_beam', 'is_bckg_up', 'is_bckg_down', 'is_bckg_left', 'is_bckg_right',
            'is_force_direct', 'is_plot_m4pitch', 'is_plot_twotheta', 'is_plot_qz', 'is_plot_pos_y_beam',
            'wavelength', 'fdirect', 'm4pitch0',
            'roi_x0', 'roi_y0', 'roi_size_x', 'roi_size_y', 'summation_roi_size_y'
            ]
        )

        expt.params['type_xrr'] = 'liquid'

        if list_xrr_files is not None:
            iwid.interact(preview_indiv_scan_xrr,
                  nxs_name=[file+'.nxs' for file in list_xrr_files],
                  expt=iwid.fixed(expt)
                  )
        else:
            print('The file %s does not exist.'%(path_to_list_xrr_files))

    def _on_button_preview_direct_clicked(button):
        """
        Preview of the direct beam with the full-scan or summation ROI.
        """
        expt.update_param_from_widget(dw,[
            'direct_scan_name', 'is_save', 'is_print_info',
            'is_track_beam', 'is_bckg_up', 'is_bckg_down', 'is_bckg_left', 'is_bckg_right',
            'is_force_direct', 'is_plot_m4pitch', 'is_plot_twotheta', 'is_plot_qz', 'is_plot_pos_y_beam',
            'wavelength', 'fdirect', 'm4pitch0',
            'roi_x0', 'roi_y0', 'roi_size_x', 'roi_size_y', 'summation_roi_size_y'
            ]
        )

        expt.params['type_xrr'] = 'liquid'

        preview_direct_xrr(expt)


    def _on_button_insert_plot_clicked(button):
        """
        Process and plot the XRR.
        """
        expt.update_param_from_widget(dw,[
            'direct_scan_name', 'is_save', 'is_print_info',
            'is_track_beam', 'is_bckg_up', 'is_bckg_down', 'is_bckg_left', 'is_bckg_right',
            'is_force_direct', 'is_plot_m4pitch', 'is_plot_twotheta', 'is_plot_qz', 'is_plot_pos_y_beam',
            'wavelength', 'fdirect', 'm4pitch0',
            'roi_x0', 'roi_y0', 'roi_size_x', 'roi_size_y', 'summation_roi_size_y'
            ]
        )

        if list_xrr_files is not None:

            # Create a cell with the code to call the backend function
            code = (
            "m4pitch, theta, qz, bckg_refl_up, bckg_refl_down, bckg_refl_left, "
            "bckg_refl_right, bckg_refl, err_refl, refl =\\\n"
            "xrr.process_xrr_liquid_scan(\n"
            "list_xrr_files=%s,\n"
            "path_to_nxs_dir=%s, "
            "direct_scan_name='%s',\n"
            "roi_x0=%s, "
            "roi_y0=%s, "
            "roi_size_x=%s, "
            "roi_size_y=%s,\n"
            "summation_roi_size_y=%s, "
            "m4pitch0=%s, "
            "wavelength=%s, "
            "fdirect=%s,\n"
            "is_bckg_up=%s, "
            "is_bckg_down=%s, "
            "is_bckg_left=%s, "
            "is_bckg_right=%s, "
            "is_track_beam=%s,\n"
            "is_force_direct=%s, "
            "path_to_save_dir=%s,\n"
            "is_plot_m4pitch=%s, "
            "is_plot_twotheta=%s, "
            "is_plot_qz=%s,\n"
            "is_plot_pos_y_beam=%s, "
            "is_save=%s, "
            "is_print_info=%s"
            ")"%(
                [elem for elem in list_xrr_files],
                'paths[\'dir_nxs\']',
                expt.params['direct_scan_name'],
                expt.params['roi_x0'],
                expt.params['roi_y0'],
                expt.params['roi_size_x'],
                expt.params['roi_size_y'],
                expt.params['summation_roi_size_y'],
                expt.params['m4pitch0'],
                expt.params['wavelength'],
                expt.params['fdirect'],
                expt.params['is_bckg_up'],
                expt.params['is_bckg_down'],
                expt.params['is_bckg_left'],
                expt.params['is_bckg_right'],
                expt.params['is_track_beam'],
                expt.params['is_force_direct'],
                'paths[\'dir_save\']',
                expt.params['is_plot_m4pitch'],
                expt.params['is_plot_twotheta'],
                expt.params['is_plot_qz'],
                expt.params['is_plot_pos_y_beam'],
                expt.params['is_save'],
                expt.params['is_print_info']
                )
            )

            notebook.create_cell(
                code=code, position='below', celltype='code',
                is_print=True, is_execute=True
                )

            # Put back the action widgets
            notebook.create_cell_action_widgets(is_delete_current_cell=True)

        else:
            print('The file %s does not exist.'%(path_to_list_xrr_files))



    ##################################
    # Create the widgets
    ##################################

    style = {'description_width': 'initial'}

    # The first scan guides to the file with the list of files
    scan = expt.list_scans[0]

    # List of XRR scans
    path_to_list_xrr_files = expt.paths['dir_nxs']+scan.name+'_XRR_files.dat'
    if os.path.exists(path_to_list_xrr_files):
        # Do not take into accound the direct, which is seen as a comment
        list_xrr_files = np.genfromtxt(path_to_list_xrr_files, dtype='U')
        # Deal with the case of a single file
        if len(np.shape(list_xrr_files))==0:
            list_xrr_files = [str(list_xrr_files)]

        # Check if there is a direct in the file
        test_direct = np.genfromtxt(path_to_list_xrr_files, dtype='U', comments='x')
        if '#Direct:' in test_direct[0]:
            direct_name = test_direct[0].split(':')[1]+'.nxs'
        else:
            # If not, the user will have to choose the direct by hand
            direct_name = expt.list_nxs[-1]

    else:
        list_xrr_files = None
        direct_name = expt.list_scans[-1]

    # Dictionnary for widgets
    dw = {}

    # Select the direct scan
    dw['direct_scan_name'] = iwid.Dropdown(
        options=expt.list_nxs,
        value=direct_name,
        style=style,
        layout=iwid.Layout(width='350px'),
        description='Direct:')

    # Save the results
    dw['is_save'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_save', alt=True),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Save')
    # Print scan info
    dw['is_print_info'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_print_info', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Print scan info')

    # Automatic tracking of the beam
    dw['is_track_beam'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_track_beam', alt=True),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Track beam')

    # Choose the backgrounds to use (top, left, bottom, up)
    dw['is_bckg_up'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_bckg_up', alt=True),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Bckg ROI up')
    dw['is_bckg_down'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_bckg_down', alt=True),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Bckg ROI down')
    dw['is_bckg_left'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_bckg_left', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Bckg ROI left')
    dw['is_bckg_right'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_bckg_right', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Bckg ROI right')

    # Force the direct to the value of fdirect
    dw['is_force_direct'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_force_direct', alt=False),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Force direct to the value:')

   # Plot XRR vs m4pitch
    dw['is_plot_m4pitch'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_plot_m4pitch', alt=True),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Plot m4pitch')

   # Plot XRR vs twotheta
    dw['is_plot_twotheta'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_plot_twotheta', alt=True),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Plot 2*theta')

    # Plot XRR vs qz
    dw['is_plot_qz'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_plot_qz', alt=True),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Plot qz')

    # Plot position of the beam
    dw['is_plot_pos_y_beam'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_plot_pos_y_beam', alt=False),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Plot position beam')

    # Wavelength
    dw['wavelength'] = iwid.FloatText(
        value=expt.get_default_param_value('wavelength', alt=0.155),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='wavelength (nm)')

    # Value of direct if direct is forced
    dw['fdirect'] = iwid.FloatText(
        value=expt.get_default_param_value('fdirect', alt=1.),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='')

    # m4pitch0 (aligned with the beam)
    dw['m4pitch0'] = iwid.FloatText(
        value=expt.get_default_param_value('m4pitch0', alt=-0.035),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='m4pitch0 (deg)')

    # Sizes and positions of the full-scan ROI
    dw['roi_x0'] = iwid.IntText(
        value=expt.get_default_param_value('roi_x0', alt=50),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Full-scan ROI: x0')
    dw['roi_y0'] = iwid.IntText(
        value=expt.get_default_param_value('roi_y0', alt=195),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='y0')
    dw['roi_size_x'] = iwid.IntText(
        value=expt.get_default_param_value('roi_size_x', alt=40),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='size x')
    dw['roi_size_y'] = iwid.IntText(
        value=expt.get_default_param_value('roi_size_y', alt=30),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='size y')

    # Height of summation ROI
    dw['summation_roi_size_y'] = iwid.IntText(
        value=expt.get_default_param_value('summation_roi_size_y', alt=5),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='dy')


    # Preview full-scan ROI
    button = iwid.Button(description='Preview full-scan ROI',
                         style={"button_color":"lightgreen"})
    button.on_click(_on_button_preview_full_roi_clicked)
    dw['button_preview_full_roi'] = button

    # Preview full-scan ROI on direct beam
    button = iwid.Button(description='Preview direct beam',
                         style={"button_color":"lightgreen"})
    button.on_click(_on_button_preview_direct_clicked)
    dw['button_preview_direct'] = button

    # Proceed
    button = iwid.Button(description='Extract & plot XRR',
                         style={"button_color":"lightgreen"})
    button.on_click(_on_button_insert_plot_clicked)
    dw['button_insert_plot'] = button


    ##################################
    # Organize and display the widgets
    ##################################

    lvl_0 = iwid.HBox([
        dw['roi_x0'], dw['roi_y0'], dw['roi_size_x'], dw['roi_size_y']
        ])

    lvl_1 = iwid.HBox([
        dw['is_bckg_up'], dw['is_bckg_down'], dw['is_bckg_left'], dw['is_bckg_right'], dw['is_track_beam']
        ])

    lvl_2 = iwid.HBox([
        dw['summation_roi_size_y'], dw['m4pitch0'], dw['wavelength'], dw['is_force_direct'], dw['fdirect']
        ])

    lvl_3 = iwid.HBox([
        dw['direct_scan_name']
        ])

    lvl_4 = iwid.HBox([
        dw['is_plot_m4pitch'], dw['is_plot_twotheta'], dw['is_plot_qz'],\
        dw['is_plot_pos_y_beam'], dw['is_save'], dw['is_print_info']
        ])

    lvl_5 = iwid.HBox([
        dw['button_preview_full_roi'], dw['button_preview_direct'], dw['button_insert_plot']
        ])

    print('Define the full-scan ROI & check that all the reflected beams are inside.')
    print('The parameter dy is the vertical width of the summation ROI centered on the beam.')
    print('\033[1mdy has to be an odd number!\033[0m')

    # Display the widgets
    display(iwid.VBox([
        lvl_0,
        lvl_1,
        lvl_2,
        lvl_3,
        lvl_4,
        lvl_5
        ])
            )

def display_widgets_xrr_solid(expt):
    """
    Set and display the widgets for processing XRR on a solid.

    Parameters
    ----------
    expt : object
        Object of the class Experiment.

    """
    ##################################
    # Functions on_click
    ##################################


    def _on_button_preview_roi_clicked(button):
        """
        Preview of the full-scan ROI.
        """
        expt.update_param_from_widget(dw,[
            'direct_scan_name', 'is_save', 'is_print_info',
            'is_bckg_up', 'is_bckg_down', 'is_bckg_left', 'is_bckg_right',
            'is_force_direct', 'is_plot_twotheta', 'is_plot_qz',
            'wavelength', 'fdirect',
            'roi_x0', 'roi_y0', 'roi_size_x', 'roi_size_y'
            ]
        )

        expt.params['type_xrr'] = 'solid'
        iwid.interact(preview_indiv_scan_xrr,
              nxs_name=[file+'.nxs' for file in list_xrr_files],
              expt=iwid.fixed(expt)
              )

    def _on_button_preview_direct_clicked(button):
        """
        Preview of the direct beam with the full-scan or summation ROI.
        """
        expt.update_param_from_widget(dw,[
            'direct_scan_name', 'is_save', 'is_print_info',
            'is_bckg_up', 'is_bckg_down', 'is_bckg_left', 'is_bckg_right',
            'is_force_direct', 'is_plot_twotheta', 'is_plot_qz',
            'wavelength', 'fdirect',
            'roi_x0', 'roi_y0', 'roi_size_x', 'roi_size_y'
            ]
        )

        expt.params['type_xrr'] = 'solid'
        preview_direct_xrr(expt)

    def _on_button_insert_plot_clicked(button):
        """
        Process and plot the XRR.
        """
        expt.update_param_from_widget(dw,[
            'direct_scan_name', 'is_save', 'is_print_info',
            'is_bckg_up', 'is_bckg_down', 'is_bckg_left', 'is_bckg_right',
            'is_force_direct', 'is_plot_twotheta', 'is_plot_qz',
            'wavelength', 'fdirect',
            'roi_x0', 'roi_y0', 'roi_size_x', 'roi_size_y'
            ]
        )

        # Create a cell with the code to call the backend function
        code = (
        "theta, qz, bckg_refl_up, bckg_refl_down, bckg_refl_left, bckg_refl_right, bckg_refl, err_refl, refl =\\\n"
        "xrr.process_xrr_solid_scan(\n"
        "list_xrr_files=%s,\n"
        "path_to_nxs_dir=%s, "
        "direct_scan_name='%s',\n"
        "roi_x0=%s, "
        "roi_y0=%s, "
        "roi_size_x=%s, "
        "roi_size_y=%s,\n"
        "wavelength=%s, "
        "fdirect=%s, "
        "is_bckg_up=%s, "
        "is_bckg_down=%s,\n"
        "is_bckg_left=%s, "
        "is_bckg_right=%s, "
        "is_force_direct=%s, "
        "path_to_save_dir=%s,\n"
        "is_plot_twotheta=%s, "
        "is_plot_qz=%s, "
        "is_save=%s, "
        "is_print_info=%s"
        ")"%(
            list_xrr_files,
            'paths[\'dir_nxs\']',
            expt.params['direct_scan_name'],
            expt.params['roi_x0'],
            expt.params['roi_y0'],
            expt.params['roi_size_x'],
            expt.params['roi_size_y'],
            expt.params['wavelength'],
            expt.params['fdirect'],
            expt.params['is_bckg_up'],
            expt.params['is_bckg_down'],
            expt.params['is_bckg_left'],
            expt.params['is_bckg_right'],
            expt.params['is_force_direct'],
            'paths[\'dir_save\']',
            expt.params['is_plot_twotheta'],
            expt.params['is_plot_qz'],
            expt.params['is_save'],
            expt.params['is_print_info']
            )
        )

        notebook.create_cell(
            code=code, position='below', celltype='code',
            is_print=True, is_execute=True
            )

        # Add a title
        if len(expt.list_scans)>1:
            scan_temp = expt.list_scans[-1]
            if scan_temp.command:
                code = '### '+scan_temp.name+': '+scan_temp.command
            else:
                code = '### '+scan_temp.name
            notebook.create_cell(code=code, position='below', celltype='markdown')

        # Put back the action widgets
        notebook.create_cell_action_widgets(is_delete_current_cell=True)


    ##################################
    # Create the widgets
    ##################################

    style = {'description_width': 'initial'}

    # The list of XRR scans has to be selected before clicking on the process button
    list_xrr_files = [scan.name for scan in expt.list_scans][::-1]

    # Try to guess the name of the direct (one before the first XRR)
    name_first_scan = list_xrr_files[0]
    scan_number_guess = int(name_first_scan.split('_')[-1])-1
    if any(str(scan_number_guess) in s for s in expt.list_nxs):
        scan_guess_arg = [i for i, elem in enumerate(expt.list_nxs) if str(scan_number_guess) in elem][-1]
        direct_scan_name_guess = expt.list_nxs[scan_guess_arg]
    else:
        direct_scan_name_guess = expt.list_nxs[-1]

    # Dictionnary for widgets
    dw = {}

    # Select the direct scan (among all the nexus files)
    dw['direct_scan_name'] = iwid.Dropdown(
        options=expt.list_nxs,
        value=direct_scan_name_guess,
        style=style,
        layout=iwid.Layout(width='350px'),
        description='Direct:')

    # Save the results
    dw['is_save'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_save', alt=True),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Save')
    # Print scan info
    dw['is_print_info'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_print_info', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Print scan info')

    # Choose the backgrounds to use (top, left, bottom, up)
    dw['is_bckg_up'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_bckg_up', alt=True),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Bckg ROI up')
    dw['is_bckg_down'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_bckg_down', alt=True),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Bckg ROI down')
    dw['is_bckg_left'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_bckg_left', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Bckg ROI left')
    dw['is_bckg_right'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_bckg_right', alt=False),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='Bckg ROI right')

    # Force the direct to the value of fdirect
    dw['is_force_direct'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_force_direct', alt=False),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Force direct to the value:')

   # Plot XRR vs twotheta
    dw['is_plot_twotheta'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_plot_twotheta', alt=True),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Plot 2*theta')

    # Plot XRR vs qz
    dw['is_plot_qz'] = iwid.Checkbox(
        value=expt.get_default_param_value('is_plot_qz', alt=True),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Plot qz')

    # Wavelength
    dw['wavelength'] = iwid.FloatText(
        value=expt.get_default_param_value('wavelength', alt=0.155),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='wavelength (nm)')

    # Value of direct if direct is forced
    dw['fdirect'] = iwid.FloatText(
        value=expt.get_default_param_value('fdirect', alt=1.),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='')

    # Sizes and positions of the full-scan ROI
    dw['roi_x0'] = iwid.IntText(
        value=expt.get_default_param_value('roi_x0', alt=50),
        style=style,
        layout=iwid.Layout(width='200px'),
        description='Full-scan ROI: x0')
    dw['roi_y0'] = iwid.IntText(
        value=expt.get_default_param_value('roi_y0', alt=195),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='y0')
    dw['roi_size_x'] = iwid.IntText(
        value=expt.get_default_param_value('roi_size_x', alt=40),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='size x')
    dw['roi_size_y'] = iwid.IntText(
        value=expt.get_default_param_value('roi_size_y', alt=30),
        style=style,
        layout=iwid.Layout(width='150px'),
        description='size y')


    # Preview full-scan ROI
    button = iwid.Button(description='Preview ROI',
                         style={"button_color":"lightgreen"})
    button.on_click(_on_button_preview_roi_clicked)
    dw['button_preview_roi'] = button

    # Preview full-scan ROI on direct beam
    button = iwid.Button(description='Preview direct beam',
                         style={"button_color":"lightgreen"})
    button.on_click(_on_button_preview_direct_clicked)
    dw['button_preview_direct'] = button

    # Proceed
    button = iwid.Button(description='Extract & plot XRR',
                         style={"button_color":"lightgreen"})
    button.on_click(_on_button_insert_plot_clicked)
    dw['button_insert_plot'] = button


    ##################################
    # Organize and display the widgets
    ##################################

    lvl_0 = iwid.HBox([
        dw['roi_x0'], dw['roi_y0'], dw['roi_size_x'], dw['roi_size_y']
        ])

    lvl_1 = iwid.HBox([
        dw['is_bckg_up'], dw['is_bckg_down'], dw['is_bckg_left'], dw['is_bckg_right']
        ])

    lvl_2 = iwid.HBox([
        dw['wavelength'], dw['is_force_direct'], dw['fdirect']
        ])

    lvl_3 = iwid.HBox([
        dw['direct_scan_name']
        ])

    lvl_4 = iwid.HBox([
        dw['is_plot_twotheta'], dw['is_plot_qz'], dw['is_save'], dw['is_print_info']
        ])

    lvl_5 = iwid.HBox([
        dw['button_preview_roi'], dw['button_preview_direct'], dw['button_insert_plot']
        ])

    print('Define the ROI & check that all the reflected beams are inside.')
    print('\033[1mdy has to be an odd number!\033[0m')

    # Display the widgets
    display(iwid.VBox([
        lvl_0,
        lvl_1,
        lvl_2,
        lvl_3,
        lvl_4,
        lvl_5
        ])
            )

