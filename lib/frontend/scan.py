"""
Module defining the class Scan.
"""
class Scan:
    """
    Class defining a Scan.

    Attributes
    ----------
    path_to_nxs : str
        Path to the nexus file, e.g. `user/SIRIUS_2020_03_12_0756.nxs`.
    nxs_name : str
        Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.
    name : str
        Name of the scan, as is in the logs, e.g. `SIRIUS_2020_03_12_0756`.
    command : str or None
        Command corresponding to the scan, if found in the logs.

    """
    def __init__(self):
        """
        Constructor.
        """
        self.path_to_nxs = ''
        self.nxs_name = '.nxs'
        self.name = ''
        self.command = None

        # For 1D plots
        self.x_label = ''
        self.y_label = ''
        self.is_logx = False
        self.is_logy = False

    def set_identifiers(self, path_to_nxs_dir, nxs_name):
        """
        Set identifiers to the scan (path, name ...).

        Parameters
        ----------
        path_to_nxs_dir : str
            Path to the nexus files directory, e.g. `user/`.
        nxs_name : str
            Nexus name, e.g. `SIRIUS_2020_03_12_0756.nxs`.

        """
        self.path_to_nxs = path_to_nxs_dir+nxs_name
        self.nxs_name = nxs_name
        self.name = nxs_name.split('.nxs')[0]

    def set_command(self, path_to_logs_dir, list_logs):
        """
        Find the command corresponding to the scan in the logs,
        and set it to the attribute `command`.

        Parameters
        ----------
        path_to_logs_dir : str
            Path to the logs files directory, e.g. `user/logs/`.
        list_logs : list of str
            List of log filenames in the logs folder.

        """
        scan_found = False

        # If no scan is found, the attribute is set to None
        self.command = None

        for log_filename in list_logs:
            with open(path_to_logs_dir+log_filename, encoding='utf8', errors='ignore') as file:
                for line in file:
                    if "#" not in line:
                        temp = line
                    if (self.name in line and 'Next nexus (data) file name:' not in line):
                        # Remove the line jump
                        self.command = temp.replace('\n','')
                        scan_found = True
                file.close()

            if scan_found:
                break
