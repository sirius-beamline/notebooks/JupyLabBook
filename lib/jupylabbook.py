"""
Custom module called by `JupyLabBook.ipynb`.
Coordinates the different sub-modules.
The object `experiment` containing all the attributes relevant to the current experiment is defined here.
"""
from IPython.display import Javascript, display
from lib.frontend import action, process, experiment, notebook

# To have all the cells expanded (not collapsed)
display(Javascript('IPython.OutputArea.prototype._should_scroll = function(lines) {return false;}'))

# Save pdf images for the final pdf report
try:
    # IPython>=7.23
    import matplotlib_inline.backend_inline
    matplotlib_inline.backend_inline.set_matplotlib_formats('png', 'pdf')
except ModuleNotFoundError:
    # IPython<7.23
    from IPython.display import set_matplotlib_formats
    set_matplotlib_formats('png', 'pdf')

# Rules for numbering versions
# vX.Y.Z with X major, Y minor, Z patch. Each new version increments Z (not used if Z=0).
# If the file Example.ipynb cannot run with the new libraries, increment Y and restart at Z=0.
__version__ = 'v3.2.3'

# Define the object experiment
# It contains all the attributes relevant to the current experiment
# Check if expt already exists (avoid loosing info when reloading the first cell)
try:
    expt
except NameError:
    expt = experiment.Experiment()

    # Indicate that the default params should be loaded
    expt.params['is_params_loaded'] = False

def print_version(version):
    """
    Print the version of JupyLabBook and a link to the main repo.

    Parameters
    ----------
    version : str
        Version of JupyLabBook vX.Y.Z with X major, Y minor, Z patch.

    """
    print('JupyLabBook version: %s'%version)
    print('More info on: %s'%\
          'https://gitlab.synchrotron-soleil.fr/sirius-beamline/notebooks/JupyLabBook\n')

def display_action():
    """
    Prepare and display the widgets for selecting the next action.
    """
    # Set the current list of nexus files
    expt.set_list_nxs()

    # Set the current list of logs
    expt.set_list_logs()

    # Display the action widgets
    action.display_widgets_action(expt)

def display_process():
    """
    Prepare and display the widgets for processing the selected scans.
    """
    # Display the process widgets
    process.display_widgets_process(expt)

def start(paths):
    """
    Check that the paths are ok and display the action widgets.

    Parameters
    ----------
    paths : dict
        Dictionary of all the required paths to specific folders and files.

    Raises
    ------
    FileNotFoundError
        If a file or a folder is missing.

    """
    print_version(__version__)

    # Check if the paths are ok, and set them
    expt.check_and_set_paths(paths)

    if not expt.is_paths_ok:
        raise FileNotFoundError('Missing folder or file.')

    # Set params to their default values if the first cell
    # has not been executed before (avoid loosing params when reloading
    # the first cell)
    if not expt.params['is_params_loaded']:
        print('Default parameters loaded.')
        expt.load_params_from_json(expt.paths['dir_params']+'params_init.json')
        expt.params['is_params_loaded'] = True
    else:
        print('Previous parameters kept.')

    if expt.is_paths_ok:
        notebook.create_cell_action_widgets(is_delete_current_cell=False)
